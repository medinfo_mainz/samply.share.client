$(document).ready(function() {
	$('#matches').on('hide.bs.collapse', function(e) {
	    $(e.target)
    		.prev().css({'font-size' : '14px',
    					'font-weight' : 'normal'
    			})
	    	.parent()
	        .find(".expandIcon:first")
	        .removeClass('fa-rotate-45');
	});
	
	$('#matches').on('show.bs.collapse', function(e) {
	    $(e.target)
    	.prev().css({'font-size' : '18px',
					'font-weight' : 'bold'
			})
    	.parent()
        .find(".expandIcon:first")
    	.addClass('fa-rotate-45');
	});
	
	if ($('.clockpicker')[0]) {
	    $('.clockpicker').clockpicker({
	        donetext: 'Done'
	    });
	}
	if ($('.inputmask')[0]) {
	    $('.inputmask').inputmask({
	      mask: '9?999'
	    });
	}
	if ($('.portmask')[0]) {
	    $('.portmask').inputmask({
	      mask: '?99999'
	    });
	}
	if ($('.switch')[0]) {
		$('.switch').bootstrapSwitch();
	}

});