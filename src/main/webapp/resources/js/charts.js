
$.jqplot.config.enablePlugins = true;

/**
 * Draws a pie chart in the div.
 * @param {string} elementId
 * @param {array of numbers} x
 * @param {array of strings} y
 * @returns {undefined}
 */
function makePieChart(elementId, x, y) {
    var data = [];
    var sumOfY = 0;
    for (var i = 0; i < x.length; ++i) {
        data.push([x[i], y[i]]);
        sumOfY += y[i];
    }
    var dataLabels = y;
    for (var i = 0; i < y.length; ++i) {
        var percent = (dataLabels[i] / sumOfY * 100).toFixed(0) ;
        dataLabels[i] = "" + dataLabels[i] + " (" + percent + "%)";
    }
    
    $.jqplot(elementId, [data], {
        seriesDefaults: {
            renderer: $.jqplot.PieRenderer, 
            trendline: { show:false }, 
            rendererOptions: { 
                padding: 8, 
                showDataLabels: true,
                dataLabels: dataLabels,
                dataLabelThreshold: 0,
                highlightMouseOver: false
            }
        },
        legend: {
            show: true, 
            placement: 'outside',
            rendererOptions: {
                numberRows: x.length,
            }, 
            location: 's',
        }       
    });
    
    // fix for overlapping legend 
    var chartElement = $("#" + elementId);
    var legendTable = chartElement.find("table.jqplot-table-legend");
    legendTable.css("position", "static"); // removes absolute positioning
    legendTable.css("margin", "0 auto"); // centers table inside wrapper div
    var legendWrapper = $(document.createElement("div"));
    legendTable.appendTo(legendWrapper);
    legendWrapper.appendTo(chartElement.parent());
}

function makeBarChart(elementId, x, y, xAxisLabel, yAxisLabel) {
    var plot = $.jqplot(elementId, [y], {
        seriesDefaults: {
            renderer:$.jqplot.BarRenderer,
            pointLabels: { 
                show: true 
            },
            rendererOptions: {
                highlightMouseOver: false,
                barMargin: 1
            }
        },
        axes: {
            xaxis: {
                label: xAxisLabel,
                renderer: $.jqplot.CategoryAxisRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                ticks: x,
                tickOptions: {
                    angle: -30
                }
            },
            yaxis: {
                label: yAxisLabel,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
            }
        },
        highlighter: { show: false }
    });
    
    // to make bar charts as big as pie charts
    var chartDiv = $("#" + elementId);
    var h = parseInt(chartDiv.height(), 10) 
            + parseInt($(".jqplot-title").height(), 10) 
            + parseInt($(".jqplot-xaxis").height(), 10) 
            - 23; // TODO less magic would be nice
    chartDiv.height(h);
    plot.replot();
}
