/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core.tables.records;


import de.samply.share.client.core.tables.Broker;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class BrokerRecord extends UpdatableRecordImpl<BrokerRecord> implements Record6<Integer, String, String, String, Timestamp, String> {

    private static final long serialVersionUID = -152665395;

    /**
     * Setter for <code>public.broker.id</code>.
     */
    public void setId(Integer value) {
        setValue(0, value);
    }

    /**
     * Getter for <code>public.broker.id</code>.
     */
    public Integer getId() {
        return (Integer) getValue(0);
    }

    /**
     * Setter for <code>public.broker.address</code>.
     */
    public void setAddress(String value) {
        setValue(1, value);
    }

    /**
     * Getter for <code>public.broker.address</code>.
     */
    public String getAddress() {
        return (String) getValue(1);
    }

    /**
     * Setter for <code>public.broker.name</code>.
     */
    public void setName(String value) {
        setValue(2, value);
    }

    /**
     * Getter for <code>public.broker.name</code>.
     */
    public String getName() {
        return (String) getValue(2);
    }

    /**
     * Setter for <code>public.broker.authcode</code>.
     */
    public void setAuthcode(String value) {
        setValue(3, value);
    }

    /**
     * Getter for <code>public.broker.authcode</code>.
     */
    public String getAuthcode() {
        return (String) getValue(3);
    }

    /**
     * Setter for <code>public.broker.last_checked</code>.
     */
    public void setLastChecked(Timestamp value) {
        setValue(4, value);
    }

    /**
     * Getter for <code>public.broker.last_checked</code>.
     */
    public Timestamp getLastChecked() {
        return (Timestamp) getValue(4);
    }

    /**
     * Setter for <code>public.broker.email</code>.
     */
    public void setEmail(String value) {
        setValue(5, value);
    }

    /**
     * Getter for <code>public.broker.email</code>.
     */
    public String getEmail() {
        return (String) getValue(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, Timestamp, String> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, Timestamp, String> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Broker.BROKER.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Broker.BROKER.ADDRESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Broker.BROKER.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Broker.BROKER.AUTHCODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field5() {
        return Broker.BROKER.LAST_CHECKED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field6() {
        return Broker.BROKER.EMAIL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getAddress();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getAuthcode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value5() {
        return getLastChecked();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value6() {
        return getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord value2(String value) {
        setAddress(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord value3(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord value4(String value) {
        setAuthcode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord value5(Timestamp value) {
        setLastChecked(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord value6(String value) {
        setEmail(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerRecord values(Integer value1, String value2, String value3, String value4, Timestamp value5, String value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached BrokerRecord
     */
    public BrokerRecord() {
        super(Broker.BROKER);
    }

    /**
     * Create a detached, initialised BrokerRecord
     */
    public BrokerRecord(Integer id, String address, String name, String authcode, Timestamp lastChecked, String email) {
        super(Broker.BROKER);

        setValue(0, id);
        setValue(1, address);
        setValue(2, name);
        setValue(3, authcode);
        setValue(4, lastChecked);
        setValue(5, email);
    }
}
