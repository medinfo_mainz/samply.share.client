/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core.tables.pojos;


import de.samply.share.client.core.enums.ChartType;

import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ChartDefinition implements Serializable {

    private static final long serialVersionUID = -1075687920;

    private Integer   id;
    private Integer   groupId;
    private String    viewField1;
    private ChartType type;

    public ChartDefinition() {}

    public ChartDefinition(ChartDefinition value) {
        this.id = value.id;
        this.groupId = value.groupId;
        this.viewField1 = value.viewField1;
        this.type = value.type;
    }

    public ChartDefinition(
        Integer   id,
        Integer   groupId,
        String    viewField1,
        ChartType type
    ) {
        this.id = id;
        this.groupId = groupId;
        this.viewField1 = viewField1;
        this.type = type;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getViewField1() {
        return this.viewField1;
    }

    public void setViewField1(String viewField1) {
        this.viewField1 = viewField1;
    }

    public ChartType getType() {
        return this.type;
    }

    public void setType(ChartType type) {
        this.type = type;
    }
}
