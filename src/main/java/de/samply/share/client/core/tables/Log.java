/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core.tables;


import de.samply.share.client.core.Keys;
import de.samply.share.client.core.Public;
import de.samply.share.client.core.tables.records.LogRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Log extends TableImpl<LogRecord> {

    private static final long serialVersionUID = 136497484;

    /**
     * The reference instance of <code>public.log</code>
     */
    public static final Log LOG = new Log();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<LogRecord> getRecordType() {
        return LogRecord.class;
    }

    /**
     * The column <code>public.log.id</code>.
     */
    public final TableField<LogRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaulted(true), this, "");

    /**
     * The column <code>public.log.actor</code>.
     */
    public final TableField<LogRecord, String> ACTOR = createField("actor", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.log.dated</code>.
     */
    public final TableField<LogRecord, Timestamp> DATED = createField("dated", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

    /**
     * The column <code>public.log.logger</code>.
     */
    public final TableField<LogRecord, String> LOGGER = createField("logger", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.log.level</code>.
     */
    public final TableField<LogRecord, String> LEVEL = createField("level", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.log.type</code>.
     */
    public final TableField<LogRecord, String> TYPE = createField("type", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.log.message</code>.
     */
    public final TableField<LogRecord, String> MESSAGE = createField("message", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * Create a <code>public.log</code> table reference
     */
    public Log() {
        this("log", null);
    }

    /**
     * Create an aliased <code>public.log</code> table reference
     */
    public Log(String alias) {
        this(alias, LOG);
    }

    private Log(String alias, Table<LogRecord> aliased) {
        this(alias, aliased, null);
    }

    private Log(String alias, Table<LogRecord> aliased, Field<?>[] parameters) {
        super(alias, Public.PUBLIC, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<LogRecord, Integer> getIdentity() {
        return Keys.IDENTITY_LOG;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<LogRecord> getPrimaryKey() {
        return Keys.LOG_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<LogRecord>> getKeys() {
        return Arrays.<UniqueKey<LogRecord>>asList(Keys.LOG_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Log as(String alias) {
        return new Log(alias, this);
    }

    /**
     * Rename this table
     */
    public Log rename(String name) {
        return new Log(name, null);
    }
}
