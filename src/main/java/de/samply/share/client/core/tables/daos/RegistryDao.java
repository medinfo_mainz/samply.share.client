/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core.tables.daos;


import de.samply.share.client.core.tables.Registry;
import de.samply.share.client.core.tables.records.RegistryRecord;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RegistryDao extends DAOImpl<RegistryRecord, de.samply.share.client.core.tables.pojos.Registry, Integer> {

    /**
     * Create a new RegistryDao without any configuration
     */
    public RegistryDao() {
        super(Registry.REGISTRY, de.samply.share.client.core.tables.pojos.Registry.class);
    }

    /**
     * Create a new RegistryDao with an attached configuration
     */
    public RegistryDao(Configuration configuration) {
        super(Registry.REGISTRY, de.samply.share.client.core.tables.pojos.Registry.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(de.samply.share.client.core.tables.pojos.Registry object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<de.samply.share.client.core.tables.pojos.Registry> fetchById(Integer... values) {
        return fetch(Registry.REGISTRY.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public de.samply.share.client.core.tables.pojos.Registry fetchOneById(Integer value) {
        return fetchOne(Registry.REGISTRY.ID, value);
    }

    /**
     * Fetch records that have <code>address IN (values)</code>
     */
    public List<de.samply.share.client.core.tables.pojos.Registry> fetchByAddress(String... values) {
        return fetch(Registry.REGISTRY.ADDRESS, values);
    }

    /**
     * Fetch records that have <code>app_key IN (values)</code>
     */
    public List<de.samply.share.client.core.tables.pojos.Registry> fetchByAppKey(String... values) {
        return fetch(Registry.REGISTRY.APP_KEY, values);
    }

    /**
     * Fetch records that have <code>last_upload_at IN (values)</code>
     */
    public List<de.samply.share.client.core.tables.pojos.Registry> fetchByLastUploadAt(Timestamp... values) {
        return fetch(Registry.REGISTRY.LAST_UPLOAD_AT, values);
    }

    /**
     * Fetch records that have <code>email IN (values)</code>
     */
    public List<de.samply.share.client.core.tables.pojos.Registry> fetchByEmail(String... values) {
        return fetch(Registry.REGISTRY.EMAIL, values);
    }

    /**
     * Fetch records that have <code>password IN (values)</code>
     */
    public List<de.samply.share.client.core.tables.pojos.Registry> fetchByPassword(String... values) {
        return fetch(Registry.REGISTRY.PASSWORD, values);
    }
}
