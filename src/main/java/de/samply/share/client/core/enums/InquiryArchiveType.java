/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core.enums;


import de.samply.share.client.core.Public;

import javax.annotation.Generated;

import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum InquiryArchiveType implements EnumType {

    IA_POSITIVE_REPLY("IA_POSITIVE_REPLY"),

    IA_NEGATIVE_REPLY("IA_NEGATIVE_REPLY"),

    IA_NO_REPLY("IA_NO_REPLY");

    private final String literal;

    private InquiryArchiveType(String literal) {
        this.literal = literal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "inquiry_archive_type";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLiteral() {
        return literal;
    }
}
