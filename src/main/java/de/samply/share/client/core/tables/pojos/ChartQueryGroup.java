/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ChartQueryGroup implements Serializable {

    private static final long serialVersionUID = -2052434689;

    private Integer id;
    private String  name;
    private String  query;

    public ChartQueryGroup() {}

    public ChartQueryGroup(ChartQueryGroup value) {
        this.id = value.id;
        this.name = value.name;
        this.query = value.query;
    }

    public ChartQueryGroup(
        Integer id,
        String  name,
        String  query
    ) {
        this.id = id;
        this.name = name;
        this.query = query;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
