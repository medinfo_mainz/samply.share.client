/**
 * This class is generated by jOOQ
 */
package de.samply.share.client.core;


import de.samply.share.client.core.tables.Actor;
import de.samply.share.client.core.tables.Broker;
import de.samply.share.client.core.tables.ChartDefinition;
import de.samply.share.client.core.tables.ChartQueryGroup;
import de.samply.share.client.core.tables.Configuration;
import de.samply.share.client.core.tables.ExportDefinition;
import de.samply.share.client.core.tables.Inquiry;
import de.samply.share.client.core.tables.Inquiryarchive;
import de.samply.share.client.core.tables.Inquiryresult;
import de.samply.share.client.core.tables.Log;
import de.samply.share.client.core.tables.Note;
import de.samply.share.client.core.tables.Registry;
import de.samply.share.client.core.tables.Tokens;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in public
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.6.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table public.actor
     */
    public static final Actor ACTOR = de.samply.share.client.core.tables.Actor.ACTOR;

    /**
     * The table public.broker
     */
    public static final Broker BROKER = de.samply.share.client.core.tables.Broker.BROKER;

    /**
     * The table public.chart_definition
     */
    public static final ChartDefinition CHART_DEFINITION = de.samply.share.client.core.tables.ChartDefinition.CHART_DEFINITION;

    /**
     * The table public.chart_query_group
     */
    public static final ChartQueryGroup CHART_QUERY_GROUP = de.samply.share.client.core.tables.ChartQueryGroup.CHART_QUERY_GROUP;

    /**
     * The table public.configuration
     */
    public static final Configuration CONFIGURATION = de.samply.share.client.core.tables.Configuration.CONFIGURATION;

    /**
     * The table public.export_definition
     */
    public static final ExportDefinition EXPORT_DEFINITION = de.samply.share.client.core.tables.ExportDefinition.EXPORT_DEFINITION;

    /**
     * The table public.inquiry
     */
    public static final Inquiry INQUIRY = de.samply.share.client.core.tables.Inquiry.INQUIRY;

    /**
     * The table public.inquiryArchive
     */
    public static final Inquiryarchive INQUIRYARCHIVE = de.samply.share.client.core.tables.Inquiryarchive.INQUIRYARCHIVE;

    /**
     * The table public.inquiryResult
     */
    public static final Inquiryresult INQUIRYRESULT = de.samply.share.client.core.tables.Inquiryresult.INQUIRYRESULT;

    /**
     * The table public.log
     */
    public static final Log LOG = de.samply.share.client.core.tables.Log.LOG;

    /**
     * The table public.note
     */
    public static final Note NOTE = de.samply.share.client.core.tables.Note.NOTE;

    /**
     * The table public.registry
     */
    public static final Registry REGISTRY = de.samply.share.client.core.tables.Registry.REGISTRY;

    /**
     * The table public.tokens
     */
    public static final Tokens TOKENS = de.samply.share.client.core.tables.Tokens.TOKENS;
}
