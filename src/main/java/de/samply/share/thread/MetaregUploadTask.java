/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.thread;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.LogLevel;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.domain.Result;
import de.samply.share.client.core.tables.daos.RegistryDao;
import de.samply.share.client.core.tables.pojos.Registry;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.model.EnumConfiguration;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.ViewFields;
import de.samply.share.model.ror.metareg.Metareg;
import de.samply.share.utils.ConfigurationManager;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.MetaregTransformer;
import de.samply.share.utils.Utils;

/**
 * A task that handles the upload to metareg (an implementation of a registry of registries)
 */
public class MetaregUploadTask extends Task {

	/** The upload path on the ror ressource. */
	private final String UPLOAD_PATH = "metaregci/v0/ror";

	/** The mdr dataelementgroup for register specific data. */
	private final String MDR_GROUP_REGISTER = "urn:osse-ror:dataelementgroup:1:2";

	/** The mdr dataelementgroup for biobank specific data. */
	private final String MDR_GROUP_BIOBANK = "urn:osse-ror:dataelementgroup:2:1";

	/** The path to create a new query in local datamanagement. */
	private final String PATH_NEW_QUERY = "/requests";

	/** The path get result from local datamanagement. */
	private final String PATH_GET_RESULT = "/result?page=0";

	/** A constant for the content type */
	private final String CONTENT_TYPE_XML = "application/xml";

	/** A constant for the content type with utf8 encoding */
	private final String CONTENT_TYPE_XML_UTF8 = "application/xml;charset=UTF-8";

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(MetaregUploadTask.class);

	/** The http connector. */
	private transient HttpConnector43 httpConnector;

	/** The mdr client. */
	private MdrClient mdrClient;

	/** The local data management url. */
	private String localDataManagement;

	/** The http proxy url. */
	private String httpProxyUrl;

	/** The http proxy port. */
	private String httpProxyPort;

	/** The proxy username. */
	private String proxyUsername;

	/** The proxy password. */
	private String proxyPassword;

	/** The proxy realm. */
	private String proxyRealm;

	/** The mdr url. */
	private String mdrUrl;

	/** The register_attributes. */
	private ArrayList<String> register_attributes;

	/** The biobank_attributes. */
	private ArrayList<String> biobank_attributes;

	/**
	 * Instantiates a new metareg upload task.
	 */
	public MetaregUploadTask() {
		try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
        }

		try {
			this.localDataManagement = ConfigurationManager.getConfigurationElement(EnumConfiguration.LDM_URL);
			this.httpProxyUrl = ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_URL);
			this.httpProxyPort = ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_PORT);
			this.proxyUsername = ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_USERNAME);
			this.proxyPassword = ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_PASSWORD);
			this.proxyRealm = ConfigurationManager.getConfigurationElement(EnumConfiguration.PROXY_REALM);
			this.mdrUrl = ConfigurationManager.getConfigurationElement(EnumConfiguration.MDR_URL);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		this.mdrClient = new MdrClient(mdrUrl, httpProxyUrl, httpProxyPort, proxyUsername, proxyPassword, proxyRealm);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.samply.share.thread.Task#doIt()
	 */
	@Override
	public TaskResult doIt() throws SQLException {
		setRunning();
		List<Registry> registries = getConnectedRegistries();
		QueryResult queryResult = null;

		for (Registry registry : registries) {
			if (registry.getAppKey() == null || registry.getAppKey().length() < 1) {
				continue;
			}
			try {
				queryResult = getDataFromLDM();
			} catch (MdrConnectionException | ExecutionException | JAXBException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (queryResult == null || queryResult.getEntity() == null || queryResult.getEntity().size() == 0) {
			    Utils.writeLog(LogLevel.INFO, "OSSE-RoR Upload failed", "Query Result from OSSE store rest was empty", "admin", "osse-ror uploadtask");
			    setDone();
			} else {
    			MetaregTransformer metaregTransformer = new MetaregTransformer(mdrClient, register_attributes, biobank_attributes);
    			Metareg metareg = metaregTransformer.transformMetaReg(queryResult);
    			try {
    				uploadDataToRegistry(registry, metareg);
    			} catch (SQLException e) {
    				logger.error("SQL Error!");
    				e.printStackTrace();
    			}
			}
		}
		setDone();
		return new TaskResult(0, "Upload to OSSE-RoR done.");
	}

	/**
	 * Gets the connected registries.
	 *
	 * @return the connected registries or an empty list if there's an error
	 */
	private List<Registry> getConnectedRegistries() {
		List<Registry> registries = new ArrayList<Registry>();

		try (Connection connection = ResourceManager.getConnection() ) {
			Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
			RegistryDao registryDao = new RegistryDao(configuration);
			registries = registryDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return registries;
	}

	/**
	 * Gets the data from local datamanagement.
	 *
	 * @return the data from ldm
	 * @throws MdrConnectionException
	 *             the mdr connection exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws JAXBException
	 *             the JAXB exception
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws SQLException
	 *             the SQL exception
	 */
	private QueryResult getDataFromLDM() throws MdrConnectionException, ExecutionException, JAXBException, UnsupportedEncodingException, SQLException {
		String ldmQuery = createLDMQuery();
		String location = postQueryToLDM(ldmQuery);
		return getResult(location);
	}

	/**
	 * Upload data to a registry of registries (here: metareg).
	 * @param registry
	 *            the registry to which the information will be uploaded
	 * @param metareg
	 *            the information that will be uploaded (see http://metareg.imbei.de/metaregci/v0/ror/xsd ).
	 * @throws SQLException
	 *             the SQL exception
	 */
	private void uploadDataToRegistry(Registry registry, Metareg metareg) throws SQLException {
		HttpEntity entity = null;
		CloseableHttpResponse response = null;

		StringWriter stringWriter = new StringWriter();
		Marshaller marshaller = null;
		int returnCode = 0;

		try {
			final JAXBContext context = JAXBContext.newInstance(de.samply.share.model.ror.metareg.ObjectFactory.class);
			marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			de.samply.share.model.ror.metareg.ObjectFactory objectFactory = new de.samply.share.model.ror.metareg.ObjectFactory();
			marshaller.marshal(objectFactory.createMetareg(metareg), stringWriter);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		try {
			httpConnector.addHttpAuth(registry.getAddress(), registry.getEmail(), registry.getPassword());
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		HttpPut httpPut = new HttpPut(Utils.addTrailingSlash(registry.getAddress()) + UPLOAD_PATH + "/" + registry.getAppKey());
		httpPut.setHeader("Content-Type", CONTENT_TYPE_XML_UTF8);

		try {
			entity = new StringEntity(stringWriter.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		httpPut.setEntity(entity);

		try {
			response = httpConnector.getHttpClient(registry.getAddress()).execute(httpPut);
			returnCode = response.getStatusLine().getStatusCode();
			response.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (returnCode == HttpStatus.SC_NO_CONTENT) {
			Utils.writeLog(LogLevel.INFO, "OSSE-RoR Upload OK", "Upload to OSSE-RoR successful", "admin", "osse-ror uploadtask");
			Date now = new Date();
			long time = now.getTime();
			registry.setLastUploadAt(new Timestamp(time));
			DbUtils.updateRegistry(registry);
			logger.info("Upload to Osse-RoR successful");
		} else {
			Utils.writeLog(LogLevel.ERROR, "OSSE-RoR Upload error", "Upload OSSE-RoR failed: Errorcode " + returnCode, "admin", "osse-ror uploadtask");
			logger.error("Upload to Osse-RoR failed");
		}

	}

	/**
	 * Posts query to local data management.
	 *
	 * @param query
	 *            the query
	 * @return the string
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws SQLException
	 *             the SQL exception
	 */
	private String postQueryToLDM(String query) throws UnsupportedEncodingException, SQLException {
		HttpPost httpPost = new HttpPost(Utils.addTrailingSlash(localDataManagement + PATH_NEW_QUERY));
		HttpEntity entity = new StringEntity(query);

		httpPost.setHeader("Content-Type", CONTENT_TYPE_XML);
		httpPost.setHeader("Accept", CONTENT_TYPE_XML);
		httpPost.setEntity(entity);
		CloseableHttpResponse response;

		CloseableHttpClient httpc = httpConnector.getHttpClient(localDataManagement);

		int retCode;
		try {
			response = httpc.execute(httpPost);
			retCode = response.getStatusLine().getStatusCode();
			response.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

		if (retCode != HttpStatus.SC_CREATED) {
			Utils.writeLog(LogLevel.ERROR, "OSSE-RoR Upload Error", "Request to LDM failed" + ": Statuscode " + retCode, "admin", "osse-ror uploadtask");
			return "";
		}

		Utils.writeLog(LogLevel.INFO, "OSSE-RoR Upload OK", "Request to LDM posted", "admin", "osse-ror uploadtask");

		return response.getFirstHeader("Location").getValue();
	}

	/**
	 * Gets the query result.
	 *
	 * @param location
	 *            the location
	 * @return the result
	 * @throws SQLException
	 *             the SQL exception
	 */
	private QueryResult getResult(String location) throws SQLException {
		CloseableHttpClient httpClient = httpConnector.getHttpClient(localDataManagement);
		HttpGet httpGet = new HttpGet(location + PATH_GET_RESULT);
		httpGet.setHeader("Accept", CONTENT_TYPE_XML);
		CloseableHttpResponse response;
		QueryResult queryResult = null;
		String response_s = "";

		int retCode = 0;

		for (int i = 0; i < 10; i++) {
			try {
				response = httpClient.execute(httpGet);
				retCode = response.getStatusLine().getStatusCode();
				HttpEntity entity = response.getEntity();
				response_s = EntityUtils.toString(entity, Consts.UTF_8);
				EntityUtils.consume(entity);
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (retCode == HttpStatus.SC_OK) {
				break;
			}

			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (retCode == HttpStatus.SC_OK) {
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				StringReader stringReader = new StringReader(response_s);
				JAXBElement<QueryResult> queryResultElement = unmarshaller.unmarshal(new StreamSource(stringReader), QueryResult.class);
				queryResult = queryResultElement.getValue();
			} catch (JAXBException e) {
				logger.error("Error while trying to unmarshal the Query Result");
				e.printStackTrace();
			}
		} else {
			Utils.writeLog(LogLevel.ERROR, "OSSE-RoR Upload Error", "Could not retrieve Query Result from LDM", "admin", "osse-ror uploadtask");
		}

		return queryResult;
	}

	/**
	 * Creates the query to local data management.
	 *
	 * @return the string
	 * @throws MdrConnectionException
	 *             the mdr connection exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws JAXBException
	 *             the JAXB exception
	 */
	private String createLDMQuery() throws MdrConnectionException, ExecutionException, JAXBException {
		View view = new View();

		register_attributes = new ArrayList<String>();
		biobank_attributes = new ArrayList<String>();

		register_attributes = getElementsFromGroupAndSubgroups(register_attributes, MDR_GROUP_REGISTER);
		biobank_attributes = getElementsFromGroupAndSubgroups(biobank_attributes, MDR_GROUP_BIOBANK);

		ViewFields viewFields = new ViewFields();
		for (String s : register_attributes) {
			viewFields.getMdrKey().add(s);
		}
		for (String s : biobank_attributes) {
			viewFields.getMdrKey().add(s);
		}
		view.setViewFields(viewFields);

		return QueryConverter.viewToXml(view).toString();
	}

	/**
	 * Gets the elements from groups and their subgroups.
	 *
	 * @param theList
	 *            the the list
	 * @param groupKey
	 *            the group key
	 * @return the elements from group and subgroups
	 * @throws MdrConnectionException
	 *             the mdr connection exception
	 * @throws ExecutionException
	 *             the execution exception
	 */
	private ArrayList<String> getElementsFromGroupAndSubgroups(ArrayList<String> theList, String groupKey) throws MdrConnectionException, ExecutionException {
		List<Result> result_l = mdrClient.getMembers(groupKey, "de");
		for (Result r : result_l) {
			if (r.getType().equalsIgnoreCase("dataelementgroup")) {
				theList = getElementsFromGroupAndSubgroups(theList, r.getId());
			} else {
				theList.add(r.getId());
			}
		}
		return theList;
	}
}