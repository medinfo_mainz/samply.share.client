/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.thread;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.LogLevel;
import org.jooq.DSLContext;
import org.jooq.Record;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.share.client.core.Tables;
import de.samply.share.client.core.enums.InquiryStatus;
import de.samply.share.client.core.tables.pojos.Broker;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.InquiryExecutionHandler;
import de.samply.share.utils.Utils;

/**
 * A task that queries all registered searchbrokers and downloads the inquiries from them.
 */
public class CollectInquiriesTaskOsse extends AbstractCollectInquiriesTask {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(CollectInquiriesTaskOsse.class);

    public CollectInquiriesTaskOsse() {
        executeTrackedInquiries = false;
    }

    public CollectInquiriesTaskOsse(boolean executeTrackedInquiries) {
        this.executeTrackedInquiries = executeTrackedInquiries;
    }

    /*
     * (non-Javadoc)
     *
     * @see de.samply.share.thread.Task#doIt()
     */
    @Override
    public TaskResult doIt() {
        setRunning();
        HashMap<String, String> configParams = DbUtils.getHttpConfigParams();
        try {
            httpConnector = new HttpConnector43(configParams);
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            setDone();
            return new TaskResult(1, "Error while initializing HTTP connector");
        }
        List<Broker> brokers = getConnectedSearchbrokers();

        for (Broker broker : brokers) {
            if (broker.getAuthcode() == null || broker.getAuthcode().length() < 1) {
                Utils.taskWriteLog(LogLevel.WARN, "InquiryCollection", "Credentials missing for " + broker.getName());
                continue;
            }
            try {
                Map<String, String> inquiries = getInquiryListFromSearchbroker(broker);
                loadAndPersistInquiries(broker, inquiries);
                Utils.taskWriteLog(LogLevel.INFO, "InquiryCollection", broker.getName() + " currently provides " + inquiries.size() + " inquiries");
            } catch (JAXBException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            updateLastChecked(broker);
        }

        if (executeTrackedInquiries) {
            queueTrackedInquiries();
        }

        setDone();
        return new TaskResult(0, "New inquiries collected");
    }

    /**
     * Load and persist inquiries for a given searchbroker.
     *
     * @param broker
     *            the broker
     * @param inquiryIdAndRevision
     *            the inquiry id and revision
     * @return true, if successful
     * @throws JAXBException
     *             the JAXB exception
     * @throws URISyntaxException
     *             the URI syntax exception
     */
    @SuppressWarnings("rawtypes")
    private boolean loadAndPersistInquiries(Broker broker, Map<String, String> inquiryIdAndRevision) throws JAXBException, URISyntaxException {
        Iterator<Entry<String, String>> it = inquiryIdAndRevision.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = it.next();
            if (isInquiryAlreadyInDb(broker.getId().toString(), pairs.getKey().toString(), pairs.getValue().toString())) {
                continue;
            }
            String id = (String) pairs.getKey();
            de.samply.share.model.osse.Inquiry inquiry = getInquiry(broker, id);
            int dbId = persistInquiryWithoutResult(inquiry, broker.getId());
            InquiryExecutionHandler.addInquiryTask(inquiry, dbId);
            it.remove();
        }
        return true;
    }

    /**
     * Gets the inquiry.
     *
     * @param broker
     *            the broker
     * @param id
     *            the id
     * @return the inquiry
     * @throws JAXBException
     *             the JAXB exception
     * @throws URISyntaxException
     *             the URI syntax exception
     */
    private de.samply.share.model.osse.Inquiry getInquiry(Broker broker, String id) throws JAXBException, URISyntaxException {
        HttpHost brokerHost = null;
        try {
            brokerHost = Utils.getAsHttpHost(broker.getAddress());
            URL brokerUrl = new URL(broker.getAddress());

            CloseableHttpClient httpClient = httpConnector.getHttpClient(brokerHost);

            URI uri = new URI(Utils.addTrailingSlash(brokerUrl.getPath()) + INQUIRIES_PATH + "/" + id);
            HttpGet httpGet = new HttpGet(uri.normalize().toString());
            httpGet.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_SAMPLY + " " + broker.getAuthcode());

            int statusCode = 0;
            String responseString = "";
            try (CloseableHttpResponse response = httpClient.execute(brokerHost, httpGet)) {
                statusCode = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, Consts.UTF_8);
                EntityUtils.consume(entity);
            }

            if (statusCode == HttpStatus.SC_OK) {
                try {
                    JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    StringReader stringReader = new StringReader(responseString);
                    JAXBElement<de.samply.share.model.osse.Inquiry> inquiryElement = unmarshaller.unmarshal(new StreamSource(stringReader), de.samply.share.model.osse.Inquiry.class);
                    de.samply.share.model.osse.Inquiry inquiry = inquiryElement.getValue();
                    return inquiry;
                } catch (JAXBException e) {
                    throw new RuntimeException(e);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Persist inquiry without result.
     *
     * @param incomingInquiry
     *            the incoming inquiry
     * @param brokerId
     *            the broker id
     *
     * @return id
     *            the inquiry id
     * @throws JAXBException
     *             the JAXB exception
     */
    private int persistInquiryWithoutResult(de.samply.share.model.osse.Inquiry incomingInquiry, int brokerId) throws JAXBException {
        int id = -1;
        try (Connection connection = ResourceManager.getConnection() ) {
            JAXBContext jaxbContext = JAXBContext.newInstance(de.samply.share.model.osse.Query.class);
            StringWriter stringWriter = new StringWriter();
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(incomingInquiry.getQuery(), stringWriter);

            Date now = new Date();
            long time = now.getTime();

            DSLContext dslContext = ResourceManager.getDSLContext(connection);

            Record record = dslContext.insertInto(Tables.INQUIRY, Tables.INQUIRY.BROKER_ID, Tables.INQUIRY.CRITERIA, Tables.INQUIRY.EXPOSE_LOCATION, Tables.INQUIRY.REVISION,
                    Tables.INQUIRY.INQUIRY_SOURCE_ID, Tables.INQUIRY.STATUS, Tables.INQUIRY.RECEIVED_AT, Tables.INQUIRY.LABEL, Tables.INQUIRY.DESCRIPTION)
                    .values(brokerId, stringWriter.toString(), incomingInquiry.getExposeURL(), Integer.parseInt(incomingInquiry.getRevision()),
                            Integer.parseInt(incomingInquiry.getId()), InquiryStatus.IS_NEW, new Timestamp(time), incomingInquiry.getLabel(), incomingInquiry.getDescription())
                    .returning(Tables.INQUIRY.ID)
                    .fetchOne();

            id = record.getValue(Tables.INQUIRY.ID);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

}
