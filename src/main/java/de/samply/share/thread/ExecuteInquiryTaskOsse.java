/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.thread;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import de.samply.share.client.core.enums.InquiryStatus;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.ViewFields;
import de.samply.share.utils.Config;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.OsseLDMConnector;
import de.samply.share.utils.Utils;

/**
 * Executes one Inquiry (identified by its resource id in the database). That means, posting it to the local datamanagement, handling unknown keys and updating the entry in the db
 */
public class ExecuteInquiryTaskOsse extends AbstractExecuteInquiryTask {

    /** The logger. */
    private Logger logger = Logger.getLogger(ExecuteInquiryTaskOsse.class);

    /** The inquiry. */
    private de.samply.share.model.osse.Inquiry inquiry;

    /** A connector to the local datamanagement */
    private OsseLDMConnector ldmConnector;

    /**
     * Instantiates a new inquiry execution task.
     */
    public ExecuteInquiryTaskOsse(de.samply.share.model.osse.Inquiry inquiry, int dbId) {
        this.inquiry = inquiry;
        this.dbId = dbId;
        this.ldmConnector = new OsseLDMConnector();
    }

    /*
     * (non-Javadoc)
     *
     * @see de.samply.share.thread.Task#doIt()
     */
    @Override
    public TaskResult doIt() throws JAXBException, URISyntaxException, ClientProtocolException, IOException, InterruptedException {
        setRunning();
        String location = postInquiryToLDM(inquiry);
        addResultToInquiry(dbId, location);

        int retryNr = 0;
        boolean success = false;
        do {
            // Check if we have an error, but no mdrkeys. Meaning something else went wrong...maybe wrong mapping?
            if (ldmConnector.getError() != null && (ldmConnector.getError().getMdrKey() == null || ldmConnector.getError().getMdrKey().size() == 0)) {
                logger.error("There was an error from LDM. But no unknown MDR Keys. Something else went wrong. Check the ldm logs.");
                break;
            }

            if (ldmConnector.isResultAvailable(location)) {
                success = true;
                break;
            }

            retryNr += 1;
            logger.debug("LDM won't be queried for the next " + retryInterval + " seconds. " + (retryAttempts - retryNr) + " attempts left until giving up.");
            TimeUnit.SECONDS.sleep(retryInterval);
        } while (retryNr < retryAttempts);

        if (success) {
//            addLocationToInquiry(dbId, location);
            DbUtils.updateInquiryStatusWithId(dbId, InquiryStatus.IS_PROCESSED);
            return new TaskResult(0, "ok");
        } else {
            logger.error("Could not get result from LDM.");
            DbUtils.updateInquiryStatusWithId(dbId, InquiryStatus.IS_LDM_ERROR);
            return new TaskResult(1, "ldm error");
        }
    }

    /**
     * Post inquiry to local datamanagement.
     *
     * @param inquiry
     *            the inquiry
     * @return the string
     */
    private String postInquiryToLDM(de.samply.share.model.osse.Inquiry inquiry) {
        String resultLocation = "";
        try {
            View view = new View();
            view.setQuery(inquiry.getQuery());
            view.setViewFields(new ViewFields());
            resultLocation = ldmConnector.postQueryAndReturnResultLocation(view);
        } catch (SQLException | IOException | JAXBException e) {
            logger.error("Error while posting to local datamanagement");
            e.printStackTrace();
        }
        return resultLocation;
    }
}
