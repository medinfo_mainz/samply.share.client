/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Manages the Executor Service
 */
public class Application {

	/** The Constant NUMWORKERS. the number of threads in the pool of the Executors class */
	private static final int NUMWORKERS = 1;

	/** The pool. */
	private static ExecutorService pool = Executors.newFixedThreadPool(NUMWORKERS);

	/**
	 * Executes a task in the background by means of an {@link ExecutorService}.
	 *
	 * @param t the task to execute
	 * @return An instance of Future that represents the task result.
	 */
	public static Future<TaskResult> execInBackground(Task t){
		return pool.submit(t);
	}

	/**
	 * Quit. Already running tasks are allowed to finish, but no new ones are accepted.
	 */
	public static void quit(){
		pool.shutdown();
	}
}
