/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.thread;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.enums.InquiryStatus;
import de.samply.share.client.core.tables.daos.InquiryDao;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.pojos.Inquiryresult;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.utils.DbUtils;

/**
 * Executes one Inquiry (identified by its resource id in the database). That means, posting it to the local datamanagement, handling unknown keys and updating the entry in the db
 */
public abstract class AbstractExecuteInquiryTask extends Task {

    /** The number of retry attempts */
    protected final int retryAttempts = 60;

    /** The time between two retry attempts in seconds */
    protected final int retryInterval = 30;

    /** TTL for inquiries*/
    protected final long inquiryTTL = 7 * 24 * 60 * 60 * 1000;

    /** The auth header key. */
    final String AUTH_HEADER_KEY = "WWW-AUTHENTICATE";

    /** The auth header value samply. */
    final String AUTH_HEADER_VALUE_SAMPLY = "Samply";

    /** The logger. */
    private Logger logger = Logger.getLogger(AbstractExecuteInquiryTask.class);

    /** The inquiry id as in the database */
    protected int dbId;

    /*
     * (non-Javadoc)
     *
     * @see de.samply.share.thread.Task#doIt()
     */
    @Override
    abstract TaskResult doIt() throws JAXBException, URISyntaxException, ClientProtocolException, IOException, InterruptedException;


    protected void addResultToInquiry(int id, String location) {
        Date now = new Date();
        long time = now.getTime();
        long expires = time + inquiryTTL;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            Inquiryresult inquiryResult = new Inquiryresult();
            inquiryResult.setExecutedAt(new Timestamp(time));
            inquiryResult.setExpiresAt(new Timestamp(expires));
            inquiryResult.setLocation(location);
            int inquiryresultId = DbUtils.insertInquiryresult(inquiryResult);

            InquiryDao inquiryDao = new InquiryDao(configuration);
            Inquiry inquiry = inquiryDao.fetchOneById(id);
            inquiry.setStatus(InquiryStatus.IS_PROCESSING);
            inquiry.setResultId(inquiryresultId);
            inquiryDao.update(inquiry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
