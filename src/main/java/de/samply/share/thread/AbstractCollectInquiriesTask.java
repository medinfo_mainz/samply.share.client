/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.thread;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import de.samply.common.http.HttpConnector43;
import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.daos.BrokerDao;
import de.samply.share.client.core.tables.pojos.Broker;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.records.InquiryRecord;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.InquiryExecutionHandler;
import de.samply.share.utils.Utils;

/**
 * A task that queries all registered searchbrokers and downloads the inquiries from them.
 */
public abstract class AbstractCollectInquiriesTask extends Task {

    /** The inquiries path. */
    protected final String INQUIRIES_PATH = "rest/searchbroker/inquiries";

    /** The auth header key. */
    final String AUTH_HEADER_KEY = "WWW-AUTHENTICATE";

    /** The auth header value samply. */
    final String AUTH_HEADER_VALUE_SAMPLY = "Samply";

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(AbstractCollectInquiriesTask.class);

    /** The http connector. */
    protected transient HttpConnector43 httpConnector;

    protected boolean executeTrackedInquiries;

    public AbstractCollectInquiriesTask() {
        executeTrackedInquiries = false;
    }

    public AbstractCollectInquiriesTask(boolean executeTrackedInquiries) {
        this.executeTrackedInquiries = executeTrackedInquiries;
    }

    /*
     * (non-Javadoc)
     *
     * @see de.samply.share.thread.Task#doIt()
     */
    @Override
    public abstract TaskResult doIt();

    /**
     * Check which inquiries are marked for tracking. Put them in the execution queue.
     *
     * @return
     */
    protected void queueTrackedInquiries() {
        List<Inquiry> inquiries = DbUtils.fetchInquiriesToTrack();
        logger.debug(inquiries.size() + " inquiries to track");

        for (Inquiry inquiry : inquiries) {
            DbUtils.deleteInquiryresult(inquiry.getResultId());
            InquiryExecutionHandler.addInquiryTask(inquiry, inquiry.getId().intValue());
        }

    }

    /**
     * Gets the connected searchbrokers.
     *
     * @return the connected searchbrokers
     */
    protected List<Broker> getConnectedSearchbrokers() {
        List<Broker> brokers = new ArrayList<Broker>();
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            BrokerDao brokerDao = new BrokerDao(configuration);
            brokers = brokerDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return brokers;
    }

    /**
     * Gets the inquiry list from a searchbroker.
     *
     * @param broker
     *            the broker
     * @return the inquiry list from searchbroker
     * @throws URISyntaxException
     *             the URI syntax exception
     */
    protected Map<String, String> getInquiryListFromSearchbroker(Broker broker) throws URISyntaxException {
        Map<String, String> queryIds = new HashMap<String, String>();
        HttpHost brokerHost = null;

        try {
            brokerHost = Utils.getAsHttpHost(broker.getAddress());
            URL brokerUrl = new URL(broker.getAddress());

            CloseableHttpClient httpClient = httpConnector.getHttpClient(brokerHost);

            URI uri = new URI(Utils.addTrailingSlash(brokerUrl.getPath()) + INQUIRIES_PATH);

            HttpGet httpGet = new HttpGet(uri.normalize().toString());
            httpGet.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_SAMPLY + " " + broker.getAuthcode());

            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
            httpGet.setConfig(requestConfig);

            int statusCode = 0;
            String responseString = "";
            try (CloseableHttpResponse response = httpClient.execute(brokerHost, httpGet)) {
                statusCode = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, Consts.UTF_8);
                EntityUtils.consume(entity);
            }

            if (statusCode == HttpStatus.SC_OK) {
                Serializer serializer = new Persister();
                Inquiries inquiries = new Inquiries();
                try {
                    inquiries = serializer.read(Inquiries.class, responseString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (inquiries.getInquiries() == null || inquiries.getInquiries().size() < 1) {
                    return queryIds;
                }
                for (de.samply.share.thread.Inquiries.Inquiry inquiry : inquiries.getInquiries()) {
                    queryIds.put(inquiry.getId(), inquiry.getRevision());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return queryIds;
    }

    /**
     * Checks if is inquiry already in db.
     *
     * @param brokerId
     *            the broker id
     * @param inquiryId
     *            the inquiry id
     * @param revision
     *            the revision
     * @return true, if is inquiry already in db
     */
    protected boolean isInquiryAlreadyInDb(String brokerId, String inquiryId, String revision) {
        boolean found = false;

        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            InquiryRecord inquiryRecord = null;

            inquiryRecord = create.selectFrom(Tables.INQUIRY).where(Tables.INQUIRY.BROKER_ID.equal(Integer.parseInt(brokerId)))
                    .and(Tables.INQUIRY.INQUIRY_SOURCE_ID.equal(Integer.parseInt(inquiryId))).fetchAny();

            if (inquiryRecord != null) {
                found = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }

    /**
     * Update last checked.
     *
     * @param broker
     *            the broker
     */
    protected void updateLastChecked(Broker broker) {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            BrokerDao brokerDao = new BrokerDao(configuration);
            Date now = new Date();
            long time = now.getTime();
            broker.setLastChecked(new Timestamp(time));
            brokerDao.update(broker);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
