/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.rest;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jooq.DSLContext;

import com.google.gson.Gson;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.pojos.Log;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.LogLine;

@Path("/log")
public class LogLines {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(LogLines.class);
    private List<LogLine> logList;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLog() {
        loadLogList();
        Response response;

        StringBuilder stringBuilder = new StringBuilder();
        Gson gson = new Gson();
        String json = gson.toJson(logList);

        stringBuilder.append("{\"data\": ");
        stringBuilder.append(json);
        stringBuilder.append("}");

        response = Response.ok(stringBuilder.toString()).build();
        return response;
    }

    /**
     * Load content of the log table from the database.
     */
    public void loadLogList() {
        List<Log> logList = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            connection.setAutoCommit(false);
            DSLContext select = ResourceManager.getDSLContext(connection);

            logList = select.selectFrom(Tables.LOG)
                                        .orderBy(Tables.LOG.DATED.desc())
                                        .fetchInto(Log.class);
            connection.close();

        }  catch (SQLException e) {
            logger.debug("Error while getting the a connection from the database.");
        }
        populateLogLines(logList);
    }

    /**
     * Populate log lines.
     *
     * @param logEntries the log entries
     */
    private void populateLogLines(List<Log> logEntries) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String date;
        logList = new ArrayList<>();
        LogLine logLine;
        for (Log log : logEntries) {
            logLine = new LogLine();
            date = dateFormat.format(log.getDated());
            logLine.setDated(date);
            logLine.setLevel(log.getLevel());
            logLine.setLogger(log.getLogger());
            logLine.setType(log.getType());
            logLine.setMessage(log.getMessage());
            logLine.setUser(log.getActor());
            logList.add(logLine);
        }
    }
}
