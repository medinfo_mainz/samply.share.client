/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import de.samply.share.common.utils.ProjectInfo;


@Path("/info")
public class Info {

    /** The server header key. */
    final String SERVER_HEADER_KEY = "Server";

    /** The server header value. */
    final String SERVER_HEADER_VALUE = "OSSE.Share.Client/" + ProjectInfo.INSTANCE.getVersionString();

    // TODO: define system status values
    final String SYSTEM_STATUS = "System is healthy";

    /**
     * Gets the name of the searchbroker as given in the config file.
     *
     * @return the name
     */
    @GET
    public Response getInfo() {
        Response response;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{\"OSSE.Share Version\":\"" + ProjectInfo.INSTANCE.getVersionString() + "\",\"status\":\"" + SYSTEM_STATUS + "\"}");
        response = Response.ok(stringBuilder.toString()).header(SERVER_HEADER_KEY, SERVER_HEADER_VALUE).build();
        return response;
    }
}
