/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.rest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.samply.share.client.core.tables.pojos.Actor;
import de.samply.share.client.core.tables.pojos.Tokens;
import de.samply.share.utils.DbUtils;

/**
 * Provides a Rest Ressource to generate temporary signin tokens for applications
 */
@Path("/tokens")
public class TokenGenerator {

	private final String SIGNIN_TOKEN_KEY = "signinToken";
	private final int TOKEN_TTL_MINUTES = 2;
	private final int SECONDS_PER_MINUTE = 60;
	private final int MILISECONDS_PER_SECOND = 1000;

	/**
	 * Creates the signin token.
	 *
	 * @param apiKey
	 *            the api key
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createSigninToken(@HeaderParam("apiKey") String apiKey) {
		Response r = null;
		String token = null;

		Actor actor = DbUtils.getActorByApiKey(apiKey);

		if (actor == null) {
			r = Response.status(Status.UNAUTHORIZED).build();
		} else {
			token = UUID.randomUUID().toString();
			String jsonReply = "{\"" + SIGNIN_TOKEN_KEY +"\":\"" + token + "\"}";

			Tokens tokens = DbUtils.getTokensForActor(actor);
			tokens.setSigninToken(token);
			Date now = new Date();
			long time = now.getTime() + (MILISECONDS_PER_SECOND * SECONDS_PER_MINUTE * TOKEN_TTL_MINUTES);
			tokens.setTokenExpiresAt(new Timestamp(time));
			DbUtils.updateTokens(tokens);

			r = Response.ok(jsonReply).build();
		}
		return r;
	}
}
