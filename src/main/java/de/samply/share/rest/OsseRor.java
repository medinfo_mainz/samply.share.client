/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.samply.share.client.core.tables.pojos.Registry;
import de.samply.share.utils.DbUtils;

/**
 * The Class OsseRor.
 * Provides a REST resource for a local data management that allows to add a registry of registries to samply share
 */
@Path("/osse-ror")
public class OsseRor {

	/**
	 * Allows to add a registry of registries to samply.share.
	 *
	 * @param credentials the information necessary to contact a registry of registries (address, username, password)
	 * @return the http response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response handlePutOsseRor(RorCredentials credentials) {
		Response r = null;
		Registry osse_ror = new Registry();
		osse_ror.setAddress(credentials.url);
		osse_ror.setEmail(credentials.username);
		osse_ror.setPassword(credentials.password);

		Response.Status rs = DbUtils.insertOrUpdateRegistry(osse_ror);

		r = Response.status(rs).build();

		return r;
	}

	/**
	 * The Class RorCredentials.
	 * Holds the information necessary to contact a registry of registries (address, username, password)
	 */
	@SuppressWarnings("unused")
	@XmlRootElement
	private static class RorCredentials {

		/** The url. */
		@XmlElement(name = "address")
		private String url;

		/** The username. */
		@XmlElement(name = "username")
		private String username;

		/** The password. */
		@XmlElement(name = "password")
		private String password;

		/**
		 * Gets the url.
		 *
		 * @return the url
		 */
		public String getUrl() {
			return url;
		}

		/**
		 * Sets the url.
		 *
		 * @param url the new url
		 */
		public void setUrl(String url) {
			this.url = url;
		}

		/**
		 * Gets the username.
		 *
		 * @return the username
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * Sets the username.
		 *
		 * @param username the new username
		 */
		public void setUsername(String username) {
			this.username = username;
		}

		/**
		 * Gets the password.
		 *
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * Sets the password.
		 *
		 * @param password the new password
		 */
		public void setPassword(String password) {
			this.password = password;
		}

	}
}
