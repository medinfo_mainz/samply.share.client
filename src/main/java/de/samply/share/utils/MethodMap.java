/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Holds the mapping between a parameter name and a setter method. To be used for MetaregTranformer.
 */
public class MethodMap {

    /**
     * The Class InstanceHolder.
     */
    private static final class InstanceHolder {

        /** The Constant INSTANCE. */
        static final MethodMap INSTANCE = new MethodMap();
    }

    /** The method map. */
    private HashMap<String, Method> methodMap;

    /**
     * Instantiates a new method map.
     */
    private MethodMap() {
        methodMap = new HashMap<String, Method>();
    }

    /**
     * Gets the single instance of MethodMap.
     *
     * @return single instance of MethodMap
     */
    public static MethodMap getInstance() {
        return InstanceHolder.INSTANCE;
    }

    /**
     * Gets the method map.
     *
     * @return the method map
     */
    public HashMap<String, Method> getMethodMap() {
        return methodMap;
    }

    /**
     * Sets the method map.
     *
     * @param methodMap
     *            the method map
     */
    public void setMethodMap(HashMap<String, Method> methodMap) {
        this.methodMap = methodMap;
    }

    /**
     * Adds a new Entry to the map.
     *
     * @param string
     *            the string
     * @param method
     *            the method
     */
    public void addToMap(String string, Method method) {
        methodMap.put(string, method);
    }

}
