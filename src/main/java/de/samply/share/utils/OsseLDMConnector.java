/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.LogLevel;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.controller.Messages;
import de.samply.share.model.EnumConfiguration;
import de.samply.share.model.osse.Error;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.QueryResultStatistic;
import de.samply.share.model.osse.View;

// TODO: Auto-generated Javadoc
/**
 * Handles Communication between Samply Share and Osse Store Rest.
 */
public class OsseLDMConnector {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(OsseLDMConnector.class);

	/** The ldm url. */
	private String ldmUrl;

    /** The edc url. */
    private String edcUrl;

	/** The http connector. */
	private transient HttpConnector43 httpConnector;

    /** The result path. */
    private static final String RESULT_PATH = "result";

    /** The requests path. */
    private static final String REQUESTS_PATH = "requests";

    /** The stats path. */
    private static final String STATS_PATH = "stats";

    /** The page suffix. */
    private static final String PAGE_SUFFIX = "?page=";

    /** The path to get a token from OSSE edc. */
    private static final String TOKEN_PATH = "/api/searchviewer/getToken";

    /** The header key for the api key. */
    private static final String APIKEY_HEADER = "apiKey";

    /** The key for the signin token in the JSON reply from OSSE edc. */
    private static final String JSON_REPLY_KEY = "signinToken";

    /** The error code from OSSE store-rest when unknown MDR keys are in a query. */
    private static final int ERROR_CODE_UNKNOWN_MDRKEYS = 400;

    /**
     * Gets the url of the local data management (store-rest).
     *
     * @return the url of the local data management (store-rest)
     */
    public String getLdmUrl() {
        return ldmUrl;
    }

    /**
     * Sets the url of the local data management (store-rest).
     *
     * @param ldmUrl the new url of the local data management (store-rest)
     */
    public void setLdmUrl(String ldmUrl) {
        this.ldmUrl = ldmUrl;
    }

    /**
     * Gets the url of the OSSE EDC installation.
     *
     * @return the url of the OSSE EDC installation
     */
    public String getEdcUrl() {
        return edcUrl;
    }

    /**
     * Sets the url of the OSSE EDC installation.
     *
     * @param edcUrl the new url of the OSSE EDC installation
     */
    public void setEdcUrl(String edcUrl) {
        this.edcUrl = edcUrl;
    }

    /** The error. */
    private Error error;

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public Error getError() {
        return error;
    }

    /**
     * Sets the error.
     *
     * @param error the new error
     */
    public void setError(Error error) {
        this.error = error;
    }

    /**
	 * Instantiates a new osse ldm connector.
	 */
	public OsseLDMConnector() {

	    try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
        }

		try {
			this.setLdmUrl(ConfigurationManager.getConfigurationElement(EnumConfiguration.LDM_URL));
            this.setEdcUrl(ConfigurationManager.getConfigurationElement(EnumConfiguration.EDC_URL));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Posts a query to local datamanagement and returns the location of the result.
	 *
	 * @param view the view
	 * @return the string
	 * @throws SQLException the SQL exception
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JAXBException the JAXB exception
	 */
	public String postQueryAndReturnResultLocation(View view) throws SQLException, ClientProtocolException, IOException, JAXBException {
	    setError(null);
	    String resultLocation = "";
        String criteria = getViewAsString(view);
        resultLocation = postQueryAndReturnResultLocation(criteria);

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e1) {
        }

        QueryResultStatistic queryResultStatistic = getStats(resultLocation, 10, 30);
        if (queryResultStatistic != null) {
            return resultLocation;
        }

        if (getError() != null && getError().getErrorCode() == ERROR_CODE_UNKNOWN_MDRKEYS) {
            List<String> missingKeys = getError().getMdrKey();
            setError(null);
            Map<String, String> mdrKeyMap = new HashMap<String, String>();
            int unknownKeyCount = 0;
            for (String unknownKey : missingKeys) {
                String mappedKey = null;
                try {
                    mappedKey = Utils.getNamespaceRepresentation(unknownKey);
                    if (mappedKey == null) {
                        unknownKeyCount++;
                    }
                    if (unknownKey.equalsIgnoreCase(mappedKey)) {
                        // The key is in the namespace, but not used in EDC
                        mdrKeyMap.put(unknownKey, null);
                    } else {
                        mdrKeyMap.put(unknownKey, mappedKey);
                    }
                } catch (ExecutionException e) {
                    logger.warn("Could not map " + unknownKey);
                }
            }
            if (mdrKeyMap.size() > 0) {
                logger.debug("Inquiry contained " + missingKeys.size() + " unknown keys. " + unknownKeyCount + " could not be translated.");
                try {
                    view = substituteAttributesInView(view, mdrKeyMap);
                    criteria = getViewAsString(view);
                    resultLocation = postQueryAndReturnResultLocation(criteria);
                } catch (XPathExpressionException | SAXException | ParserConfigurationException | TransformerException e) {
                    throw new RuntimeException(e);
                }
            }
        }

		return resultLocation;
	}

    /**
     * Posts a query to local datamanagement and returns the location of the result.
     *
     * @param criteria
     *            the criteria
     * @return the string
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws SQLException
     *             the SQL exception
     */
    private String postQueryAndReturnResultLocation(String criteria) throws UnsupportedEncodingException, SQLException {
        HttpPost httpPost = new HttpPost(Utils.addTrailingSlash(ldmUrl) + REQUESTS_PATH);
        HttpEntity entity = new StringEntity(criteria, StandardCharsets.UTF_8.displayName());
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML.toString());
        httpPost.setHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML.toString());

        httpPost.setEntity(entity);
        CloseableHttpResponse response;
        int statusCode;
        try {
            response = httpConnector.getHttpClient(getLdmUrl()).execute(httpPost);
            statusCode = response.getStatusLine().getStatusCode();
            response.close();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

        if (statusCode != HttpStatus.SC_CREATED) {
            Utils.writeLog(LogLevel.ERROR, Messages.getString("UploadTask.ldm"), Messages.getString("UploadTask.requestFailed") + ": Statuscode " + statusCode,
                    "admin", Messages.getString("UploadTask.ldm"));
            return null;
        }

        return response.getFirstHeader(HttpHeaders.LOCATION).getValue();
    }

    /**
     * Gets the full query result.
     *
     * @param view the view
     * @return the full query result
     * @throws ClientProtocolException the client protocol exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws JAXBException the JAXB exception
     * @throws SQLException the SQL exception
     */
    public QueryResult getFullQueryResult(View view) throws ClientProtocolException, IOException, JAXBException, SQLException {
        String resultLocation = postQueryAndReturnResultLocation(view);

        QueryResultStatistic queryResultStatistic = getStats(resultLocation, 20, 1);

        if (queryResultStatistic == null || queryResultStatistic.getTotalSize() < 1) {
            logger.debug("Export was triggered, but no data was found");
            return new QueryResult();
        }

        QueryResult queryResult;
        QueryResult fullQueryResult = new QueryResult();

        for (int i = 0; i < queryResultStatistic.getNumberOfPages(); ++i) {
            queryResult = getQueryResultPage(resultLocation, i);
            fullQueryResult.getEntity().addAll(queryResult.getEntity());
        }

        return fullQueryResult;
    }

    /**
     * Gets the query result page.
     *
     * @param location the location
     * @param page the page
     * @return the query result page
     * @throws ClientProtocolException the client protocol exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws JAXBException the JAXB exception
     */
    public static QueryResult getQueryResultPage(String location, int page) throws ClientProtocolException, IOException, JAXBException {
        int attempts = 10;
        int interval = 10;

        QueryResult queryResult = new QueryResult();

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return queryResult;
        }

        HttpGet httpGet = new HttpGet(Utils.addTrailingSlash(location) + RESULT_PATH + PAGE_SUFFIX + page);

        int count = 0;
        do {
            CloseableHttpResponse response = httpConnector.getHttpClient(location).execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            String entityOutput = EntityUtils.toString(entity, Consts.UTF_8);
            EntityUtils.consume(entity);

            if (200 == statusCode) {
                JAXBContext jaxbContext = JAXBContext.newInstance(QueryResult.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                StringReader xmlOutputReader = new StringReader(entityOutput);
                Source queryResultSource = new StreamSource(xmlOutputReader);
                JAXBElement<QueryResult> queryResultJaxbElement =
                        unmarshaller.unmarshal(queryResultSource, QueryResult.class);
                return queryResultJaxbElement.getValue();
            } else if (422 == statusCode) {
                logger.debug("Got return code 422 - aborting...");
                break;
            }
            try {
                TimeUnit.SECONDS.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (count++ < attempts);

        return queryResult;
    }


    /**
     * Gets the results from page.
     *
     * @param location the location
     * @param page the page
     * @return the results from page
     * @throws ClientProtocolException the client protocol exception
     * @throws SQLException the SQL exception
     * @throws JAXBException the JAXB exception
     * @throws TransformerException the transformer exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     */
    public QueryResult getResultsFromPage(String location, int page) throws ClientProtocolException, SQLException, JAXBException, TransformerException,
            IOException, MdrConnectionException, ExecutionException {
        QueryResultStatistic queryResultStatistic = getStats(location);

        if (queryResultStatistic == null || queryResultStatistic.getTotalSize() < 1) {
            return new QueryResult();
        }
        QueryResult queryResult = getResultPage(location, page);
        return queryResult;
    }

	/**
	 * Gets the query result from a given query location.
	 *
	 * @param location the location
	 * @return the results
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JAXBException the JAXB exception
	 * @throws SQLException the SQL exception
	 * @throws TransformerException the transformer exception
	 */
	public QueryResult getResults(String location) throws ClientProtocolException, IOException, JAXBException, SQLException, TransformerException {
		QueryResultStatistic queryResultStatistic = getStats(location);
		QueryResult queryResult = new QueryResult();

		if (queryResultStatistic.getTotalSize() < 1) {
			return queryResult;
		}

		for (int i = 0; i < queryResultStatistic.getNumberOfPages(); ++i) {
			QueryResult queryResult_ = getResultPage(location, i);
			queryResult.getEntity().addAll(queryResult_.getEntity());
		}

		return queryResult;
	}

	/**
	 * Checks if the query is present in the given location.
	 *
	 * @param location the location where the query should be available
	 * @return true, if is query present
	 * @throws URISyntaxException the URI syntax exception
	 */
	public boolean isQueryPresent(String location) throws URISyntaxException {
		URI uri = new URI(Utils.addTrailingSlash(location) + STATS_PATH);
		HttpGet httpGet = new HttpGet(uri.normalize().toString());

		CloseableHttpResponse response;
		int statusCode = 0;
		try {
			response = httpConnector.getHttpClient(location).execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			response.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (statusCode == HttpStatus.SC_OK) {
			return true;
		}

		return false;
	}

	/**
	 * Gets the stats.
	 *
	 * @param location the location
	 * @return the stats
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JAXBException the JAXB exception
	 */
	public QueryResultStatistic getStats(String location) throws ClientProtocolException, IOException, JAXBException {
	    return getStats(location, 2, 20);
	}

    /**
     * Gets the stats.
     *
     * @param location the location
     * @param attempts the attempts
     * @return the stats
     * @throws ClientProtocolException the client protocol exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws JAXBException the JAXB exception
     */
    public QueryResultStatistic getStats(String location, int attempts) throws ClientProtocolException, IOException, JAXBException {
        return getStats(location, attempts, 20);
    }

	/**
	 * Gets the stats for a query on the given location.
	 *
	 * @param location the location
	 * @param attempts the attempts
	 * @param secondsSleep the seconds sleep
	 * @return the stats
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JAXBException the JAXB exception
	 */
	public QueryResultStatistic getStats(String location, int attempts, int secondsSleep) throws ClientProtocolException, IOException, JAXBException {
        HttpGet httpGet = new HttpGet(Utils.addTrailingSlash(location) + STATS_PATH);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpGet.setConfig(requestConfig);

		int count = 0;
        do {
            CloseableHttpClient httpClient = httpConnector.getHttpClient(location);
            if (httpClient == null) {
                logger.error("Could not get http client for " + location);
                return null;
            }
            try {
                CloseableHttpResponse response = httpClient.execute(httpGet);

                int statusCode = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                String entityOutput = EntityUtils.toString(entity, Consts.UTF_8);
                if (statusCode == HttpStatus.SC_OK) {
                    JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    StringReader stringReader = new StringReader(entityOutput);
                    JAXBElement<de.samply.share.model.osse.QueryResultStatistic> queryResultStatisticElement = unmarshaller.unmarshal(new StreamSource(stringReader), de.samply.share.model.osse.QueryResultStatistic.class);
                    QueryResultStatistic queryResultStatistic = queryResultStatisticElement.getValue();
                    EntityUtils.consume(entity);
                    response.close();
                    return queryResultStatistic;
                } else if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                    JAXBContext jaxbContext = JAXBContext.newInstance(Error.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    setError((Error) jaxbUnmarshaller.unmarshal(new StringReader(entityOutput)));
                    EntityUtils.consume(entity);
                    response.close();
                    return null;
                } else if (statusCode == HttpStatus.SC_ACCEPTED) {
                    try {
                        TimeUnit.SECONDS.sleep(secondsSleep);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    logger.error("Unexpected statuscode " + statusCode + " while getting stats at " + location);
                    return new QueryResultStatistic();
                }
            } catch (SocketTimeoutException ste) {
                logger.warn("Socket Timeout Exception while trying to read stats...trying again");
            }
        } while (++count < attempts);
        logger.error("Timeout while trying to get stats at " + location);
		return new QueryResultStatistic();
	}

	/**
     * Gets the result count for a query on a given location.
     *
     * @param location the location
     * @return the result count
     */
    public Integer getResultCount(String location) {
        QueryResultStatistic queryResultStatistic;
        Integer totalSize;
        try {
            queryResultStatistic = getStats(location);
            totalSize = queryResultStatistic.getTotalSize();
        } catch (IOException | JAXBException | NullPointerException e) {
            totalSize = 0;
        }
        return totalSize;
    }

	/**
	 * Gets the result count for a query on a given location.
	 *
	 * @param location the location
	 * @return the result count
	 */
	public String getResultCountString(String location) {
		return getResultCount(location).toString();
	}

	/**
	 * Gets one result page from a given location.
	 *
	 * @param location the location
	 * @param page the page
	 * @return the result page
	 * @throws SQLException the SQL exception
	 * @throws JAXBException the JAXB exception
	 * @throws TransformerException the transformer exception
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public QueryResult getResultPage(String location, int page) throws SQLException, JAXBException, TransformerException, ClientProtocolException, IOException {
		if (page < 0)
			return null;

		int attempts = 20;

		HttpGet httpGet = new HttpGet(Utils.addTrailingSlash(location) + "result?page=" + page);

		int count = 0;
		do {
			CloseableHttpResponse response = httpConnector.getHttpClient(location).execute(httpGet);
			int retCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, Consts.UTF_8);
			EntityUtils.consume(entity);
			if (200 == retCode) {
				JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				StringReader stringReader = new StringReader(responseString);
				JAXBElement<de.samply.share.model.osse.QueryResult> queryResultElement = unmarshaller.unmarshal(new StreamSource(stringReader), de.samply.share.model.osse.QueryResult.class);
				QueryResult queryResult = queryResultElement.getValue();
				return queryResult;
			} else if (422 == retCode) {
				break;
			}
		} while (++count < attempts);

		Utils.writeLog(LogLevel.ERROR, Messages.getString("UploadTask.queryStats"), Messages.getString("UploadTask.timeOut"), "admin",
				Messages.getString("UploadTask.ldm"));
		return null;
	}

    /**
     * Check if the first result page is available.
     *
     * @param location the location
     * @return true if the result is available or if the stats are available and there are 0 results
     * @throws ClientProtocolException the client protocol exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public boolean isResultAvailable(String location) throws ClientProtocolException, IOException {
        Integer resultCount = getResultCount(location);

        // If the stats are written and the results are empty, return true
        if (resultCount != null && resultCount == 0) {
            return true;
        }

        boolean result = false;
        HttpGet httpGet = new HttpGet(Utils.addTrailingSlash(location) + "result?page=0");
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpGet.setConfig(requestConfig);
        try (CloseableHttpResponse response = httpConnector.getHttpClient(location).execute(httpGet)) {
            int statusCode = response.getStatusLine().getStatusCode();
            if (200 == statusCode) {
                result = true;
            } else {
                result = false;
            }
        } catch (SocketTimeoutException | ConnectTimeoutException e) {
            logger.debug("Connection request to LDM timed out");
            result = false;
        }
        return result;
    }

	/**
	 * Gets the view as string.
	 *
	 * @param view the view
	 * @return the view as string
	 */
	private String getViewAsString(View view) {
		JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(View.class);
            StringWriter stringWriter = new StringWriter();
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(view, stringWriter);
            return stringWriter.toString();
        } catch (JAXBException ex) {
            throw new IllegalArgumentException("Could not serialize View");
        }
	}

	/**
	 * Gets the login token edc.
	 *
	 * @param queryResultId the query result id
	 * @return the login token edc
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getLoginTokenEdc(String queryResultId) throws IOException {
	    String reply = "";
        HttpGet httpGet = new HttpGet(Utils.addTrailingSlash(edcUrl) + TOKEN_PATH + "?queryResultId=" + queryResultId);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpGet.setConfig(requestConfig);
        httpGet.setHeader(APIKEY_HEADER, DbUtils.getExporterApiKey());

        try (CloseableHttpResponse response = httpConnector.getHttpClient(edcUrl).execute(httpGet)) {
            int statusCode = response.getStatusLine().getStatusCode();
            if (200 == statusCode) {
                HttpEntity entity = response.getEntity();
                reply = EntityUtils.toString(entity, Consts.UTF_8);
                EntityUtils.consume(entity);
                JSONObject jsonObject = new JSONObject(reply);
                return jsonObject.getString(JSON_REPLY_KEY);
            } else {
                logger.error("Error getting LoginToken for EDC. StatusCode "  + statusCode);
                reply = "";
            }
        } catch (SocketTimeoutException | ConnectTimeoutException e) {
            logger.debug("Connection request to LDM timed out while requesting a token");
            reply = "";
        } catch (JSONException e) {
            logger.error("Couldn't parse json reply: " + reply);
        }
        return reply;
	}

    /**
     * Substitute attributes in view.
     *
     * @param view the view
     * @param keyMap the key map
     * @return the view
     * @throws JAXBException the JAXB exception
     * @throws SAXException the SAX exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ParserConfigurationException the parser configuration exception
     * @throws XPathExpressionException the x path expression exception
     * @throws TransformerException the transformer exception
     */
    private View substituteAttributesInView(View view, Map<String, String> keyMap) throws JAXBException, SAXException, IOException,
            ParserConfigurationException, XPathExpressionException, TransformerException {
        if (keyMap == null || keyMap.size() < 1) {
            return view;
        }

        Iterator<String> keyIterator = keyMap.keySet().iterator();

        String xml = QueryConverter.viewToXml(view);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));

        XPathFactory xpf = XPathFactory.newInstance();
        XPath xpath = xpf.newXPath();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("//*[text()='");
        stringBuilder.append(keyIterator.next());

        while (keyIterator.hasNext()) {
            stringBuilder.append("' or text()='");
            stringBuilder.append(keyIterator.next());
        }

        stringBuilder.append("']");

        logger.debug(stringBuilder.toString());

        XPathExpression expression = xpath.compile(stringBuilder.toString());

        NodeList matchingKeyNodes = (NodeList) expression.evaluate(document, XPathConstants.NODESET);
        for (int j = 0; j < matchingKeyNodes.getLength(); ++j) {
            logger.error(j);
            Node n = matchingKeyNodes.item(j);
            Node parentNode = n.getParentNode();
            String toReplace = n.getTextContent();
            String replacement = keyMap.get(toReplace);

            if (!toReplace.startsWith("urn")) {
                continue;
            }

            if (parentNode.getNodeName().endsWith("ViewFields")) {
                if (replacement == null) {
                    logger.debug(toReplace + " is unknown. Remove from viewfields");
                    parentNode.removeChild(n);
                } else {
                    logger.debug("Replacing " + toReplace + " in viewfields with " + replacement);
                    n.setTextContent(replacement);
                }
            } else {
                if (replacement == null) {
                    logger.debug("This is part of the query. Remove the Parent(s) as well.");
                    Node anchor = parentNode.getParentNode();
                    if (parentNode.getNodeName().endsWith("Attribute")) {
                        logger.debug("It's an Attribute - so delete the Operator Node as well");
                        anchor = anchor.getParentNode();
                        anchor.removeChild(parentNode.getParentNode());
                    } else {
                        anchor.removeChild(parentNode);
                    }
                } else {
                    logger.debug("Replacing " + toReplace + " in query with " + replacement);
                    n.setTextContent(replacement);
                }
            }
        }

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        StringWriter sw = new StringWriter();
        t.transform(new DOMSource(document), new StreamResult(sw));
        return QueryConverter.xmlToView(sw.toString());

    }

}
