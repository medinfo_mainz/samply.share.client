/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jooq.Condition;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.enums.InquiryStatus;
import de.samply.share.client.core.tables.daos.ActorDao;
import de.samply.share.client.core.tables.daos.BrokerDao;
import de.samply.share.client.core.tables.daos.ExportDefinitionDao;
import de.samply.share.client.core.tables.daos.InquiryDao;
import de.samply.share.client.core.tables.daos.InquiryarchiveDao;
import de.samply.share.client.core.tables.daos.InquiryresultDao;
import de.samply.share.client.core.tables.daos.RegistryDao;
import de.samply.share.client.core.tables.daos.TokensDao;
import de.samply.share.client.core.tables.pojos.Actor;
import de.samply.share.client.core.tables.pojos.Broker;
import de.samply.share.client.core.tables.pojos.ExportDefinition;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.pojos.Inquiryarchive;
import de.samply.share.client.core.tables.pojos.Inquiryresult;
import de.samply.share.client.core.tables.pojos.Registry;
import de.samply.share.client.core.tables.pojos.Tokens;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.model.EnumConfiguration;

/**
 * Offers some helper methods for db access.
 */
public class DbUtils {

    private static final Logger logger = Logger.getLogger(DbUtils.class);

    /**
     * The java keystore file name, which must be located in a configuration directory searched for in the following order: 1. Java OPTs (projectname.confdir) 2. Environment variable ($PROJECTNAME_CONFDIR) 3.
     * OS specific (/etc/projectname or Windows registry entry) 4. User folder ($HOME/.config/projectname) 5. Fallback (classes/, conf/) defined by PROJECT_FALL_BACK_ROOT_PATH
     *
     * If you don't give a PROJECT_NAME, "samply" is used as standard
     */
    public static final String KEYSTORE_FILENAME = "keystore.filename";

    /** The password for the java keystore file. */
    public static final String KEYSTORE_PASSWORD = "keystore.password";
    /**
     * The fallback root directory is the directory containing either a classes/ or a conf/ subdirectory. In JSF projects usually found in the /WEB-INF/ directory which you have to give here (as full path)
     *
     * Only used if you need to set a keystore!
     */
    public static final String PROJECT_FALL_BACK_ROOT_PATH = "project.fallBackRootPath";
    /**
     * The project name is assumed to be the path name for the configuration directory e.g. in linux /etc/projectname/ or in windows to be set into the registry entry HKLM/Software/projectname/
     *
     * Only used if you need to set a keystore!
     */
    public static final String PROJECT_NAME = "project.name";

    /** The proxy realm. */
    public static final String PROXY_REALM = "proxy.realm";

    /** The password for the https proxy. */
    public static final String PROXY_HTTPS_PASSWORD = "proxy.https.password";

    /** The username for the https proxy. */
    public static final String PROXY_HTTPS_USERNAME = "proxy.https.username";

    /** The password for the http proxy. */
    public static final String PROXY_HTTP_PASSWORD = "proxy.http.password";

    /** The username for the http proxy. */
    public static final String PROXY_HTTP_USERNAME = "proxy.http.username";

    /** The port of the https proxy. */
    public static final String PROXY_HTTPS_PORT = "proxy.https.port";

    /** The hostname of the https proxy. */
    public static final String PROXY_HTTPS_HOST = "proxy.https.host";

    /** The port of the http proxy. */
    public static final String PROXY_HTTP_PORT = "proxy.http.port";

    /** The hostname of the http proxy. */
    public final static String PROXY_HTTP_HOST = "proxy.http.host";

    /** The hostname of the http proxy. */
    public final static String USER_AGENT = "http.useragent";

    /**
     * Gets the tokens for actor.
     *
     * @param actor
     *            the actor
     * @return the tokens for actor
     */
    public static Tokens getTokensForActor(Actor actor) {
        TokensDao tokensDao = null;
        Tokens tokens = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            tokensDao = new TokensDao(configuration);
            tokens = tokensDao.fetchOneById(actor.getTokensId());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tokens;
    }

    /**
     * Update tokens.
     *
     * @param tokens
     *            the tokens
     * @return true, if successful
     */
    public static boolean updateTokens(Tokens tokens) {
        TokensDao tokensDao = null;
        boolean success = false;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            tokensDao = new TokensDao(configuration);
            tokensDao.update(tokens);
            success = true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Gets the actor by api key.
     *
     * @param apiKey
     *            the api key
     * @return the actor by api key
     */
    public static Actor getActorByApiKey(String apiKey) {
        ActorDao actorDao = null;
        Actor actor = null;
        int tokensId = -1;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            DSLContext create = ResourceManager.getDSLContext(connection);

            Record record = create.select(Tables.TOKENS.ID).from(Tables.ACTOR).join(Tables.TOKENS).onKey().where(Tables.TOKENS.APIKEY.equal(apiKey)).fetchOne();

            if (record != null) {
                tokensId = record.getValue(Tables.TOKENS.ID);

                actorDao = new ActorDao(configuration);
                actor = actorDao.fetchOneByTokensId(tokensId);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actor;
    }

    /**
     * Update actor.
     *
     * @param actor
     *            the actor
     * @return true, if successful
     */
    public static boolean updateActor(Actor actor) {
        ActorDao actorDao = null;
        boolean success = false;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            actorDao = new ActorDao(configuration);
            actorDao.update(actor);
            success = true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Gets the actor by token.
     *
     * @param token
     *            the token
     * @return the actor by token
     */
    public static Actor getActorByToken(String token) {
        ActorDao actorDao = null;
        Actor actor = null;
        int tokensId = -1;

        Date now = new Date();
        long time = now.getTime();
        Timestamp timestamp = new Timestamp(time);

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            DSLContext create = ResourceManager.getDSLContext(connection);

            Record record = create.select(Tables.TOKENS.ID).from(Tables.ACTOR).join(Tables.TOKENS).onKey().where(Tables.TOKENS.SIGNIN_TOKEN.equal(token))
                    .and(Tables.TOKENS.TOKEN_EXPIRES_AT.greaterOrEqual(timestamp)).fetchOne();

            if (record != null) {
                tokensId = record.getValue(Tables.TOKENS.ID);

                actorDao = new ActorDao(configuration);
                actor = actorDao.fetchOneByTokensId(tokensId);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actor;
    }

    /**
     * Update registry.
     *
     * @param registry
     *            the registry
     */
    public static void updateRegistry(Registry registry) {
        RegistryDao registryDao = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            registryDao = new RegistryDao(configuration);
            registryDao.update(registry);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update broker.
     *
     * @param broker
     *            the broker
     */
    public static void updateBroker(Broker broker) {
        BrokerDao brokerDao = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            brokerDao = new BrokerDao(configuration);
            brokerDao.update(broker);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update inquiry.
     *
     * @param inquiry
     *            the inquiry
     * @return true, if successful
     */
    public static boolean updateInquiry(Inquiry inquiry) {
        InquiryDao inquiryDao = null;
        boolean success = false;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            inquiryDao = new InquiryDao(configuration);
            inquiryDao.update(inquiry);
            success = true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Update inquiry status.
     *
     * @param id the id of the inquiry
     * @param status the status
     * @return true, if successful
     */
    public static boolean updateInquiryStatusWithId(int id, InquiryStatus status) {
        InquiryDao inquiryDao = null;
        Inquiry inquiry = null;
        boolean success = false;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            inquiryDao = new InquiryDao(configuration);
            inquiry = inquiryDao.fetchOneById(id);
            inquiry.setStatus(status);
            inquiryDao.update(inquiry);
            success = true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Insert or update registry.
     *
     * @param registry
     *            the registry
     * @return the response. status
     */
    public static Response.Status insertOrUpdateRegistry(Registry registry) {
        RegistryDao registryDao = null;
        Response.Status responseStatus = null;

        if (registry.getAddress() == null || registry.getEmail() == null || registry.getPassword() == null || registry.getAddress().length() < 1
                || registry.getEmail().length() < 1 || registry.getPassword().length() < 1) {

            return Response.Status.BAD_REQUEST;
        }

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            DSLContext create = ResourceManager.getDSLContext(connection);

            Record record = create.selectFrom(Tables.REGISTRY).where(Tables.REGISTRY.ADDRESS.equal(registry.getAddress())).fetchAny();

            // Record not found - insert
            if (record == null) {
                registryDao = new RegistryDao(configuration);
                registryDao.insert(registry);
                responseStatus = Response.Status.CREATED;
            } else {
                String oldMail = record.getValue(Tables.REGISTRY.EMAIL);
                String oldPass = record.getValue(Tables.REGISTRY.PASSWORD);
                int oldId = record.getValue(Tables.REGISTRY.ID);
                String oldAppykey = record.getValue(Tables.REGISTRY.APP_KEY);

                // Record found with same credentials - don't do anything
                if (oldMail.equalsIgnoreCase(registry.getEmail()) && oldPass.equalsIgnoreCase(registry.getPassword())) {
                    responseStatus = Response.Status.NO_CONTENT;
                } else { // Credentials changed - update it
                    registryDao = new RegistryDao(configuration);
                    registry.setId(oldId);
                    registry.setAppKey(oldAppykey);
                    registryDao.update(registry);
                    responseStatus = Response.Status.NO_CONTENT;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return responseStatus;
    }

    /**
     * Insert inquiry result.
     *
     * @param inquiryresult the inquiry result
     * @return the id of the newly inserted inquiry result
     */
    public static int insertInquiryresult(Inquiryresult inquiryresult) {
        int id = -1;
        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            Record record = create.insertInto(Tables.INQUIRYRESULT, Tables.INQUIRYRESULT.LOCATION, Tables.INQUIRYRESULT.EXPIRES_AT, Tables.INQUIRYRESULT.EXECUTED_AT)
                    .values(inquiryresult.getLocation(), inquiryresult.getExpiresAt(), inquiryresult.getExecutedAt()).returning(Tables.INQUIRYRESULT.ID).fetchOne();
            id = record.getValue(Tables.INQUIRYRESULT.ID);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Insert export.
     *
     * @param exportDefinition
     *            the export
     */
    public static void insertExport(ExportDefinition exportDefinition) {
        ExportDefinitionDao exportDefinitionDao = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            exportDefinitionDao = new ExportDefinitionDao(configuration);
            exportDefinitionDao.insert(exportDefinition);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Count rors.
     *
     * @return the long
     */
    public static long countRors() {
        RegistryDao registryDao = null;
        long count = 0;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            registryDao = new RegistryDao(configuration);
            count = registryDao.count();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Fetch actor by name. Not case sensitive.
     *
     * @param actorName the actor name
     * @return the actor
     */
    public static Actor fetchActorByNameIgnoreCase(String actorName) {
        Actor actor = null;
        Record record = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            record = create.select().from(Tables.ACTOR).where(Tables.ACTOR.USERNAME.equalIgnoreCase(actorName)).fetchOne();
            if (record != null) {
                actor = record.into(Actor.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actor;
    }

    /**
     * Fetch active inquiries ordered by date.
     *
     * @return the list of inquiries
     */
    public static List<Inquiry> fetchInquiriesOrderByDate() {
        List<Inquiry> inquiries = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            inquiries = create
                    .select()
                    .from(Tables.INQUIRY)
                    .where(Tables.INQUIRY.STATUS.in(InquiryStatus.IS_NEW, InquiryStatus.IS_PROCESSED,
                            InquiryStatus.IS_PROCESSING, InquiryStatus.IS_LDM_ERROR)).orderBy(Tables.INQUIRY.RECEIVED_AT.desc())
                    .fetchInto(Inquiry.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inquiries;
    }

    /**
     * Fetch inquiries order by date.
     *
     * @param conditions additional conditions to apply to the where clause
     * @return the list of matching inquiries
     */
    public static List<Inquiry> fetchInquiriesOrderByDate(Condition... conditions) {
        List<Inquiry> inquiries = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            inquiries = create.select().from(Tables.INQUIRY).where(conditions).orderBy(Tables.INQUIRY.RECEIVED_AT.desc()).fetchInto(Inquiry.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inquiries;
    }

    /**
     * Fetch inquiries to track.
     *
     * @return the list of inquiries that have tracking activated
     */
    public static List<Inquiry> fetchInquiriesToTrack() {
        List<Inquiry> inquiries = null;
        InquiryDao inquiryDao = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            inquiryDao = new InquiryDao(configuration);
            inquiries = inquiryDao.fetchByTrack(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inquiries;
    }

    /**
     * Delete inquiry result.
     *
     * @param id the id of the inquiry result to delete
     */
    public static void deleteInquiryresult(int id) {
        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            create.delete(Tables.INQUIRYRESULT).where(Tables.INQUIRYRESULT.ID.equal(id)).execute();
        } catch (SQLException e) {
           logger.error("Could not delete inquiry result with id " + id);
        }
    }

    /**
     * Gets the http config params.
     *
     * @return a hashmap with the http config params
     */
    public static HashMap<String, String> getHttpConfigParams() {
        HashMap<String, String> configParams = new HashMap<String, String>();

        try {
            configParams.put(PROXY_HTTP_HOST, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_URL));
            configParams.put(PROXY_HTTP_PORT, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_PORT).trim());
            configParams.put(PROXY_HTTP_USERNAME, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_USERNAME));
            configParams.put(PROXY_HTTP_PASSWORD, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTP_PROXY_PASSWORD));
            configParams.put(PROXY_HTTPS_HOST, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTPS_PROXY_URL));
            configParams.put(PROXY_HTTPS_PORT, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTPS_PROXY_PORT).trim());
            configParams.put(PROXY_HTTPS_USERNAME, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTPS_PROXY_USERNAME));
            configParams.put(PROXY_HTTPS_PASSWORD, ConfigurationManager.getConfigurationElement(EnumConfiguration.HTTPS_PROXY_PASSWORD));
            configParams.put(PROXY_REALM, ConfigurationManager.getConfigurationElement(EnumConfiguration.PROXY_REALM));
            configParams.put(KEYSTORE_PASSWORD, null);
            configParams.put(KEYSTORE_FILENAME, null);
            configParams.put(PROJECT_NAME, ProjectInfo.INSTANCE.getProjectName());
            configParams.put(PROJECT_FALL_BACK_ROOT_PATH, "/");
            configParams.put(USER_AGENT, "Samply.Share/" + ProjectInfo.INSTANCE.getVersionString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return configParams;
    }

    /**
     * Gets the result location for an inquiry.
     *
     * @param inquiry the inquiry
     * @return the result location of that inquiry
     */
    public static String getResultLocationByInquiry(Inquiry inquiry) {
        String ret = null;
        InquiryresultDao inquiryresultDao = null;
        Inquiryresult inquiryresult = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            inquiryresultDao = new InquiryresultDao(configuration);
            inquiryresult = inquiryresultDao.fetchOneById(inquiry.getResultId());
            if (inquiryresult != null) {
                ret = inquiryresult.getLocation();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Gets the result for an inquiry.
     *
     * @param inquiry the inquiry
     * @return the result for that inquiry
     */
    public static Inquiryresult getResultByInquiry(Inquiry inquiry) {
        Inquiryresult ret = null;
        InquiryresultDao inquiryresultDao = null;
        Inquiryresult inquiryresult = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            inquiryresultDao = new InquiryresultDao(configuration);
            inquiryresult = inquiryresultDao.fetchOneById(inquiry.getResultId());
            ret = inquiryresult;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Gets the archive information of an inquiry.
     *
     * @param inquiry the inquiry
     * @return the archive information of that inquiry
     */
    public static Inquiryarchive getArchiveByInquiry(Inquiry inquiry) {
        Inquiryarchive ret = null;
        InquiryarchiveDao inquiryarchiveDao = null;
        Inquiryarchive inquiryarchive = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            inquiryarchiveDao = new InquiryarchiveDao(configuration);
            inquiryarchive = inquiryarchiveDao.fetchOneById(inquiry.getArchiveId());
            ret = inquiryarchive;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Change password for user.
     *
     * @param user the user
     * @param newPasswordClear the new password
     * @return true, if successful
     */
    public static boolean changePasswordForUser(Actor user, String newPasswordClear) {
        ActorDao actorDao = null;
        boolean ret = true;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            String newPassHashed = BCrypt.hashpw(newPasswordClear, BCrypt.gensalt());

            actorDao = new ActorDao(configuration);
            user.setPassword(newPassHashed);
            actorDao.update(user);
        } catch (SQLException e) {
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }

    /**
     * Insert Actor.
     *
     * @param actor
     *            the new actor
     */
    public static void insertActor(Actor actor) {
        ActorDao actorDao = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            actorDao = new ActorDao(configuration);
            actorDao.insert(actor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a list of inquiries that require notification mails to be sent
     *
     * @return the list of inquiries that require notification mails to be sent
     */
    public static List<Inquiry> getInquiriesForNotifications() {
        List<Inquiry> inquiries = null;

        try (Connection connection = ResourceManager.getConnection()) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            inquiries = create.select().from(Tables.INQUIRY).join(Tables.INQUIRYRESULT).onKey()
                    .where(Tables.INQUIRYRESULT.NOTIFICATION_SENT.equal(Boolean.FALSE)).and(Tables.INQUIRYRESULT.SIZE.greaterThan(0)).fetchInto(Inquiry.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inquiries;
    }

    /**
     * Mark inquiries for those a notification mail was sent.
     *
     * @param inquiries a list of inquiries for which a notification mail was sent
     */
    public static void setNotificationSentForInquiries(List<Inquiry> inquiries) {
        List<Integer> ids = new ArrayList<Integer>();
        for (Inquiry inquiry : inquiries) {
            ids.add(inquiry.getResultId());
        }

        try (Connection connection = ResourceManager.getConnection()) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            create.update(Tables.INQUIRYRESULT).set(Tables.INQUIRYRESULT.NOTIFICATION_SENT, Boolean.TRUE).where(Tables.INQUIRYRESULT.ID.in(ids)).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update result counts.
     */
    public static void updateResultCounts() {
        List<Inquiry> inquiries = null;
        InquiryDao inquiryDao = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            inquiryDao = new InquiryDao(configuration);
            inquiries = inquiryDao.findAll();

            for (Inquiry inquiry : inquiries) {
                updateResultCountForInquiryId(inquiry.getId());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the result count for a given inquiry by its id.
     *
     * @param inquiryId
     *            the inquiry id
     * @return the result count
     */
    private static void updateResultCountForInquiryId(int inquiryId) {
        Integer results = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            InquiryDao inquiryDao = new InquiryDao(configuration);
            Inquiry inquiry = inquiryDao.fetchOneById(inquiryId);

            if (inquiry.getStatus().equals(InquiryStatus.IS_LDM_ERROR)) {
                return;
            }

            Inquiryresult inquiryresult = DbUtils.getResultByInquiry(inquiry);

            if (inquiryresult == null || inquiryresult.getLocation() == null || inquiryresult.getLocation().length() < 1) {
                logger.trace("No result found (yet)");
                return;
            }

            if (inquiryresult.getSize() != null) {
                return;
            } else {
                OsseLDMConnector ldmConnector = new OsseLDMConnector();
                results = ldmConnector.getResultCount(inquiryresult.getLocation());

                if (results != null) {
                    inquiryresult.setSize(results);
                    InquiryresultDao inquiryresultDao = new InquiryresultDao(configuration);
                    inquiryresultDao.update(inquiryresult);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the api key for the exporter role.
     *
     * @return the api key
     */
    public static String getExporterApiKey() {
        Tokens token = null;

        try (Connection connection = ResourceManager.getConnection()) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            token = create.select().from(Tables.ACTOR).join(Tables.TOKENS).onKey().where(Tables.ACTOR.USERNAME.equal("exporter")).fetchOneInto(Tokens.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return token.getApikey();
    }

    /**
     * Insert archive information for an inquiry.
     *
     * @param inquiryArchive the archive information
     * @return the id of the archive information
     */
    public static int insertInquiryArchive(Inquiryarchive inquiryArchive) {
        int id = -1;
        try (Connection connection = ResourceManager.getConnection() ) {
            DSLContext create = ResourceManager.getDSLContext(connection);

            Record record = create.insertInto(Tables.INQUIRYARCHIVE, Tables.INQUIRYARCHIVE.NOTE, Tables.INQUIRYARCHIVE.TYPE)
                    .values(inquiryArchive.getNote(), inquiryArchive.getType()).returning(Tables.INQUIRYARCHIVE.ID).fetchOne();
            id = record.getValue(Tables.INQUIRYARCHIVE.ID);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Delete inquiry archive information.
     *
     * @param inquiryArchive the inquiry archive information
     */
    public static void deleteInquiryArchive(Inquiryarchive inquiryArchive) {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryarchiveDao inquiryArchiveDao = new InquiryarchiveDao(configuration);
            inquiryArchiveDao.delete(inquiryArchive);
        }catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
