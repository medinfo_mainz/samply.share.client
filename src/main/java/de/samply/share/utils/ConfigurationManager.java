/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.client.core.tables.daos.ConfigurationDao;
import de.samply.share.client.core.tables.pojos.Configuration;
import de.samply.share.model.EnumConfiguration;

/**
 * Provides access to configuration elements stored in the database.
 */
public class ConfigurationManager {

    private static final Logger logger = Logger.getLogger(ConfigurationManager.class);

	/**
	 * Gets a configuration element.
	 *
	 * @param configurationElement The configuration element to get.
	 * @return Value of the requested configuration element.
	 * @throws SQLException If a database error occurs.
	 */
	public static String getConfigurationElement(EnumConfiguration configurationElement) throws SQLException {
	    String configurationValue = "";
		try (Connection connection = ResourceManager.getConnection()) {
    		connection.setAutoCommit(false);
            org.jooq.Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            ConfigurationDao configurationDao = new ConfigurationDao(configuration);
            Configuration findWhereNameEquals = configurationDao.fetchOneByName(configurationElement.toString());

            if (findWhereNameEquals != null) {
                configurationValue = findWhereNameEquals.getValue();
            } else {
                logger.warn("Configuration element not found: " + configurationElement.toString());
            }
    		connection.commit();
		}
		return configurationValue;
	}
}
