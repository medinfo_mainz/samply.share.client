/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.util.Date;

import org.jooq.Converter;

/**
 * Used in JOOQ to be able to use java.util.Date values in the pojos.
 * See {@code <forcedTypes>} in jooq.xml.
 */
public class JooqDateConverter implements Converter<java.sql.Date, Date> {

    private static final long serialVersionUID = 3748291657992911015L;

    @Override
    public Date from(java.sql.Date databaseObject) {
        if (databaseObject == null) {
            return null;
        }
        else {
            return new Date(databaseObject.getTime());
        }
    }

    @Override
    public java.sql.Date to(Date date) {
        if (date == null) {
            return null;
        }
        else {
            return new java.sql.Date(date.getTime());
        }
    }

    @Override
    public Class<java.sql.Date> fromType() {
        return java.sql.Date.class;
    }

    @Override
    public Class<Date> toType() {
        return Date.class;
    }
}
