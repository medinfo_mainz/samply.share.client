/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.tables.daos.ConfigurationDao;
import de.samply.share.client.core.tables.pojos.Configuration;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.EnumConfiguration;
import de.samply.share.thread.CollectInquiriesTaskOsse;

/**
 * Handles the scheduled inquiry collection
 */
public class ScheduledInquiryCollection {

	/** The Constant DAY_IN_MINUTES. */
	private static final int DAY_IN_MINUTES = 1440;

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(ScheduledInquiryCollection.class);

	/** The Constant scheduler. */
	private final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	/**
	 * Handler created when a task is scheduled. Enables cancelling that task.
	 */
	private static ScheduledFuture<?> scheduledInquiryCollectionHandler;

	/**
	 * If the automatic upload is activated, schedule the upload tasks.
	 */
	public static void scheduleInquiryCollectionFromConfig() {
	    try (Connection connection = ResourceManager.getConnection() ) {
			org.jooq.Configuration jooqConfiguration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
			ConfigurationDao configurationDao = new ConfigurationDao(jooqConfiguration);
			Configuration configuration = configurationDao.fetchOneByName(EnumConfiguration.COLLECT_INQUIRIES_AT.toString());
			String collectInquiriresAt = configuration.getValue();
			ScheduledInquiryCollection.reScheduleInquiryCollection(collectInquiriresAt);
		} catch (SQLException e) {
			logger.debug("Error reading the configurations from the database.");
			e.printStackTrace();
		}
	}

	/**
	 * Cancels already scheduled inquiry collections if any and schedules new inquiry collections according to the parameters.
	 *
	 * @param timeStr
	 *            String representation of the time at which the inquiry collection is to be scheduled every day, e.g. "04:30"
	 */
	public static void reScheduleInquiryCollection(final String timeStr) {
		cancelScheduledInquiryCollection();
		scheduleInquiryCollection(timeStr);
	}

	/**
	 * Shutdown service.
	 */
	public static void shutdownService() {
		scheduler.shutdownNow();
		try {
			scheduler.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cancels already scheduled inquiry collection if any.
	 */
	private static void cancelScheduledInquiryCollection() {
		if (scheduledInquiryCollectionHandler != null) {
			logger.debug("Cancelling previously scheduled inquiry collection...");
			scheduledInquiryCollectionHandler.cancel(false);
		}
	}

	/**
	 * Schedule an inquiry collection task.
	 *
	 * @param timeStr
	 *            the time str
	 */
	private static void scheduleInquiryCollection(final String timeStr) {
		logger.debug("Scheduling the inquiry collection...");

		String[] split = timeStr.split(":");
		int scheduledHour = Integer.valueOf(split[0]).intValue();
		int scheduledMinutes = Integer.valueOf(split[1]).intValue();

		// runnable upload action
		final Runnable runnableInquiryCollection = new Runnable() {
			@Override
			public void run() {
				SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
				Date now = new Date();
				logger.debug("Running scheduled inquiry collection! Started at " + sdf.format(now));
				// For the scheduled inquiry collection, the flag to re-execute tracked inquiries will be set
				CollectInquiriesTaskOsse collectInquiriesTask = new CollectInquiriesTaskOsse(true);
				collectInquiriesTask.doIt();
			}
		};

		int initialDelay = 0;

		// get the minutes passed since the beginning of the day
		Calendar calendar = Calendar.getInstance();
		long now = calendar.getTimeInMillis();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		long msSinceMidnight = now - calendar.getTimeInMillis();
		int minSinceMidnight = (int) (msSinceMidnight / (60 * 1000));

		// scheduled time since midnight
		Calendar scheduled = Calendar.getInstance();
		scheduled.set(Calendar.HOUR_OF_DAY, scheduledHour);
		scheduled.set(Calendar.MINUTE, scheduledMinutes);
		long scheduledMsSinceMidnight = scheduled.getTimeInMillis() - calendar.getTimeInMillis();
		int scheduledMinSinceMidnight = (int) (scheduledMsSinceMidnight / (60 * 1000));

		// calculate delay to next scheduled run
		if (scheduledMinSinceMidnight > minSinceMidnight) {
			initialDelay = scheduledMinSinceMidnight - minSinceMidnight;
		} else {
			initialDelay = (DAY_IN_MINUTES - minSinceMidnight) + scheduledMinSinceMidnight;
		}

		logger.debug("Next scheduled inquiry collection will run in " + initialDelay + " minutes...");

		scheduledInquiryCollectionHandler = scheduler.scheduleAtFixedRate(runnableInquiryCollection, initialDelay, DAY_IN_MINUTES, TimeUnit.MINUTES);

	}
}