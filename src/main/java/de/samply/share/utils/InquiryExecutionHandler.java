/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.thread.ExecuteInquiryTaskOsse;

public class InquiryExecutionHandler {

    private static final Logger logger = Logger.getLogger(InquiryExecutionHandler.class);

    /** The amount of workers for the inquiry scheduler. */
    private static final int NR_THREADS = 10;

    /** A scheduled executor service to execute inquiries. */
    private static ScheduledExecutorService inquiryScheduler = Executors.newScheduledThreadPool(NR_THREADS);

    /** Add a new inquiry collection task to the queue */
    public static void addInquiryTask(de.samply.share.model.osse.Inquiry inquiry, int dbId) {
        inquiryScheduler.submit(new ExecuteInquiryTaskOsse(inquiry, dbId));
    }

    public static void addInquiryTask(de.samply.share.client.core.tables.pojos.Inquiry inquiry, int dbId) {
        addInquiryTaskOsse(inquiry, dbId);
    }

    private static void addInquiryTaskOsse(de.samply.share.client.core.tables.pojos.Inquiry inquiry, int dbId) {
        de.samply.share.model.osse.Inquiry inq = new de.samply.share.model.osse.Inquiry();
        try {
            inq.setQuery(QueryConverter.xmlToQuery(inquiry.getCriteria()));
            inquiryScheduler.submit(new ExecuteInquiryTaskOsse(inq, dbId));
        } catch (JAXBException e) {
            logger.error("Could not add inquiry execution task due to JAXB error");
        }
    }

    public static void shutdownService() {
        inquiryScheduler.shutdownNow();
    }
}
