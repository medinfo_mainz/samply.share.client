/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Slot;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.ror.metareg.Biomaterialbank;
import de.samply.share.model.ror.metareg.Countries;
import de.samply.share.model.ror.metareg.Metareg;
import de.samply.share.model.ror.metareg.Register;
import de.samply.share.model.ror.metareg.Register.NumberOfCases;

/**
 * Transforms the MDR-based information from local datamanagement to non-mdr related keys that metareg understands.
 */
public class MetaregTransformer {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(MetaregTransformer.class);

	/** The Constant CLASSNAME_BIOMATERIALBANK. */
	private final static String CLASSNAME_BIOMATERIALBANK = "de.samply.share.model.ror.metareg.Biomaterialbank";

	/** The Constant CLASSNAME_REGISTER. */
	private final static String CLASSNAME_REGISTER = "de.samply.share.model.ror.metareg.Register";

	/** The Constant PREFIX_BIOMATERIALBANK. */
	private final static String PREFIX_BIOMATERIALBANK = "bmb_";

	/** The Constant PREFIX_REGISTER. */
	private final static String PREFIX_REGISTER = "reg_";

	/** The Constant SLOT_NAME. */
	private final static String SLOT_NAME = "ror_upload_item_id";

	/** The mdr client. */
	private MdrClient mdrClient;

	/** The register_attributes. */
	private ArrayList<String> register_attributes;

	/** The biomaterialbank_attributes. */
	private ArrayList<String> biomaterialbank_attributes;

	/**
	 * Instantiates a new metareg transformer.
	 *
	 * @param mdrClient the mdr client
	 * @param registerAttributes the register attributes
	 * @param bioMaterialBankAttributes the attributes of the biobank
	 */
	public MetaregTransformer(MdrClient mdrClient, ArrayList<String> registerAttributes, ArrayList<String> bioMaterialBankAttributes) {
		this.mdrClient = mdrClient;
		this.register_attributes = registerAttributes;
		this.biomaterialbank_attributes = bioMaterialBankAttributes;
	}

	/**
	 * Replace the mdr keys with the corresponding values metareg understands. This information is stored in the slot "ror_upload_item_id" in the mdr
	 *
	 * @param queryResult the query result
	 * @return the metareg object with the translated keys
	 */
	public Metareg transformMetaReg(QueryResult queryResult) {
		Metareg metareg = new Metareg();
		Register register = new Register();
		Biomaterialbank bioMaterialBank = new Biomaterialbank();

		Class<?> cReg = null;
		Class<?> cBio = null;
		try {
			cReg = Class.forName(CLASSNAME_REGISTER);
			cBio = Class.forName(CLASSNAME_BIOMATERIALBANK);
		} catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
		}

		updateMethodMap(cReg, PREFIX_REGISTER);
		updateMethodMap(cBio, PREFIX_BIOMATERIALBANK);

		List<Attribute> attributes = null;
		try {
			attributes = queryResult.getEntity().get(0).getAttribute();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if (attributes == null) {
			logger.error("No Data found in QueryResult");
			return metareg;
		}

		Map<String, String> attributeTranslations = new HashMap<String, String>();
		try {
			attributeTranslations = getTranslations(attributes);
		} catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e1) {
            throw new RuntimeException(e1);
		}

		for (Attribute attribute : attributes) {
			if (attribute.getValue().isNil()) {
				continue; // skip empty elements
			}

			String prefix = "";
			if (register_attributes.contains(attribute.getMdrKey())) {
				prefix = PREFIX_REGISTER;
			} else if (biomaterialbank_attributes.contains(attribute.getMdrKey())) {
				prefix = PREFIX_BIOMATERIALBANK;
			} else {
				continue;
			}


			String outName = attributeTranslations.get(attribute.getMdrKey());

			Method method = MethodMap.getInstance().getMethodMap().get(prefix + outName);
			if (method == null) {
				logger.debug("Couldn't find method for: " + outName);
				continue;
			}
			Class<?>[] parameters = method.getParameterTypes();
			Class<?> parameter = parameters[0];
			if (parameter == null) {
				logger.debug("Can't find parameter type for method: " + method.getName());
				continue;
			}

			String parameterType = parameter.getName();
			Object value = null;

			if (parameterType.equalsIgnoreCase("java.lang.String")) {
				value = attribute.getValue().getValue();
			} else if (parameterType.equalsIgnoreCase("java.lang.Boolean")) {
				value = Boolean.parseBoolean(attribute.getValue().getValue());
			} else if (parameterType.equalsIgnoreCase("javax.xml.datatype.XMLGregorianCalendar")) {
				try {
					Validations validations = mdrClient.getDataElementValidations(attribute.getMdrKey(), "de");
					String formatString = validations.getFormat();
					formatString = formatString.replace('D', 'd').replace('Y', 'y');
					value = Utils.parseXMLGregorianCalendar(attribute.getValue().getValue(), formatString);
				} catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
					logger.debug("Couldn't parse date");
					value = null;
				}
			} else if (parameterType.equalsIgnoreCase("de.samply.share.model.ror.metareg.Register$NumberOfCases")) {
				NumberOfCases noc = new NumberOfCases();
				noc.setValue(new BigInteger(attribute.getValue().getValue()));
				noc.setNumberOfCasesAt(getXMLGregorianCalendarNow());
				value = noc;
			} else if (parameterType.equalsIgnoreCase("java.math.BigInteger")) {
				value = new BigInteger(attribute.getValue().getValue());
			} else if (parameterType.equalsIgnoreCase("de.samply.share.model.ror.metareg.Countries")) {
				value = Countries.fromValue(attribute.getValue().getValue());
			} else {
				logger.debug("Unknown Parameter Type: " + parameterType);
			}

			boolean success = true;

			try {
				method.invoke(register, value);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				success = false;
			}
			if (!success) {
				try {
					method.invoke(bioMaterialBank, value);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
		            throw new RuntimeException(e);
				}
			}

		}

		metareg.setRegister(register);

		metareg.getBiomaterialbank().add(bioMaterialBank);
		return metareg;
	}

	/**
	 * Gets the XML gregorian calendar representation of the current moment.
	 *
	 * @return the XML gregorian calendar now
	 */
	private XMLGregorianCalendar getXMLGregorianCalendarNow() {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		DatatypeFactory datatypeFactory = null;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		now.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		return now;
	}

	/**
	 * Gets the keys for metareg from the slots in the MDR
	 *
	 * @param attributes the attributes
	 * @return the translations
	 * @throws MdrConnectionException the mdr connection exception
	 * @throws MdrInvalidResponseException the mdr invalid response exception
	 * @throws ExecutionException the execution exception
	 */
	private Map<String, String> getTranslations(List<Attribute> attributes) throws MdrConnectionException, MdrInvalidResponseException, ExecutionException {
		Map<String, String> translations = new HashMap<String, String>();
		ArrayList<Slot> slots;

		for (Attribute attribute : attributes) {
			slots = mdrClient.getDataElementSlots(attribute.getMdrKey());
			for (Slot s : slots) {
				if (s.getSlotName().equalsIgnoreCase(SLOT_NAME)) {
					translations.put(attribute.getMdrKey(), s.getSlotValue());
					break;
				}
			}
		}
		return translations;
	}

	/**
	 * Update method map.
	 *
	 * @param theClass the the class
	 * @param prefix the prefix
	 */
	private void updateMethodMap(Class<?> theClass, String prefix) {
		Method[] methods = theClass.getDeclaredMethods();
		String name;

		// Iterate through all methods of the given class. If it's a setter that's not yet in the map, add it.
		for (Method m : methods) {
			name = m.getName();
			if (!name.startsWith("set") || name.length() < 4) {
				continue;
			}
			name = prefix + name.substring(3);

			if (MethodMap.getInstance().getMethodMap().containsKey(name)) {
				continue;
			}

			MethodMap.getInstance().addToMap(name, m);
		}
	}
}