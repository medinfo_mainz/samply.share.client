/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.LogLevel;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import de.samply.auth.rest.AccessTokenDTO;
import de.samply.common.mailing.EmailBuilder;
import de.samply.common.mailing.MailSender;
import de.samply.common.mailing.MailSending;
import de.samply.common.mailing.OutgoingEmail;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.domain.ResultList;
import de.samply.config.util.FileFinderUtil;
import de.samply.share.client.core.tables.daos.LogDao;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.pojos.Inquiryresult;
import de.samply.share.client.core.tables.pojos.Log;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.controller.ApplicationBean;
import de.samply.share.model.EnumConfiguration;
import de.samply.share.thread.Application;
import de.samply.share.thread.Task;
import de.samply.share.thread.TaskResult;
import de.samply.web.mdrFaces.MdrContext;

// TODO: Auto-generated Javadoc
/**
 * A class holding various utility methods.
 *
 */
public class Utils {

    /** The Constant CONFIG_FILENAME. */
    private final static String CONFIG_FILENAME = "samply.share.conf";

    /**  The Mail subject for a new inquiry notification. */
    private final static String MAIL_SUBJECT = "New inquiries in OSSE.Share";

    /** The config path. */
    private static String configPath = null;

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(Utils.class);

    /**
     * Write to log table.
     *
     * @param level the level
     * @param type the type
     * @param message the message
     */
    public static void taskWriteLog(LogLevel level, String type, String message) {
        try {
            writeLog(level, type, message, "system", "system");
        } catch (SQLException e) {
            logger.debug("Could not write to log db");
        }
    }

    /**
     * Write a message to the log in the database.
     *
     * @param level
     *            the level
     * @param type
     *            the type
     * @param message
     *            the message
     * @param actor
     *            the actor
     * @param logger
     *            the logger
     * @throws SQLException
     *             the SQL exception
     */
    public static void writeLog(LogLevel level, String type, String message, String actor, String logger) throws SQLException {
        Connection connection = ResourceManager.getConnection();
        Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
        LogDao logDao = new LogDao(configuration);
        Log log = new Log();
        Date date = new Date();
        log.setDated(new Timestamp(date.getTime()));

        log.setLevel(level.toString());
        log.setType(type);

        log.setMessage(message);
        log.setActor(actor);
        log.setLogger(logger);
        logDao.insert(log);
    }

    /**
     * Gets the config path.
     *
     * @return the config path
     */
    public static String getConfigPath() {

        if (configPath == null) {
            try {
                configPath = FileFinderUtil.findFile(CONFIG_FILENAME, ProjectInfo.INSTANCE.getProjectName(), null).getParent();
            } catch (FileNotFoundException e) {
                logger.fatal("Could not find config file...");
                e.printStackTrace();
            }
        }
        return configPath;
    }

    /**
     * Convert date to iso string.
     *
     * @param date
     *            the date
     * @return the string
     */
    public static String convertDateToIsoString(Date date) {
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }

    /**
     * Spawn new task.
     *
     * @param task
     *            the task
     * @throws Exception
     *             the exception
     */
    public static void spawnNewTask(Task task) throws Exception {
        // enqueue...
        Future<TaskResult> ticket = Application.execInBackground(task);

        // work will now be done in the background. Is it finished yet?
        boolean done = ticket.isDone(); // this is non-blocking!
        if (!done) {
            logger.debug("Your results are not finished yet. Please wait...");
        }

        TaskResult result = ticket.get(); // this is blocking!
        logger.debug("Here's your result: ..." + result.getMessageToBeLogged());
    }

    /**
     * Adds a trailing file separator.
     *
     * @param in
     *            the in
     * @return the string
     */
    public static String addTrailingFileSeparator(String in) {
        String matchExpression;
        if (File.separator.equals("\\")) {
            matchExpression = File.separator + File.separator + "+$";
        } else {
            matchExpression = File.separator + "+$";
        }
        return in.replaceAll(matchExpression, "") + File.separator;
    }

    /**
     * Adds a trailing slash.
     *
     * @param in
     *            the in
     * @return the string
     */
    public static String addTrailingSlash(String in) {
        if (in == null) {
            return "";
        }
        return in.replaceAll("/+$", "") + "/";
    }

    /**
     * Removes the duplicate slashes.
     *
     * @param in
     *            the in
     * @return the string
     */
    public static String removeDuplicateSlashes(String in) {
        if (in == null) {
            return "";
        }
        return in.replaceAll("/+", "/");
    }

    /**
     * Gets the given string as http host.
     *
     * @param hostString
     *            the host string
     * @return the http host
     * @throws MalformedURLException
     *             the malformed url exception
     */
    public static HttpHost getAsHttpHost(String hostString) throws MalformedURLException {
        URL url = new URL(hostString);
        int port = url.getPort();
        if (port < 0) {
            if (url.getProtocol().equalsIgnoreCase("https"))
                port = 443;
            else
                port = 80;
        }
        return new HttpHost(url.getHost(), port, url.getProtocol());
    }

    /**
     * Gets the ApplicationBean.
     *
     * @return the ApplicationBean
     */
    public static ApplicationBean getApplicationBean() {
        return (ApplicationBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
                .getValue(FacesContext.getCurrentInstance().getELContext(), null, "applicationBean");
    }

    /**
     * Convert date from string.
     *
     * @param value
     *            the value
     * @param formatString
     *            the format string
     * @return the object
     */
    public static Object convertDateFromString(String value, String formatString) {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat(formatString);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        Calendar calendar = Calendar.getInstance();

        try {
            calendar.setTime(simpleDateFormat.parse(value));
            Date myDate = calendar.getTime();
            return myDate;
        } catch (ParseException e) {
            logger.error("Parse exception when trying to parse Date " + value + " with fmt " + formatString);
        }
        return value;
    }

    /**
     * Parses the xml gregorian calendar.
     *
     * @param date
     *            the date
     * @param format
     *            the format
     * @return the XML gregorian calendar
     */
    public static XMLGregorianCalendar parseXMLGregorianCalendar(String date, String format) {
        DateFormat simpleDateFormatInput = new SimpleDateFormat(format, Locale.ENGLISH);

        Date tmp = null;
        try {
            tmp = simpleDateFormatInput.parse(date);
        } catch (ParseException e) {
        }

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(tmp);

        DatatypeFactory datatypeFactory = null;
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        XMLGregorianCalendar xmlGregorianCalendar = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
        xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        xmlGregorianCalendar.setTime(DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);

        return xmlGregorianCalendar;
    }

    /**
     * Send mail.
     *
     * @return true, if successful
     */
    public static boolean sendMail() {
        String receiverAdress;
        String shareUrl;
        try {
            receiverAdress = ConfigurationManager.getConfigurationElement(EnumConfiguration.MAIL_RECEIVER_ADRESS);
            shareUrl = ConfigurationManager.getConfigurationElement(EnumConfiguration.MAIL_TEILER_URL);
        } catch (SQLException e) {
            logger.warn("Mail system not configured correctly.");
            return false;
        }

        if (receiverAdress == null || receiverAdress.length() < 1) {
            logger.warn("No receiver set for emails. Aborting.");
            return false;
        }

        OutgoingEmail email = new OutgoingEmail();
        email.addAddressee(receiverAdress);
        email.setSubject(MAIL_SUBJECT);
        email.setLocale("en");
        email.putParameter("teilerUrl", shareUrl);
        String projectName = ProjectInfo.INSTANCE.getProjectName();
        MailSending mailSending = MailSender.loadMailSendingConfig(projectName);
        String templateFolder = getRealPath(mailSending.getTemplateFolder());

        EmailBuilder builder = new EmailBuilder(templateFolder);
        builder.addTemplateFile("Footer.soy", "Footer");
        builder.addTemplateFile("NewInquiriesContent.soy", "NewInquiriesContent");
        email.setBuilder(builder);

        MailSender mailSender = new MailSender(mailSending);
        mailSender.send(email);
        return true;
    }

    /**
     * Gets the real path.
     *
     * @param relativeWebPath the relative web path
     * @return the real path
     */
    public static String getRealPath(String relativeWebPath) {
        ServletContext servletContext = ProjectInfo.INSTANCE.getServletContext();
        return servletContext.getRealPath(relativeWebPath);
    }

    /**
     * Check and send notifications.
     */
    public static void checkAndSendNotifications() {
        logger.debug("Checking for notifications to send");
        DbUtils.updateResultCounts();
        List<Inquiry> inquiries = DbUtils.getInquiriesForNotifications();
        if (inquiries == null || inquiries.size() < 1) {
            logger.debug("No new notifications to send");
        } else {
            if (sendMail()) {
                logger.debug("Mail sent and notifications deleted");
                DbUtils.setNotificationSentForInquiries(inquiries);
            } else {
                logger.debug("Could not send mails.");
            }
        }
    }

    /**
     * The Class UploadStats.
     */
    @Root(name = "UploadStats")
    public static class UploadStats {

        /** The last upload timestamp. */
        @Element(name = "LastUploadTimestamp")
        private String lastUploadTimestamp;

        /**
         * Instantiates a new upload stats.
         */
        public UploadStats() {
            super();
        }

        /**
         * Instantiates a new upload stats.
         *
         * @param lastUploadTimestamp
         *            the last upload timestamp
         */
        public UploadStats(String lastUploadTimestamp) {
            this.lastUploadTimestamp = lastUploadTimestamp;
        }

        /**
         * Gets the last upload timestamp.
         *
         * @return the last upload timestamp
         */
        public String getLastUploadTimestamp() {
            return lastUploadTimestamp;
        }
    }

    /**
     * Gets the query id from inquiry.
     *
     * @param inquiry the inquiry
     * @return the query id from inquiry
     */
    public static String getQueryIdFromInquiry(Inquiry inquiry) {
        Inquiryresult inquiryResult = DbUtils.getResultByInquiry(inquiry);
        if (inquiryResult != null) {
            String location = inquiryResult.getLocation();
            int pos_l = location.lastIndexOf("/");
            if (pos_l > 1) {
                return location.substring(pos_l + 1);
            }
        } else {
            logger.warn("Could not get Result for inquiry " + inquiry.getId());
        }
        return "";
    }

    /**
     * Gets the accesstoken.
     *
     * @return the accesstoken
     */
    public static String getAccesstoken() {
        Client client = Client.create();
        try {
            WebResource webResource = client.resource(ConfigurationManager.getConfigurationElement(EnumConfiguration.LDM_URL) + "/access_token");

            AccessTokenDTO accessToken = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                    .get(AccessTokenDTO.class);
            return accessToken.getAccessToken();
        } catch (SQLException e) {
            logger.debug("Error on the processing of the access and id tokens: " + e);
        }
        return null;
    }

    /**
     * Gets the namespace representation.
     *
     * @param mdrId the mdr id
     * @return the namespace representation
     * @throws ExecutionException the execution exception
     */
    public static String getNamespaceRepresentation(String mdrId) throws ExecutionException {
        MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
        ResultList results = null;
        try {
            results = mdrClient.getUserNamespaceRepresentations(mdrId, getAccesstoken(), null);
        } catch (UniformInterfaceException e) {
        }
        if (results == null || results.getTotalcount() == 0) {
            logger.warn("Could not find own namespace representation for " + mdrId);
            return null;
        } else {
            String ret = results.getResults().get(0).getId();
            return ret;
        }
    }
}
