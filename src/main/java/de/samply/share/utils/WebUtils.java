/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Meaning;
import de.samply.common.mdrclient.domain.PermissibleValue;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.pojos.Inquiryarchive;
import de.samply.share.client.core.tables.pojos.Inquiryresult;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.web.mdrFaces.MdrContext;

// TODO: Auto-generated Javadoc
/**
 * The Class WebUtils offers methods to be used directly from xhtml.
 */
public final class WebUtils {

    /** The Constant MDR_ID_CASE_DATE. */
    public static final String MDR_ID_CASE_DATE = "urn:dktk:dataelement:27";

    /** The Constant VALIDATION_DATATYPE_ENUMERATED. */
    private static final String VALIDATION_DATATYPE_ENUMERATED = "enumerated";

    /**
     * Prohibit class instantiation since it only offers static methods.
     */
    private WebUtils() {
    }

    /**
     * Gets the designation for a dataelement in the mdr.
     *
     * @param dataElement            the data element id
     * @param languageCode            the language code
     * @return the designation
     */
    public static String getDesignation(String dataElement, String languageCode) {

        MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

        try {
            return mdrClient.getDataElementDefinition(dataElement, languageCode).getDesignations().get(0).getDesignation();
        } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
            e.printStackTrace();
            return ("??" + dataElement + "??");
        }

    }

    /**
     * Gets the designation for a value of a dataelement in the mdr.
     *
     * @param dataElement            the data element id
     * @param value            the value for which the designation is searched
     * @param languageCode            the language code
     * @return the designation
     */
    public static String getValueDesignation(String dataElement, String value, String languageCode) {
        String designation = value;
        MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

        try {
            Validations validations = mdrClient.getDataElementValidations(dataElement, languageCode);
            String dataType = validations.getDatatype();
            if (dataType.equalsIgnoreCase(VALIDATION_DATATYPE_ENUMERATED)) {
                List<PermissibleValue> permissibleValues = validations.getPermissibleValues();
                for (PermissibleValue permissibleValue : permissibleValues) {
                    List<Meaning> meanings = permissibleValue.getMeanings();
                    if (permissibleValue.getValue().equals(value)) {
                        for (Meaning meaning : meanings) {
                            if (meaning.getLanguage().equalsIgnoreCase(languageCode)) {
                                return meaning.getDesignation();
                            }
                        }
                    }
                }
            }
        } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
            e.printStackTrace();
        }
        return designation;
    }

    /**
     * Convert time to dd.MM.yyyy format.
     *
     * @param time
     *            the time
     * @return the converted time.
     */
    public static String convertTime(Timestamp time) {
        if (time == null)
            return "";
        return new SimpleDateFormat("dd.MM.yyyy").format(time);
    }

    /**
     * Gets the project name.
     *
     * @return the project name
     */
    public static String getProjectName() {
        return ProjectInfo.INSTANCE.getProjectName();
    }

    /**
     * Gets the version string of this Samply Share instance to show it on the login screen.
     *
     * @return the version string
     */
    public static String getVersionString() {
        return ProjectInfo.INSTANCE.getVersionString();
    }


    /**
     * Gets the execution date of an inquiry.
     *
     * @param inqiry the inquiry
     * @return the execution date of the inquiry
     */
    public static String getExecutionDate(Inquiry inqiry) {
        Inquiryresult inquiryresult = DbUtils.getResultByInquiry(inqiry);
        if (inquiryresult != null) {
            Timestamp time = inquiryresult.getExecutedAt();
            if (time != null) {
                return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(time);
            }
        }
        return "";
    }

    /**
     * Gets the alert classes.
     *
     * @param severity the severity
     * @return the alert classes
     */
    public static String getAlertClasses(String severity) {

        if (severity.toLowerCase().startsWith("info")) {
            return "alert alert-info alert-dismissible";
        } else if (severity.toLowerCase().startsWith("warn")) {
            return "alert alert-warning alert-dismissible";
        } else {
            return "alert alert-danger alert-dismissible";
        }
    }

    /**
     * Gets the id of the server's time zone. This can be used for
     * {@code <f:convertDateTime>} tags that must display the date in the
     * server's time zone.
     *
     * @return the server time zone
     */
    public static String getServerTimeZone() {
        return TimeZone.getDefault().getID();
    }

    /**
     * Gets the archivation type for an inquiry.
     *
     * @param inquiry the inquiry
     * @return the archive type of the inquiry
     */
    public static String getArchiveType(Inquiry inquiry) {
        Inquiryarchive inquiryArchive = DbUtils.getArchiveByInquiry(inquiry);
        if (inquiryArchive != null) {
            return inquiryArchive.getType().toString();
        }
        return "";
    }

    /**
     * Checks if is element known.
     *
     * @param mdrId the mdr id
     * @return true, if is element known
     */
    public static boolean isElementKnown(String mdrId) {
        String translation = null;
        try {
            translation = Utils.getNamespaceRepresentation(mdrId);
        } catch (ExecutionException e) {
        }
        return (translation != null);
    }
}