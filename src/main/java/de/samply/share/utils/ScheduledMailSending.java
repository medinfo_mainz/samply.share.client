/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.utils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.samply.share.thread.MailSendingTask;

/**
 * Handles the scheduled sending of notification emails
 */
public class ScheduledMailSending {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(ScheduledMailSending.class);

    /** The Constant scheduler. */
    protected final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    /**
     * Handler created when a task is scheduled. Enables cancelling that task.
     */
    private static ScheduledFuture<?> scheduledMailSender;

    /**
     * Cancels already scheduled uploads if any and schedules new uploads according to the parameters.
     *
     * @param timeStr
     *            String representation of the time at which the upload is to be scheduled every day, e.g. "04:30"
     */
    public static void reScheduleMailSending() {
        cancelScheduledMailSending();
        scheduleMailSending();
    }

    /**
     * Shutdown service.
     */
    public static void shutdownService() {
        scheduler.shutdownNow();
        try {
            scheduler.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cancels already scheduled uploads if any.
     */
    private static void cancelScheduledMailSending() {
        if (scheduledMailSender != null) {
            scheduledMailSender.cancel(false);
        }
    }

    public static void scheduleMailSendingHourly() {
        ScheduledMailSending.reScheduleMailSending();
    }

    /**
     * Schedule an upload tasks.
     *
     * @param timeStr
     *            String representation of the time at which the upload is to be scheduled every day, e.g. "04:30"
     */
    private static void scheduleMailSending() {
        logger.debug("Scheduling the mail sender...");

        // runnable upload action
        final Runnable runnableMailSender = new Runnable() {
            @Override
            public void run() {
                MailSendingTask mailSendingTask = new MailSendingTask();
                mailSendingTask.doIt();
            }
        };
        // Just do it each hour
        scheduledMailSender = scheduler.scheduleAtFixedRate(runnableMailSender, 1, 1, TimeUnit.HOURS);

    }
}