/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/** Access Control Filter.
 *
 *  If a user is trying to go to an area he is not supposed to, redirect him to his start page
 */
@WebFilter({"/*"})
public class AccessControlFilter implements Filter {

    private static final Logger logger = Logger.getLogger(AccessControlFilter.class);

    /** The Constant SESSION_ROLE. */
    private static final String ADMIN_NAME = "admin";

    /** The key for the username in the session */
    private static final String SESSION_USERNAME = "username";

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        String username = (String) request.getSession().getAttribute(SESSION_USERNAME);
        String uri = request.getRequestURI();

        if (uri.toLowerCase().contains("userlist") || uri.toLowerCase().contains("configuration") || uri.toLowerCase().contains("logviewer")) {
            if (username != null && username.equalsIgnoreCase(ADMIN_NAME)) {
                chain.doFilter(request, res);
            } else {
                logger.warn("User tried to access restricted area. Redirecting...");
                HttpServletResponse response = (HttpServletResponse) res;
                response.sendRedirect(request.getContextPath() + "/incomingInquiries.xhtml");
            }
        } else {
            chain.doFilter(request, res);
        }
    }

    @Override
    public void destroy() {
    }
}