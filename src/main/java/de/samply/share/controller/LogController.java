/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.jooq.DSLContext;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.pojos.Log;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.listener.StartupListener;
import de.samply.share.model.EnumConfiguration;
import de.samply.share.model.LogLine;
import de.samply.share.thread.MetaregUploadTask;
import de.samply.share.utils.ConfigurationManager;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.Utils;

/**
A JSF Managed Bean that is existent for a whole session. It handles log-related issues.
 */
@ManagedBean(name = "logController")
@SessionScoped
public class LogController implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2913207120030128475L;

	/** The Constant RESULT_PAGE_0_URL_PART. */
	private static final String RESULT_PAGE_0_URL_PART = "/result?page=0";

	/** The Constant REQ_URL_PART. */
	private static final String REQ_URL_PART = "/requests/";

	/** The Constant REQ_CODE_IN_MSG_REGEX. */
	private static final String REQ_CODE_IN_MSG_REGEX = ".*\\s+(([a-zA-Z0-9]*(-)+[a-zA-Z0-9]*)*).*";

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(LogController.class);

	/** The log list. */
	private List<LogLine> logList = new ArrayList<>();

	/** The date format. */
	private DateFormat dateFormat;

	/**
	 * Instantiates a new log controller and loads configuration from the database.
	 */
	public LogController() {
		loadLogList();
	}

	/**
	 * Gets the list of log lines
	 *
	 * @return the log list
	 */
	public List<LogLine> getLogList() {
		return logList;
	}

	/**
	 * Sets the log list.
	 *
	 * @param logList the new log list
	 */
	public void setLogList(List<LogLine> logList) {
		this.logList = logList;
	}

	/**
	 * Get the amount of connected RoRs
	 *
	 * @return the amount of rors
	 */
	public long getRorCount() {
	    return DbUtils.countRors();
	}

	/**
	 * Triggers upload to registry of registries.
	 */
	public void rorUpload() {
	    StartupListener.submitTask(new MetaregUploadTask());
	}

	/**
	 * Load content of the log table from the database.
	 */
	public void loadLogList() {
		List<Log> logList = null;

		try (Connection connection = ResourceManager.getConnection() ) {
			connection.setAutoCommit(false);
			DSLContext select = ResourceManager.getDSLContext(connection);

			logList = select.selectFrom(Tables.LOG)
										.orderBy(Tables.LOG.DATED.desc())
										.fetchInto(Log.class);
			connection.close();

		}  catch (SQLException e) {
			logger.debug("Error while getting the a connection from the database.");
		}

		populateLogLines(logList);
	}

	/**
	 * Get the request URL from the request code of a log message.
	 *
	 * @param logMessage the log message
	 * @return the URL to the first page of the request, or empty String if
	 *         there is no request code in the message
	 */
	public String getReqUrl (String logMessage) {
		String url="";
		String regex = REQ_CODE_IN_MSG_REGEX;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(logMessage);

		if (matcher.matches() && !matcher.group(1).isEmpty()) {
			if (logMessage.startsWith("0")) {
				return url;
			}
			try {
				url = Utils.removeDuplicateSlashes(
						new StringBuilder()
						.append(ConfigurationManager.getConfigurationElement(EnumConfiguration.LDM_URL))
						.append(REQ_URL_PART)
						.append(matcher.group(1).trim())
						.append(RESULT_PAGE_0_URL_PART)
						.toString());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return url;
	}

	/**
	 * Sort as date. The arguments are converted to dates by calling {@link DateFormat#parse(String)}.
	 *
	 * @param s1 A string that represents a date.
	 * @param s2 A string that represents a date.
	 * @return 0 if the parsed dates are equal, a value less than 0 if s1 is less then s2,
	 * 	a value greater than 0 if s2 is greater than s1.
	 */
	public int sortAsDate(String s1, String s2) {
        try {
            return dateFormat.parse(s1).compareTo(dateFormat.parse(s2));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
	}

	/**
	 * Populate log lines.
	 *
	 * @param logEntries the log entries
	 */
	private void populateLogLines(List<Log> logEntries) {
		dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String date;
		logList = new ArrayList<>();
		LogLine logLine;
		for (Log log : logEntries) {
			logLine = new LogLine();
			date = dateFormat.format(log.getDated());
			logLine.setDated(date);
			logLine.setLevel(log.getLevel());
			logLine.setLogger(log.getLogger());
			logLine.setType(log.getType());
			logLine.setMessage(log.getMessage());
			logLine.setUser(log.getActor());
			logList.add(logLine);
		}
	}
}
