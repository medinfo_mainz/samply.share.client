/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.share.client.core.tables.daos.BrokerDao;
import de.samply.share.client.core.tables.pojos.Broker;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.EnumBrokerStatus;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.Utils;

/**
 * A JSF Managed Bean that is valid for the lifetime of a view. It holds methods and information necessary to handle communication with connected search brokers
 */
@ManagedBean(name = "brokerController")
@ViewScoped
public class BrokerController implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5348654341602813763L;

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(BrokerController.class);

    /** The auth header key. */
    private final String AUTH_HEADER_KEY = "Authorization";

    /** The access token header key. */
    private final String ACCESSTOKEN_HEADER_KEY = "Accesstoken";

    /** The auth header value registration. */
    private final String AUTH_HEADER_VALUE_REGISTRATION = "SamplyRegistration";

    /** The auth header value samply. */
    private final String AUTH_HEADER_VALUE_SAMPLY = "Samply";

    /** The path to the bank rest ressource */
    private final String BANKS_PATH = "rest/searchbroker/banks/";

    /** The path to retrieve the name of a searchbroker*/
    private final String BROKER_NAME_PATH = "rest/searchbroker/name";

    /** The broker list. */
    private List<Broker> brokerList;

    /** The broker map. */
    private Map<Broker, String> brokerMap;

    /** The mail address used for registration with a new searchbroker. */
    private String newMail;

    /** The address (url) of the new searchbroker. */
    private String newAddress;

    /**
     * Gets the mail address used for registration with a new searchbroker.
     *
     * @return the mail address used for registration with a new searchbroker
     */
    public String getNewMail() {
        return newMail;
    }

    /**
     * Sets the mail address used for registration with a new searchbroker.
     *
     * @param newMail
     *            the new mail address used for registration with a new searchbroker
     */
    public void setNewMail(String newMail) {
        this.newMail = newMail;
    }

    /**
     * Gets the address (url) of the new searchbroker.
     *
     * @return the address (url) of the new searchbroker
     */
    public String getNewAddress() {
        return newAddress;
    }

    /**
     * Sets the address (url) of the new searchbroker.
     *
     * @param newAddress
     *            the new address (url) of the new searchbroker
     */
    public void setNewAddress(String newAddress) {
        this.newAddress = newAddress;
    }

    /**
     * Gets the list of searchbrokers to which this Samply.Share instance is registered to.
     *
     * @return the broker list
     */
    public List<Broker> getBrokerList() {
        return brokerList;
    }

    /**
     * Sets the broker list.
     *
     * @param brokerList
     *            the new broker list
     */
    public void setBrokerList(List<Broker> brokerList) {
        this.brokerList = brokerList;
    }

    /**
     * Gets the broker map.
     *
     * @return the broker map. Contains the Broker itself and a String, representing a status to be displayed on the respective page.
     */
    public Map<Broker, String> getBrokerMap() {
        return brokerMap;
    }

    /**
     * Sets the broker map.
     *
     * @param brokerMap
     *            the broker map
     */
    public void setBrokerMap(Map<Broker, String> brokerMap) {
        this.brokerMap = brokerMap;
    }

    /**
     * Initializes the BrokerController. Loads the List of brokers the client is registered to from the database and stores the corresponding status in a map.
     */
    @PostConstruct
    public void init() {
        refreshBrokerMap();
    }

    /**
     * Refresh broker list from Database.
     */
    private void refreshBrokers() {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            brokerList = new ArrayList<Broker>();
            BrokerDao brokerDao = new BrokerDao(configuration);
            brokerList = brokerDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Refresh broker map by getting the available brokers from the database and query their status.
     */
    private void refreshBrokerMap() {
        if (brokerList == null) {
            refreshBrokers();
        }
        brokerMap = new HashMap<Broker, String>();
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            BrokerDao brokerDao = new BrokerDao(configuration);

            brokerList = brokerDao.findAll();
            for (Broker b : brokerList) {
                String status = getStatus(b);
                brokerMap.put(b, status);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Refresh the status of one given broker.
     *
     * @param broker
     *            the broker to refresh
     */
    private void refreshBroker(Broker broker) {
        if (brokerMap == null) {
            return;
        }
        try {
            String status = getStatus(broker);
            brokerMap.put(broker, status);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Register to a new searchbroker. The parameters are entered on the correspondant xhtml page and bound to the parameters 'newMail' and 'newAddress'
     *
     * @return the outcome to use for navigation
     * @throws ClientProtocolException
     *             in case of an http protocol error
     * @throws IOException
     *             in case of a problem or the connection was aborted
     * @throws URISyntaxException
     *             in case of a malformed URI for the new broker address
     */
    public String register() throws ClientProtocolException, IOException, URISyntaxException {
        if (newAddress == null || newMail == null || newAddress.length() < 1 || newMail.length() < 1) {
            return "error";
        }

        URL destURL;
        try {
            destURL = new URL(newAddress);
        } catch (MalformedURLException e) {
            logger.debug("Malformed URL (" + newAddress + ")- trying to add default protocol.");
            destURL = null;
        }

        if (destURL == null) {
            try {
                destURL = new URL("http://" + newAddress);
            } catch (MalformedURLException e) {
                logger.error("Could not parse Broker Address: " + newAddress);
                return "error";
            }
        }

        int destPort = destURL.getPort();
        if (destPort < 0) {
            if (destURL.getProtocol().equalsIgnoreCase("https"))
                destPort = 443;
            else
                destPort = 80;
        }

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return "error";
        }

        CloseableHttpClient httpc = httpConnector.getHttpClient(destURL.toString());

        HttpHost target = new HttpHost(destURL.getHost(), destPort, destURL.getProtocol());

        URI uri = new URI(Utils.addTrailingSlash(destURL.getPath()) + BANKS_PATH + newMail);

        HttpPut httpPut = new HttpPut(uri.normalize().toString());
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpPut.setConfig(requestConfig);
        CloseableHttpResponse response = httpc.execute(target, httpPut);

        int retCode = response.getStatusLine().getStatusCode();
        response.close();

        if (retCode == HttpStatus.SC_UNAUTHORIZED) {
            try (Connection connection = ResourceManager.getConnection() ) {
                Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
                Broker broker = new Broker();
                broker.setAddress(destURL.toString());
                broker.setEmail(newMail);
                BrokerDao brokerDao = new BrokerDao(configuration);
                brokerDao.insert(broker);
                refreshBroker(broker);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return "brokerList";
        } else if (retCode == HttpStatus.SC_CONFLICT)
            return "conflict";
        else
            return "error";
    }

    /**
     * Send activation code to the corresponding searchbroker.
     *
     * @param broker
     *            the broker
     * @param activationCode
     *            the activation code
     * @return the string
     * @throws ClientProtocolException
     *             in case of an http protocol error
     * @throws IOException
     *             in case of a problem or the connection was aborted.
     * @throws URISyntaxException
     *             in case of a malformed URI for the broker address
     */
    public String sendActivationCode(Broker broker, String activationCode) throws ClientProtocolException, IOException, URISyntaxException {
        URL destinationURL = new URL(broker.getAddress());
        int destinationPort = destinationURL.getPort();

        if (destinationPort < 0) {
            if (destinationURL.getProtocol().equalsIgnoreCase("https"))
                destinationPort = 443;
            else
                destinationPort = 80;
        }

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return "error";
        }
        CloseableHttpClient httpClient = httpConnector.getHttpClient(broker.getAddress());

        HttpHost targetHost = new HttpHost(destinationURL.getHost(), destinationPort, destinationURL.getProtocol());
        URI targetUri = new URI(Utils.addTrailingSlash(broker.getAddress()) + BANKS_PATH + broker.getEmail());
        HttpPut httpPut = new HttpPut(targetUri.normalize().toString());
        httpPut.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_REGISTRATION + " " + activationCode);
        httpPut.addHeader(ACCESSTOKEN_HEADER_KEY, Utils.getAccesstoken());

        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpPut.setConfig(requestConfig);
        CloseableHttpResponse response = httpClient.execute(targetHost, httpPut);
        HttpEntity entity = response.getEntity();
        String entityOutput = EntityUtils.toString(entity, Consts.UTF_8);

        int retCode = response.getStatusLine().getStatusCode();
        response.close();

        if (retCode == HttpStatus.SC_CREATED) {
            try (Connection connection = ResourceManager.getConnection() ) {
                Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
                broker.setAuthcode(entityOutput);
                BrokerDao brokerDao = new BrokerDao(configuration);
                brokerDao.update(broker);
                refreshBroker(broker);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return "success";
        } else {
            return "error";
        }
    }

    /**
     * Send an HTTP Delete command to a search broker in order to be removed from the known clients list.
     *
     * @param broker
     *            the broker from which this client should be removed
     * @return true, if successful
     * @throws ClientProtocolException
     *             in case of an http protocol error
     * @throws IOException
     *             in case of a problem or the connection was aborted.
     * @throws URISyntaxException
     *             in case of a malformed URI for the broker address
     */
    private boolean sendDeleteCommand(Broker broker) throws ClientProtocolException, IOException, URISyntaxException {
        logger.info("Request deletion from: " + broker.getAddress());
        URL targetURL = new URL(broker.getAddress());
        int targetPort = targetURL.getPort();
        if (targetPort < 0) {
            if (targetURL.getProtocol().equalsIgnoreCase("https"))
                targetPort = 443;
            else
                targetPort = 80;
        }

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return false;
        }
        CloseableHttpClient httpClient = httpConnector.getHttpClient(broker.getAddress());

        HttpHost targetHost = new HttpHost(targetURL.getHost(), targetPort, targetURL.getProtocol());
        URI targetUri = new URI(Utils.addTrailingSlash(broker.getAddress()) + BANKS_PATH + broker.getEmail());
        HttpDelete httpDelete = new HttpDelete(targetUri.normalize().toString());
        httpDelete.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_SAMPLY + " " + broker.getAuthcode());
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpDelete.setConfig(requestConfig);
        CloseableHttpResponse response = httpClient.execute(targetHost, httpDelete);

        int retCode = response.getStatusLine().getStatusCode();
        response.close();
        if (retCode == HttpStatus.SC_NO_CONTENT) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the status of a connected search broker.
     *
     * @param broker
     *            the broker whose status to query.
     * @return the status (registered, confirmation needed, wrong credentials, unknown)
     * @throws ClientProtocolException
     *             in case of an http protocol error
     * @throws IOException
     *             in case of a problem or the connection was aborted.
     * @throws URISyntaxException
     *             in case of a malformed URI for the broker address
     */
    public String getStatus(Broker broker) throws ClientProtocolException, IOException, URISyntaxException {

        URL destURL = new URL(broker.getAddress());
        int destPort = destURL.getPort();
        if (destPort < 0) {
            if (destURL.getProtocol().equalsIgnoreCase("https"))
                destPort = 443;
            else
                destPort = 80;
        }

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return EnumBrokerStatus.UNKNOWN_ERROR.getName();
        }
        CloseableHttpClient httpClient = httpConnector.getHttpClient(broker.getAddress());

        HttpHost target = new HttpHost(destURL.getHost(), destPort, destURL.getProtocol());
        URI uri = new URI(Utils.addTrailingSlash(destURL.getPath()) + BANKS_PATH + broker.getEmail() + "/status");
        HttpGet httpGet = new HttpGet(uri.normalize().toString());
        httpGet.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_SAMPLY + " " + broker.getAuthcode());
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpGet.setConfig(requestConfig);

        CloseableHttpResponse response;
        try {
            response = httpClient.execute(target, httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            response.close();

            if (statusCode == HttpStatus.SC_OK)
                return EnumBrokerStatus.REGISTERED.getName();
            else if (statusCode == HttpStatus.SC_FORBIDDEN)
                return EnumBrokerStatus.CHECK_CREDENTIALS.getName();
            else if (statusCode == HttpStatus.SC_ACCEPTED)
                return EnumBrokerStatus.CONFIRMATION_NEEDED.getName();
            else
                return EnumBrokerStatus.NOT_KNOWN.getName();
        } catch (Exception e) {
            return EnumBrokerStatus.UNKNOWN_ERROR.getName();
        }

    }

    /**
     * Gets the name of a searchbroker in order to display it in the broker list.
     *
     * @param broker
     *            the broker whose name to get
     * @return the name of the searchbroker (or the url if the name could not be retrieved)
     * @throws ClientProtocolException
     *             in case of an http protocol error
     * @throws IOException
     *             in case of a problem or the connection was aborted.
     * @throws URISyntaxException
     *             in case of a malformed URI for the broker address
     */
    public String getName(Broker broker) throws ClientProtocolException, IOException, URISyntaxException {

        if (broker.getName() != null && broker.getName().length() > 0) {
            return broker.getName();
        }

        URL targetURL = new URL(broker.getAddress());
        int targetPort = targetURL.getPort();
        if (targetPort < 0) {
            if (targetURL.getProtocol().equalsIgnoreCase("https"))
                targetPort = 443;
            else
                targetPort = 80;
        }

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return broker.getAddress();
        }
        CloseableHttpClient httpClient = httpConnector.getHttpClient(broker.getAddress());

        HttpHost targetHost = new HttpHost(targetURL.getHost(), targetPort, targetURL.getProtocol());
        URI targetUri = new URI(Utils.addTrailingSlash(targetURL.getPath()) + BROKER_NAME_PATH);
        HttpGet httpGet = new HttpGet(targetUri.normalize().toString());
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpGet.setConfig(requestConfig);

        CloseableHttpResponse response;
        try {
            response = httpClient.execute(targetHost, httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            String brokerName = EntityUtils.toString(entity, "UTF-8");
            response.close();

            if (statusCode == HttpStatus.SC_OK) {
                broker.setName(brokerName);
                DbUtils.updateBroker(broker);
                return broker.getName();
            } else
                return broker.getAddress();
        } catch (Exception e) {
            logger.warn("Searchbroker not reachable: " + broker.getAddress());
            return broker.getAddress();
        }

    }

    /**
     * Delete broker from database.
     *
     * @param broker
     *            the broker to delete
     * @return the outcome for the jsf2 navigation
     */
    public String deleteBroker(Broker broker) {
        try {
            sendDeleteCommand(broker);
        } catch (IOException | URISyntaxException e1) {
            e1.printStackTrace();
        }

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            BrokerDao brokerDao = new BrokerDao(configuration);
            brokerDao.delete(broker);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "brokerList";
    }
}
