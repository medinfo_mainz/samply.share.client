/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.tables.daos.ConfigurationDao;
import de.samply.share.client.core.tables.pojos.Configuration;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.ConfigurationElement;
import de.samply.share.model.EnumConfiguration;
import de.samply.share.utils.ScheduledInquiryCollection;
import de.samply.share.utils.ScheduledRorUpload;
import de.samply.share.utils.Utils;

/**
 * A JSF Managed Bean handling the configuration of the application.
 */
@ManagedBean(name = "configurationController")
@SessionScoped
public class ConfigurationController implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5835534453497659211L;

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(ConfigurationController.class);

    /** The configuration. */
    private ConfigurationElement configurationElement = new ConfigurationElement();

    /**
     * Instantiates a new configuration controller.
     */
    public ConfigurationController() {
        logger.debug("Construct ConfigurationController");
        init();
    }

    @PostConstruct
    public void init() {
        logger.debug("Initialize ConfigurationController");
        loadValues();
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public ConfigurationElement getConfigurationElement() {
        return configurationElement;
    }

    /**
     * Sets the configuration.
     *
     * @param configurationElement
     *            the new configuration
     */
    public void setConfigurationElement(ConfigurationElement configurationElement) {
        this.configurationElement = configurationElement;
    }

    /**
     * Checks if configuration entry is set.
     *
     * @param configuration
     *            the configuration
     * @return true, if configuration is set and not empty, false otherwise
     */
    private boolean isConfigurationSet(Configuration configuration) {
        if (configuration == null) {
            return false;
        } else if (configuration.getValue() == null) {
            return false;
        }
        return configuration.getValue().length() > 0;
    }

    /**
     * Save the configuration to the database.
     */
    public void save() {
        FacesMessage msg = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            List<Configuration> configurationList = new ArrayList<Configuration>();
            connection.setAutoCommit(false);
            org.jooq.Configuration jooqConfiguration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            ConfigurationDao configurationDao = new ConfigurationDao(jooqConfiguration);

            Configuration configuration = new Configuration();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm");
            String time;

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.POLL_FREQUENCY.toString());
            configuration.setValue(String.valueOf(getConfigurationElement().getPollFrequency()));
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.TIMEOUT.toString());
            configuration.setValue(String.valueOf(getConfigurationElement().getTimeout()));
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.COLLECT_INQUIRIES_AT.toString());
            time = dateFormat.format(getConfigurationElement().getCollectInquiriesAt());
            configuration.setValue(time);
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.MDR_URL.toString());
            configuration.setValue(getConfigurationElement().getMdrUrl());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.LDM_URL.toString());
            configuration.setValue(getConfigurationElement().getLdmUrl());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.EDC_URL.toString());
            configuration.setValue(getConfigurationElement().getEdcUrl());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTP_PROXY_URL.toString());
            configuration.setValue(getConfigurationElement().getHttpProxyUrl());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTP_PROXY_PORT.toString());
            configuration.setValue(String.valueOf(getConfigurationElement().getHttpProxyPort()).trim());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTPS_PROXY_URL.toString());
            configuration.setValue(getConfigurationElement().getHttpsProxyUrl());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTPS_PROXY_PORT.toString());
            configuration.setValue(String.valueOf(getConfigurationElement().getHttpsProxyPort()).trim());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTP_PROXY_USERNAME.toString());
            configuration.setValue(getConfigurationElement().getHttpProxyUsername());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTP_PROXY_PASSWORD.toString());
            configuration.setValue(getConfigurationElement().getHttpProxyPassword());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTPS_PROXY_USERNAME.toString());
            configuration.setValue(getConfigurationElement().getHttpsProxyUsername());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.HTTPS_PROXY_PASSWORD.toString());
            configuration.setValue(getConfigurationElement().getHttpsProxyPassword());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.PROXY_REALM.toString());
            configuration.setValue(getConfigurationElement().getProxyRealm());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.TRUSTSTORE_PASSWORD.toString());
            configuration.setValue(getConfigurationElement().getTrustStorePassword());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.AUTOMATIC_ROR_UPLOAD.toString());
            configuration.setValue(getConfigurationElement().getAutomaticRorUpload());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.AUTOMATIC_ROR_UPLOAD_AT.toString());
            dateFormat = new SimpleDateFormat("HH:mm");
            time = dateFormat.format(getConfigurationElement().getRorUploadAt());
            configuration.setValue(time);
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.MAIL_RECEIVER_ADRESS.toString());
            configuration.setValue(getConfigurationElement().getMailReceiverAdress());
            configurationList.add(configuration);

            configuration = new Configuration();
            configuration.setName(EnumConfiguration.MAIL_TEILER_URL.toString());
            configuration.setValue(getConfigurationElement().getMailTeilerUrl());
            configurationList.add(configuration);

            for (Configuration conf : configurationList) {
                if (configurationDao.exists(conf)) {
                    logger.debug("configuration exists: " + conf.getName());
                    configurationDao.update(conf);
                } else {
                    logger.debug("configuration does not exist: " + conf.getName());
                    configurationDao.insert(conf);
                }
            }

            connection.commit();
            connection.close();

            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("ConfigurationController.configuration"),
                    Messages.getString("ConfigurationController.configurationSaved"));

            Utils.getApplicationBean().resetMdrContext();

            ScheduledRorUpload.scheduleRorUploadFromConfig();
            ScheduledInquiryCollection.scheduleInquiryCollectionFromConfig();

        } catch (SQLException e) {
            logger.error("Could not access the database.");
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("ConfigurationController.error"),
                    Messages.getString("ConfigurationController.dbConnectionError"));
        }
        org.omnifaces.util.Messages.add(null, msg);
    }

    /**
     * Load values of the configuration saved in the database.
     */
    private void loadValues() {
        logger.debug("Loading values from config database");
        try (Connection connection = ResourceManager.getConnection()){
            connection.setAutoCommit(false);
            org.jooq.Configuration jooqConfiguration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            ConfigurationDao configurationDao = new ConfigurationDao(jooqConfiguration);

            Configuration configuration;

            configuration = configurationDao.fetchOneByName(EnumConfiguration.POLL_FREQUENCY.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setPollFrequency(Integer.valueOf(configuration.getValue()).intValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.TIMEOUT.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setTimeout(Integer.valueOf(configuration.getValue()).intValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.COLLECT_INQUIRIES_AT.toString());
            if (isConfigurationSet(configuration)) {
                Date date = new SimpleDateFormat("HH:mm", Locale.GERMAN).parse(configuration.getValue());
                configurationElement.setCollectInquiriesAt(date);
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.LDM_URL.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setLdmUrl(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.EDC_URL.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setEdcUrl(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTP_PROXY_URL.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpProxyUrl(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTP_PROXY_PORT.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpProxyPort(configuration.getValue().trim());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTPS_PROXY_URL.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpsProxyUrl(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTPS_PROXY_PORT.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpsProxyPort(configuration.getValue().trim());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTP_PROXY_USERNAME.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpProxyUsername(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTP_PROXY_PASSWORD.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpProxyPassword(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTPS_PROXY_USERNAME.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpsProxyUsername(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.HTTPS_PROXY_PASSWORD.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setHttpsProxyPassword(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.PROXY_REALM.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setProxyRealm(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.TRUSTSTORE_PASSWORD.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setTrustStorePassword(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.AUTOMATIC_ROR_UPLOAD.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setAutomaticRorUpload(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.AUTOMATIC_ROR_UPLOAD_AT.toString());
            if (isConfigurationSet(configuration)) {
                Date date = new SimpleDateFormat("HH:mm", Locale.GERMAN).parse(configuration.getValue());
                configurationElement.setRorUploadAt(date);
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.MAIL_RECEIVER_ADRESS.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setMailReceiverAdress(configuration.getValue());
            }

            configuration = configurationDao.fetchOneByName(EnumConfiguration.MAIL_TEILER_URL.toString());
            if (isConfigurationSet(configuration)) {
                configurationElement.setMailTeilerUrl(configuration.getValue());
            }

            connection.commit();
            connection.close();

        } catch (SQLException e) {
            logger.error("Could not access the database.");
        } catch (ParseException e) {
            logger.debug(e.getMessage());
        }
    }
}
