/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.daos.ActorDao;
import de.samply.share.client.core.tables.daos.ExportDefinitionDao;
import de.samply.share.client.core.tables.pojos.Actor;
import de.samply.share.client.core.tables.pojos.ExportDefinition;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.User;

/**
 * This Bean manages the list of {@link ExportDefinition}s.
 */
@ManagedBean(name = "exportDefinitionListBean")
@ViewScoped
public class ExportDefinitionListBean implements Serializable {

    private static final long serialVersionUID = -1890767239233270145L;

    @ManagedProperty(value = "#{loginController.user}")
    private User currentUser;

    private List<ExportDefinition> exportDefinitions;

    /**
     * @return the list of loaded {@link ExportDefinition}s, ordered by ID
     */
    public List<ExportDefinition> getExportDefinitions() {
        return exportDefinitions;
    }

    /**
     * Needed for injection.
     * @param user the user that is currently logged in
     */
    public void setCurrentUser(User user) {
        this.currentUser = user;
    }

    /**
     * Loads all {@link ExportDefinition}s for the current user from the DB.
     */
    @PostConstruct
    private void loadExportsForUser() {
        try (Connection connection = ResourceManager.getConnection()) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            ActorDao actorDao = new ActorDao(configuration);
            Actor actor = actorDao.fetchOneByUsername(currentUser.getUsername());
            DSLContext create = ResourceManager.getDSLContext(connection);
            exportDefinitions = create.select().from(Tables.EXPORT_DEFINITION)
                    .where(Tables.EXPORT_DEFINITION.ACTOR_ID.equal(actor.getId()))
                    .orderBy(Tables.EXPORT_DEFINITION.ID)
                    .fetch().into(ExportDefinition.class);
        } catch (SQLException ex) {
            throw new RuntimeException("Loading ExportDefinitions failed.", ex);
        }
    }

    /**
     * Deletes an ExportDefinition in the DB.
     * @param exportDefinitionId ID of the ExportDefinition to be deleted
     * @return outcome of export management overview
     * @throws SQLException if a DB error occurs
     */
    public String delete(int exportDefinitionId) throws SQLException {
        Configuration configuration = new DefaultConfiguration().set(SQLDialect.POSTGRES);
        try (Connection connection = ResourceManager.getConnection()) {
            configuration.set(connection);
            ExportDefinitionDao exportDefinitionDao = new ExportDefinitionDao(configuration);
            exportDefinitionDao.deleteById(exportDefinitionId);
        }
        return "exportOverview";
    }

}
