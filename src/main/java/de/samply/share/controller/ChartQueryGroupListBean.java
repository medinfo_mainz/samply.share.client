/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.daos.ChartQueryGroupDao;
import de.samply.share.client.core.tables.pojos.ChartQueryGroup;
import de.samply.share.client.jdbc.ResourceManager;

/**
 * Provides a list of all saved chart query groups.
 */
@ManagedBean
@ViewScoped
public class ChartQueryGroupListBean implements Serializable {

    private static final long serialVersionUID = -2739577428898684881L;

    private List<ChartQueryGroup> chartQueryGroups;

    /**
     * Loads all available ChartQueryGroups from the DB.
     */
    @PostConstruct
    private void loadChartGroups() {
        try (Connection connection = ResourceManager.getConnection()) {
            DSLContext create = ResourceManager.getDSLContext(connection);
            chartQueryGroups = create.select().from(Tables.CHART_QUERY_GROUP)
                    .orderBy(Tables.CHART_QUERY_GROUP.ID)
                    .fetch().into(ChartQueryGroup.class);
        } catch (SQLException ex) {
            throw new RuntimeException("Loading chart query groups failed.", ex);
        }
    }

    /**
     * Gets the list of ChartQueryGroups, ordered by their id.
     * @return the list of chart query groups
     */
    public List<ChartQueryGroup> getChartQueryGroups() {
        return chartQueryGroups;
    }

    /**
     * Deletes a ChartQueryGroup in the DB.
     * @param groupId
     * @return
     * @throws SQLException
     */
    public String delete(int groupId) throws SQLException {
        Configuration configuration = new DefaultConfiguration().set(SQLDialect.POSTGRES);
        try (Connection connection = ResourceManager.getConnection()) {
            configuration.set(connection);
            ChartQueryGroupDao chartQueryGroupDao = new ChartQueryGroupDao(configuration);
            chartQueryGroupDao.deleteById(groupId);
        }
        return "chartsOverview";
    }

}
