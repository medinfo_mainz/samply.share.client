/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import de.samply.share.common.control.uiquerybuilder.AbstractSearchController;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * This class forwards the overwritten methods of {@link AbstractSearchController}
 * to a another bean implementing the {@link QueryControllerInterface}.
 * The instance of the bean must
 * be set by the specific view with {@link SearchController#setQueryController(de.samply.share.controller.QueryControllerInterface) },
 * e. g. in a view action.
 */
@ManagedBean(name = "SearchController")
@ViewScoped
public class SearchController extends AbstractSearchController {

    private static final long serialVersionUID = -8017459089132761312L;
    private QueryControllerInterface queryController;

    @Override
    public String getSerializedQuery() {
        if (queryController != null) {
            return queryController.getSerializedQuery();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSerializedQuery(String serializedQuery) {
        queryController.setSerializedQuery(serializedQuery);
    }

    @Override
    public String onSubmit() {
        serializeQuery(); // sets query to export Definition
        return queryController.onSubmit();
    }

    /**
     * This method must be called in order to set a valid query controller instance.
     * @param queryController
     */
    public void setQueryController(QueryControllerInterface queryController) {
        this.queryController = queryController;
        init();
    }

}
