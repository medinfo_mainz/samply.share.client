/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.omnifaces.util.Faces;

import com.google.common.base.Objects;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.daos.ActorDao;
import de.samply.share.client.core.tables.daos.ExportDefinitionDao;
import de.samply.share.client.core.tables.pojos.Actor;
import de.samply.share.client.core.tables.pojos.ExportDefinition;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.model.uiquerybuilder.MenuItem;
import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.listener.OsseEdcContext;
import de.samply.share.model.User;
import de.samply.share.model.osse.And;
import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.Geq;
import de.samply.share.model.osse.Leq;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.Query;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.Where;
import de.samply.share.utils.OsseLDMConnector;
import de.samply.share.utils.Utils;
import de.samply.share.utils.XmlToCSV;

/**
 * This Bean manages an {@link ExportDefinition} and handles the data export to CSV/XML.
 */
@ManagedBean(name = "exportController")
@ViewScoped
public class ExportController implements QueryControllerInterface, Serializable {

	private static final long serialVersionUID = 1487470194168138291L;

    /** The logger for this class. */
	private static final Logger logger = Logger.getLogger(ExportController.class);

	/** The mdrkey for the date of the last change of a patient's data. */
	private final String MDRKEY_CHANGED = "last_changed";

	/** The mdrkey for the date of registration of a patient. */
	private final String MDRKEY_REGISTERED = "registered_at";

	/** The mdrkey for the date of the last episode form of a patient. */
	private final String MDRKEY_EPISODE = "episode_date";

    /** Buffer size for writing to the ServletOutputStream */
    private static final int DEFAULT_SENDFILE_BUFFER_SIZE = 10240;

    private ExportDefinition exportDefinition;

    private Integer exportDefinitionId;

    /** The flash key for putting and getting the managed ExportDefinition. */
    private static final String EXPORT_DEFINITION_FLASH_KEY = "exportDefinition";

    @ManagedProperty(value = "#{viewFieldsSelector}")
    private ViewFieldsSelector viewFieldsSelector;

    @ManagedProperty(value = "#{loginController.user}")
    private User currentUser;

    /**
     * @return {@link Query} for export as string
     */
    @Override
    public String getSerializedQuery() {
        return exportDefinition.getQuery();
    }

    /**
     * @param serializedQuery sets the string that is to be used as
     * {@link Query} for the export
     */
    @Override
    public void setSerializedQuery(String serializedQuery) {
        exportDefinition.setQuery(serializedQuery);
    }

    /**
     * Needed for injection.
     * @param viewFieldsSelector injected viewFieldsSelector
     */
    public void setViewFieldsSelector(ViewFieldsSelector viewFieldsSelector) {
        this.viewFieldsSelector = viewFieldsSelector;
    }

    /**
     * Needed for injection.
     * @param currentUser the user that is currently logged in
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * @return MDR elements used in the registry as list of menu items
     */
    public List<MenuItem> getViewFieldItems() {
        return null;
    }

    /**
     * @return the ExportDefinition managed by this Bean
     */
    public ExportDefinition getExportDefinition() {
        return exportDefinition;
    }

    /**
     * (Re-)sets the ExportDefinition. Only used internally.
     * @param exportDefinition an ExportDefinition
     */
    private void setExportDefinition(ExportDefinition exportDefinition) {
        this.exportDefinitionId = exportDefinition.getId();
        this.exportDefinition = exportDefinition;
    }

    /**
     * @return gets the ID of the ExportDefinition managed by this Bean.
     */
    public Integer getExportDefinitionId() {
        return exportDefinitionId;
    }

    /**
     * @param exportDefinitionId ID of the ExportDefinition that is to be managed
     */
    public void setExportDefinitionId(Integer exportDefinitionId) {
        this.exportDefinitionId = exportDefinitionId;
    }

    /**
     * @return outcome of the ExportDefinition management page
     */
    @Override
    public String onSubmit() {
        keepExportDefinitionInFlash();
        return "exportDefinition";
    }

    /**
     * Represents the types of export.
     */
    public enum ExportType {

        ZIPPED_CSV(".zip", "application/x-zip-compressed"),
        XML(".xml", "application/xml");

        ExportType(String fileSuffix, String mimeType) {
            this.fileSuffix = fileSuffix;
            this.mimeType = mimeType;
        }

        private final String fileSuffix;
        private final String mimeType;

        /**
         * @return suffix for the downloaded file
         */
        public String getFileSuffix() {
            return fileSuffix;
        }

        /**
         * @return MIME type to be used in HTTP answer
         */
        public String getMimeType() {
            return mimeType;
        }

    }

    /**
     * Default constructor.
     */
    public ExportController() {
        exportDefinition = new ExportDefinition();
    }

	/**
	 * Writes the QueryResult als XML string to the fiven OutputStream.
	 *
     * @param queryResult the query result from the local data management
     * @param outputStream the stream to write the result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws SQLException
	 *             the SQL exception
	 * @throws JAXBException
	 *             the JAXB exception
	 */
	public void writeXml(QueryResult queryResult, OutputStream outputStream) throws IOException, SQLException, JAXBException {

		JAXBContext context = JAXBContext.newInstance(QueryResult.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://schema.samply.de/osse/QueryResult http://schema.samply.de/osse/QueryResult.xsd");
        ObjectFactory objectFactory = new ObjectFactory();
		marshaller.marshal(objectFactory.createQueryResult(queryResult), outputStream);
	}

	/**
	 * Converts the QueryResult to CSV and writes the zipped CSV files
     * to the given OutputStream.
	 *
     * @param queryResult the query result from the local data management
     * @param outputStream stream to write to
	 * @throws SQLException
	 *             the SQL exception
	 * @throws ClientProtocolException
	 *             the client protocol exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JAXBException
	 *             the JAXB exception
	 */
	private void writeZippedCsv(QueryResult queryResult, OutputStream outputStream) throws SQLException, ClientProtocolException, IOException, JAXBException {

		StringWriter csvWriter = new StringWriter();
		StringWriter mappingWriter = new StringWriter();

		XmlToCSV xml2csv = new XmlToCSV();

		xml2csv.convert(queryResult, csvWriter, mappingWriter);

		try (ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
            zipOutputStream.putNextEntry(new ZipEntry("export_data.csv"));
            zipOutputStream.write(csvWriter.toString().getBytes());
            zipOutputStream.closeEntry();

            zipOutputStream.putNextEntry(new ZipEntry("mapping.csv"));
            zipOutputStream.write(mappingWriter.toString().getBytes());
            zipOutputStream.closeEntry();
        }
	}

	/**
	 * Creates the view. Inserts the date restrictions in the Where-Clause
     * of the query.
	 *
	 * @return the created view object
	 */
	private View createView() {
		View view = new View();
		Query query = getQueryFromExportDefinition();
        List<Serializable> dateRestrictions = getDateRestrictions();

        if (!dateRestrictions.isEmpty()) {
            And newRootAndClause = new And();
            newRootAndClause.getAndOrEqOrLike().addAll(dateRestrictions);
            newRootAndClause.getAndOrEqOrLike().addAll(query.getWhere().getAndOrEqOrLike());
            query.getWhere().getAndOrEqOrLike().clear();
            query.getWhere().getAndOrEqOrLike().add(newRootAndClause);
        }

		view.setViewFields(viewFieldsSelector.getViewFields());
		view.setQuery(query);

        try {
            logger.debug("Created view for export: " + QueryConverter.asString(JAXBContext.newInstance(View.class), view));
        } catch (JAXBException ex) {
            throw new RuntimeException(ex);
        }
		return view;
	}

    /**
     * Adds an additional condition ({@link Leq} or {@ Geq}) to the given list
     * if the given date value is not null.
     *
     * @param andClause basic andClause of Query
     * @param mdrKey fixed key of date restriction (see constants in this class)
     * @param value date value
     * @param upperBound whether the restriction is an upper or lower bound
     */
    private void addDateRestriction(List<Serializable> restrictions, String mdrKey, Date value, boolean upperBound) {
        if (value != null) {
            Attribute attribute = new Attribute();
            attribute.setMdrKey(mdrKey);
            ObjectFactory objectFactory = new ObjectFactory();
            attribute.setValue(objectFactory
                    .createValue(Utils.convertDateToIsoString(value)));
            if (upperBound) {
                Leq leq = new Leq();
                leq.setAttribute(attribute);
                restrictions.add(leq);
            }
            else {
                Geq geq = new Geq();
                geq.setAttribute(attribute);
                restrictions.add(geq);
            }
        }
    }

    /**
     * @return list of conditions of types {@link Leq} or {@ Geq}
     * for the special date restrictions of the export
     */
    private List<Serializable> getDateRestrictions() {
        List<Serializable> restrictions = new ArrayList<>();

        boolean upperBound = true;
        boolean lowerBound = false;

        addDateRestriction(restrictions, MDRKEY_REGISTERED,
                exportDefinition.getChangedFrom(), lowerBound);

        addDateRestriction(restrictions, MDRKEY_REGISTERED,
                exportDefinition.getChangedTo(), upperBound);

        addDateRestriction(restrictions, MDRKEY_CHANGED,
                exportDefinition.getChangedFrom(), lowerBound);

        addDateRestriction(restrictions, MDRKEY_CHANGED,
                exportDefinition.getChangedTo(), upperBound);

        addDateRestriction(restrictions, MDRKEY_EPISODE,
                exportDefinition.getEpisodeFrom(), lowerBound);

        addDateRestriction(restrictions, MDRKEY_EPISODE,
                exportDefinition.getEpisodeTo(), upperBound);

        return restrictions;
    }

    /**
     * Sends the export as file to the user.
     * @param exportType type of distribution
     * @throws IOException
     * @throws SQLException
     * @throws JAXBException
     */
    public void sendExport(ExportType exportType)
            throws IOException, SQLException, JAXBException {

        // when an unsaved export definition is used,
        if (exportDefinition.getId() == null) {
            // get the view fields from the viewFieldsSelector
            viewFieldsSelector.serializeViewFields(exportDefinition);
        }

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        // Prepare the response and set the necessary headers.
        externalContext.setResponseBufferSize(DEFAULT_SENDFILE_BUFFER_SIZE);
        externalContext.setResponseContentType(exportType.getMimeType());
        externalContext.setResponseHeader("Content-Disposition",
                String.format("attachment;filename=\"%s\"",
                        "Export" + exportType.getFileSuffix()));

        OutputStream output = externalContext.getResponseOutputStream();

        OsseLDMConnector ldmConnector = new OsseLDMConnector();
		QueryResult queryResult = ldmConnector.getFullQueryResult(createView());

        switch (exportType) {
            case XML:
                writeXml(queryResult, output);
                break;
            case ZIPPED_CSV:
                writeZippedCsv(queryResult, output);
                break;
        }

        facesContext.responseComplete();
    }

    /**
     * Sets the given ExportDefinition in this object and performs an export.
     * @param exportDefinition the definition for the export to be performed
     * @param exportType the type of the output to the user
     * @throws IOException
     * @throws SQLException
     * @throws JAXBException
     */
    public void sendExport(ExportDefinition exportDefinition, ExportType exportType)
            throws IOException, SQLException, JAXBException {
        setExportDefinition(exportDefinition);
        viewFieldsSelector.init(exportDefinition);
        sendExport(exportType);
    }

    /**
     * Loads the ExportDefinition using the ID set by {@link #setExportDefinitionId(java.lang.Integer) }
     * retrieves it from the flash.
     */
    public void loadExportDefinition() {
        // at first try to get export definition from flash
        exportDefinition = Faces.getFlashAttribute(EXPORT_DEFINITION_FLASH_KEY);
        // if that ist not succesfull, load from db
        if (exportDefinition == null || !Objects.equal(exportDefinitionId, exportDefinition.getId())) {
            if (exportDefinitionId != null) {
                try (Connection connection = ResourceManager.getConnection()) {
                    Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
                    ExportDefinitionDao exportDao = new ExportDefinitionDao(configuration);
                    exportDefinition = exportDao.fetchOneById(exportDefinitionId);
                } catch (SQLException ex) {
                    throw new RuntimeException("Loading ExportDefinition "
                            + "from DB with id " + exportDefinitionId + " failed.", ex);
                }
            }
            else {
                exportDefinition = new ExportDefinition();
            }
        }
        else {
            logger.debug("Got exportDefinition from Flash: " + exportDefinition.toString());
        }
        viewFieldsSelector.init(exportDefinition);
    }

    /**
     * @return deserialized Query
     */
    private Query getQueryFromExportDefinition() {
        String queryString = exportDefinition.getQuery();
        if (queryString != null) {
            try {
                Query query = QueryConverter.xmlToQuery(queryString);
                if (query.getWhere() == null) {
                    query.setWhere(new Where());
                }
                return query;
            } catch (JAXBException ex) {
                throw new RuntimeException("Could not initialize ViewFields", ex);
            }
        }
        else {
            Query query = new Query();
            query.setWhere(new Where());
            return query;
        }
    }

    /**
     * Keeps the ExportDefinition for the next GET-request.
     */
    public void keepExportDefinitionInFlash() {
        Flash flash = Faces.getFlash();
        flash.put(EXPORT_DEFINITION_FLASH_KEY, exportDefinition);
        flash.setRedirect(true);
    }

    /**
     * Serializes the selected view fields and keeps them for the next GET-request.
     */
    public void serializeViewFieldsAndKeepExportDefinitionInFlash() {
        viewFieldsSelector.serializeViewFields(exportDefinition);
        keepExportDefinitionInFlash();
    }

    /**
     * Saves the managed ExportDefinition in the DB.
     * @return outcome of the export overview
     * @throws SQLException if a DB error occurs
     */
    public String save() throws SQLException {
        viewFieldsSelector.serializeViewFields(exportDefinition);

        Configuration configuration = new DefaultConfiguration().set(SQLDialect.POSTGRES);
        if (exportDefinition.getId() != null) {
            // update existing export definition
            try (Connection connection = ResourceManager.getConnection()) {
                configuration.set(connection);
                ExportDefinitionDao exportDefinitionDao = new ExportDefinitionDao(configuration);
                exportDefinitionDao.update(exportDefinition);
            }
        }
        else { // insert new exportDefinition
            try (Connection connection = ResourceManager.getConnection()) {
                configuration.set(connection);
                ActorDao actorDao = new ActorDao(configuration);
                Actor actor = actorDao.fetchOneByUsername(currentUser.getUsername());
                exportDefinition.setActorId(actor.getId());
                DSLContext dslContext = ResourceManager.getDSLContext(connection);
                Record record = dslContext
                        .insertInto(Tables.EXPORT_DEFINITION)
                        .set(Tables.EXPORT_DEFINITION.NAME, exportDefinition.getName())
                        .set(Tables.EXPORT_DEFINITION.QUERY, exportDefinition.getQuery())
                        .set(Tables.EXPORT_DEFINITION.VIEW_FIELDS, exportDefinition.getViewFields())
                        .set(Tables.EXPORT_DEFINITION.ACTOR_ID, exportDefinition.getActorId())
                        .set(Tables.EXPORT_DEFINITION.CHANGED_FROM, exportDefinition.getChangedFrom())
                        .set(Tables.EXPORT_DEFINITION.CHANGED_TO, exportDefinition.getChangedTo())
                        .set(Tables.EXPORT_DEFINITION.CREATED_FROM, exportDefinition.getCreatedFrom())
                        .set(Tables.EXPORT_DEFINITION.CREATED_TO, exportDefinition.getCreatedTo())
                        .set(Tables.EXPORT_DEFINITION.EPISODE_FROM, exportDefinition.getEpisodeFrom())
                        .set(Tables.EXPORT_DEFINITION.EPISODE_TO, exportDefinition.getEpisodeTo())
                        .returning(Tables.EXPORT_DEFINITION.ID).fetchOne();
                exportDefinition.setId(record.getValue(Tables.EXPORT_DEFINITION.ID));
            }
        }
        return "exportOverview";
    }

    /**
     * Exits the query builder and goes to main page of the export definition
     * management.
     * @return outcome of the export definition management main page
     */
    public String exitQueryBuilder() {
        keepExportDefinitionInFlash();
        return "exportDefinition";
    }

    /**
     * Restores an ExportDefinition object that has been saved locally and
     * redirects to the editing page of the ExportDefinition.
     * @throws IOException if redirection fails
     */
    public void restore() throws IOException {
        // restored values have been submitted
        keepExportDefinitionInFlash();
        exportDefinitionId = exportDefinition.getId();
        if (exportDefinitionId == null || exportDefinitionId.equals(0)) {
            Faces.redirect("exportDefinition.xhtml");
        }
        else {
            Faces.redirect("exportDefinition.xhtml?definitionId=%s",
                    exportDefinitionId.toString());
        }
    }

    /**
     * Reload the OSSE Edc Context
     */
    public void resetOsseEdcContext() {
        System.out.println("reload osse edc context");
        OsseEdcContext.reset();
    }
}