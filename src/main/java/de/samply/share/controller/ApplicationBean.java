/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mailing.MailSender;
import de.samply.common.mailing.MailSending;
import de.samply.common.mdrclient.MdrClient;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.listener.OsseEdcContext;
import de.samply.share.utils.DbUtils;
import de.samply.web.mdrFaces.MdrContext;

// TODO: Auto-generated Javadoc
/**
 * A JSF Managed Bean that is existent in the whole lifetime of the application.
 */
@ManagedBean(name = "applicationBean", eager=true)
@ApplicationScoped
public class ApplicationBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3713133584879174354L;

	/** The active tasks. */
	private static List<String> activeTasks;

	/**
	 * Initializes the ApplicationBean with an instance of the MDR Client.
	 */
	@PostConstruct
	public void init() {
		setActiveTasks(new ArrayList<String>());
		resetMdrContext();
	}

    /**
	 * Gets the Names of currently active tasks.
	 *
	 * @return the active tasks
	 */
	public static List<String> getActiveTasks() {
		return activeTasks;
	}

	/**
	 * Gets the amount of currently running tasks.
	 *
	 * @return the amount of currently running tasks
	 */
	public int getNumberOfTasks() {
		return ApplicationBean.getActiveTasks().size();
	}

	/**
	 * Sets the active tasks.
	 *
	 * @param activeTasks the new active tasks
	 */
	public static void setActiveTasks(List<String> activeTasks) {
		ApplicationBean.activeTasks = activeTasks;
	}

	/**
	 * Checks if task with given class name is running.
	 *
	 * @param classname the class name of the task to check
	 * @return true, if task is running, false otherwise
	 */
	public boolean isTaskRunning(String classname) {
		return ApplicationBean.activeTasks.contains(classname);
	}

	/**
	 * Gets the mail server from mailSending.xml.
	 *
	 * @return the mail server string
	 */
	public String getMailServer() {
	    MailSending mailSending = MailSender.loadMailSendingConfig(ProjectInfo.INSTANCE.getProjectName());
	    String host = mailSending.getHost();
	    int port = mailSending.getPort();
	    String protocol = mailSending.getProtocol();

	    if (host == null || host.length() < 1
	            || port < 1
	            || protocol == null || protocol.length() < 1) {
	        return null;
	    } else {
	        return (protocol + "://" + host + ":" + port);
	    }
	}

	/**
	 * Resets and reinitializes the mdr context.
	 */
	public void resetMdrContext() {
        try {
            String mdrUrl;
            mdrUrl = OsseEdcContext.getOsseEdcConfiguration().getMdrUrl();
            HttpConnector43 httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
            MdrClient mdrClient = new MdrClient(mdrUrl, httpConnector.getJerseyClient(mdrUrl));
            MdrContext.getMdrContext().init(mdrClient);
        } catch (HttpConnectorException e) {
            throw new RuntimeException("Could not reset MDR Context: ", e);
        }
	}

    /**
     * Gets the display name.
     *
     * @return the display name
     */
    public static String getDisplayName() {
        return "OSSE.Share";
    }

}
