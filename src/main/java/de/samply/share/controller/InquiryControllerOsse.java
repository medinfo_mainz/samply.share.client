/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.omnifaces.util.Faces;

import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.share.client.core.enums.InquiryStatus;
import de.samply.share.client.core.tables.daos.InquiryDao;
import de.samply.share.client.core.tables.daos.InquiryresultDao;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.pojos.Inquiryresult;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.common.model.uiquerybuilder.QueryItem;
import de.samply.share.model.osse.Contact;
import de.samply.share.model.osse.Info;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.QueryResultStatistic;
import de.samply.share.thread.Application;
import de.samply.share.thread.CollectInquiriesTaskOsse;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.OsseLDMConnector;
import de.samply.share.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * A JSF Managed Bean that is valid for the lifetime of a view. It provides the methods to deal with inquiries (from decentral search brokers).
 */
@ManagedBean(name = "inquiryController")
@ViewScoped
public class InquiryControllerOsse extends AbstractInquiryController implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 9140568869920781616L;

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(InquiryControllerOsse.class);

    /** The Constant SHOW_RESULT_PATH. */
    private static final String SHOW_RESULT_PATH = "/patientlistSearchView.xhtml";

    /** The query result. */
    private QueryResult queryResult;

    /** The statistics for the query result. */
    private QueryResultStatistic queryResultStatistic;

    /**  Additional information about the selected inquiry. */
    private Info selectedInquiryInfo;

    /**  Contact information about the inquirer of the selected inquiry. */
    private Contact selectedInquiryContact;

    /**
     * Gets the query result statistic.
     *
     * @return the query result statistic
     */
    public QueryResultStatistic getQueryResultStatistic() {
        return queryResultStatistic;
    }

    /**
     * Sets the query result statistic.
     *
     * @param queryResultStatistic the new query result statistic
     */
    public void setQueryResultStatistic(QueryResultStatistic queryResultStatistic) {
        this.queryResultStatistic = queryResultStatistic;
    }

    /**
     * Gets the selected inquiry info.
     *
     * @return the selected inquiry info
     */
    public Info getSelectedInquiryInfo() {
        return selectedInquiryInfo;
    }

    /**
     * Sets the selected inquiry info.
     *
     * @param selectedInquiryInfo the new selected inquiry info
     */
    public void setSelectedInquiryInfo(Info selectedInquiryInfo) {
        this.selectedInquiryInfo = selectedInquiryInfo;
    }

    /**
     * Gets the selected inquiry contact.
     *
     * @return the selected inquiry contact
     */
    public Contact getSelectedInquiryContact() {
        return selectedInquiryContact;
    }

    /**
     * Sets the selected inquiry contact.
     *
     * @param selectedInquiryContact the new selected inquiry contact
     */
    public void setSelectedInquiryContact(Contact selectedInquiryContact) {
        this.selectedInquiryContact = selectedInquiryContact;
    }

    /**
     * Gets the query result.
     *
     * @return the query result
     */
    public QueryResult getQueryResult() {
        return queryResult;
    }

    /**
     * Sets the query result.
     *
     * @param queryResult
     *            the new query result
     */
    public void setQueryResult(QueryResult queryResult) {
        this.queryResult = queryResult;
    }

    /**
     * Initializes the Inquiry Controller.
     */
    @Override
    @PostConstruct
    public void init() {
        super.init();
    }

    /* (non-Javadoc)
     * @see de.samply.share.controller.AbstractInquiryController#showResults()
     */
    @Override
    public boolean showResults() {
        if (getSelectedInquiry().getResultId() == null) {
            return false;
        }
        OsseLDMConnector ldmConnector = new OsseLDMConnector();
        try {
            return ldmConnector.isResultAvailable(DbUtils.getResultLocationByInquiry(getSelectedInquiry()));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Collect inquiries from all searchbrokers the client is currently registered to.
     */
    @Override
    public void collectInquiries() {
        CollectInquiriesTaskOsse collectInquiriesTask = new CollectInquiriesTaskOsse();
        Application.execInBackground(collectInquiriesTask);
    }

    /**
     * Gets the result count for a given inquiry by its id.
     *
     * @param inquiryId
     *            the inquiry id
     * @return the result count
     */
    @Override
    public String getResultCountById(int inquiryId) {
        String resultCount = null;
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            InquiryDao inquiryDao = new InquiryDao(configuration);
            Inquiry inquiry = inquiryDao.fetchOneById(inquiryId);

            if (inquiry.getStatus().equals(InquiryStatus.IS_LDM_ERROR)) {
                return "Fehler";
            }

            Inquiryresult inquiryresult = DbUtils.getResultByInquiry(inquiry);

            if (inquiryresult == null || inquiryresult.getLocation() == null || inquiryresult.getLocation().length() < 1) {
                logger.trace("No result found (yet)");
                return "-";
            }

            if (inquiryresult.getSize() != null) {
                return inquiryresult.getSize().toString();
            } else {
                OsseLDMConnector ldmConnector = new OsseLDMConnector();
                Integer results = ldmConnector.getResultCount(inquiryresult.getLocation());

                if (results != null) {
                    resultCount = results.toString();
                    inquiryresult.setSize(results);
                    InquiryresultDao inquiryresultDao = new InquiryresultDao(configuration);
                    inquiryresultDao.update(inquiryresult);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (resultCount == null) {
            return "-";
        } else {
            return resultCount;
        }
    }

    /**
     * Load the selected inquiry and mark it as seen.
     *
     * @throws JAXBException             the JAXB exception
     * @throws ClientProtocolException             the client protocol exception
     * @throws URISyntaxException             the URI syntax exception
     */
    @Override
    public void loadInquiry() throws JAXBException, ClientProtocolException, URISyntaxException {
        logger.debug("loading inquiry " + getSelectedInquiryId());
        OsseLDMConnector ldmConnector = new OsseLDMConnector();

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryDao inquiryDao = new InquiryDao(configuration);
            setSelectedInquiry(inquiryDao.fetchOneById(getSelectedInquiryId()));

            getSelectedInquiry().setSeen(true);
            inquiryDao.update(getSelectedInquiry());
            logger.debug("populating criteria tree");
            populateCriteriaTree();

            try {
                if (getSelectedInquiry().getResultId() != null) {
                    if (ldmConnector.isResultAvailable(DbUtils.getResultLocationByInquiry(getSelectedInquiry()))) {
                        populateQueryResult();
                    }
                }
            } catch (IOException | ExecutionException | InterruptedException | MdrConnectionException | TransformerException e) {
                logger.error("An error occurred while trying to get and transform the result from LDM.");
            }

            loadContact();
            loadInfo();
            loadInquiryArchive();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populate the criteria tree for the currently selected inquiry.
     */
    private void populateCriteriaTree() {
        TreeModel<QueryItem> tree = new ListTreeModel<>();

        if (getSelectedInquiry() == null) {
            return;
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader stringReader = new StringReader(getSelectedInquiry().getCriteria());
            JAXBElement<de.samply.share.model.osse.Query> queryElement = unmarshaller.unmarshal(new StreamSource(stringReader), de.samply.share.model.osse.Query.class);
            de.samply.share.model.osse.Query query = queryElement.getValue();
            tree = QueryConverter.queryToTree(query);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        setCriteriaTree(tree);
        return;
    }

    /**
     * Load contact.
     *
     * @throws URISyntaxException the URI syntax exception
     */
    private void loadContact() throws URISyntaxException {
        try {
            String contactString = loadContactString();
            if (contactString == null) {
                logger.error("Could not load contact");
                selectedInquiryContact = new Contact();
                return;
            }
            JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader stringReader = new StringReader(contactString);
            JAXBElement<de.samply.share.model.osse.Contact> contactElement = unmarshaller.unmarshal(new StreamSource(stringReader), de.samply.share.model.osse.Contact.class);
            selectedInquiryContact = contactElement.getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load info.
     *
     * @throws URISyntaxException the URI syntax exception
     */
    private void loadInfo() throws URISyntaxException {
        String infoString = loadInfoString();
        if (infoString == null) {
            logger.error("Could not load info");
            selectedInquiryInfo = new Info();
            return;
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader stringReader = new StringReader(infoString);
            JAXBElement<de.samply.share.model.osse.Info> infoElement = unmarshaller.unmarshal(new StreamSource(stringReader), de.samply.share.model.osse.Info.class);
            selectedInquiryInfo = infoElement.getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the pagination indices.
     *
     * @return the pagination indices
     */
    @Override
    public List<Integer> getPaginationIndices() {
        List<Integer> indices = new ArrayList<Integer>();
        if (getSelectedInquiry() == null) {
            return indices;
        }
        try {
            OsseLDMConnector ldmConnector = new OsseLDMConnector();
            QueryResultStatistic queryResultStatistic = ldmConnector.getStats(DbUtils.getResultLocationByInquiry(getSelectedInquiry()));
            // QueryResultStatistic queryResultStatistic = applicationBean.getLdmConnector().getStats(selectedInquiry.getResultLocation());
            for (int i = 0; i < queryResultStatistic.getNumberOfPages(); ++i) {
                indices.add(i + 1);
            }
        } catch (IOException | JAXBException e) {
            e.printStackTrace();
        }
        return indices;
    }

    /**
     * Populate query result.
     *
     * @throws SQLException             the SQL exception
     * @throws JAXBException             the JAXB exception
     * @throws ClientProtocolException             the client protocol exception
     * @throws IOException             Signals that an I/O exception has occurred.
     * @throws TransformerException             the transformer exception
     * @throws URISyntaxException             the URI syntax exception
     * @throws MdrConnectionException the mdr connection exception
     * @throws ExecutionException the execution exception
     * @throws InterruptedException the interrupted exception
     */
    private void populateQueryResult() throws SQLException, JAXBException, ClientProtocolException, IOException, TransformerException, URISyntaxException,
            MdrConnectionException, ExecutionException, InterruptedException {
        OsseLDMConnector ldmConnector = new OsseLDMConnector();

        String queryResultLocation = DbUtils.getResultLocationByInquiry(getSelectedInquiry());

        queryResult = ldmConnector.getResultsFromPage(queryResultLocation, 0);
        queryResultStatistic = ldmConnector.getStats(queryResultLocation);
    }

    /* (non-Javadoc)
     * @see de.samply.share.controller.AbstractInquiryController#changeResultPage()
     */
    @Override
    public void changeResultPage() {
        int page = 0;
        try {
            page = Integer.parseInt(Faces.getRequestParameter("page"));
            page = page - 1; // Paginator starts with 1, result from ldm starts with 0
        } catch (NumberFormatException e) {
            logger.warn("Could not parse page number: " + page);
        }
        OsseLDMConnector ldmConnector = new OsseLDMConnector();
        try {
            queryResult = ldmConnector.getResultsFromPage(DbUtils.getResultLocationByInquiry(getSelectedInquiry()), page);
        } catch (SQLException | JAXBException | TransformerException | IOException | MdrConnectionException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Go to query result.
     */
    public void goToQueryResult() {
        logger.debug("Going to query result");
        OsseLDMConnector ldmConnector = new OsseLDMConnector();

        try {
            String queryId = Utils.getQueryIdFromInquiry(getSelectedInquiry());
            String loginToken = ldmConnector.getLoginTokenEdc(queryId);
            String redirectUrl = ldmConnector.getEdcUrl() + SHOW_RESULT_PATH + "?signinToken=" + loginToken;
            logger.debug("Redirect URL => " + redirectUrl);
            FacesContext.getCurrentInstance().getExternalContext().redirect(redirectUrl);
        } catch (IOException e) {
            logger.error("Could not get logintoken");
        }
    }
}
