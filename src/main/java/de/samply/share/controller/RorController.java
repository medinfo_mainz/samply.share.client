/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.share.client.core.tables.daos.RegistryDao;
import de.samply.share.client.core.tables.pojos.Registry;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.Utils;

/**
 * A JSF Managed Bean that is valid for the lifetime of a view.
 * It holds methods and information necessary to handle communication with connected registries of registries
*/
@ManagedBean(name = "rorController")
@ViewScoped
public class RorController implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4634812129083467035L;

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(RorController.class);

	/** The register path. */
	private final String REGISTER_PATH = "metaregci/v0/ror";

	/** The new address. */
	private String newAddress;

	/** The new username. */
	private String newUsername;

	/** The new password. */
	private String newPassword;

	/** The new app key. */
	private String newAppKey;

	/** The ror list. */
	private List<Registry> rorList;


	/**
	 * Checks if is any ror present.
	 *
	 * @return true, if is any ror present. false otherwise
	 */
	public boolean isAnyRorPresent() {
		return (DbUtils.countRors() > 0);
	}

	/**
	 * Gets the address of a new ror.
	 *
	 * @return the address of the new ror
	 */
	public String getNewAddress() {
		return newAddress;
	}

	/**
	 * Sets the address of a new ror.
	 *
	 * @param newAddress the address of a new ror
	 */
	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	/**
	 * Gets the username for authenticating with a new ror.
	 *
	 * @return the username
	 */
	public String getNewUsername() {
		return newUsername;
	}

	/**
	 * Sets the username for authenticating with a new ror.
	 *
	 * @param newUsername the new username
	 */
	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}

	/**
	 * Gets the password for authenticating with a new ror.
	 *
	 * @return the password
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Sets the password for authenticating with a new ror.
	 *
	 * @param newPassword the new password
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * Gets the app key for authenticating with a new ror. By design of the ror, it is basically an externalized identifier for this client
	 *
	 * @return the app key
	 */
	public String getNewAppKey() {
		return newAppKey;
	}

	/**
	 * Sets the new app key.
	 *
	 * @param newAppKey the new app key
	 */
	public void setNewAppKey(String newAppKey) {
		this.newAppKey = newAppKey;
	}

	/**
	 * Gets the list of all registries of registries, to which the samply share client instance is connected.
	 *
	 * @return the list of rors
	 */
	public List<Registry> getRorList() {
		return rorList;
	}

	/**
	 * Sets the list of all registries of registries, to which the samply share client instance is connected.
	 *
	 * @param rorList the new ror list
	 */
	public void setRorList(List<Registry> rorList) {
		this.rorList = rorList;
	}

	/**
	 * Inits the RorController by reloading the list of rors from the database.
	 */
	@PostConstruct
	public void init() {
		refreshRors();
	}

	/**
	 * Refresh the list of rors from the database.
	 */
	private void refreshRors() {
	    try (Connection connection = ResourceManager.getConnection() ) {
			Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
			rorList = new ArrayList<Registry>();
			RegistryDao registryDao = new RegistryDao(configuration);
			rorList = registryDao.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Register Samply Share at a new registry of registries.
	 *
	 * @return a string representing the outcome of the action in order for JSF to redirect to or show the appropriate response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws URISyntaxException when the target address can't be parsed as an URI
	 */
	public String register() throws IOException, URISyntaxException {
		if (newAddress == null || newUsername == null || newPassword == null ||
				newAddress.length() < 1 || newUsername.length() < 1	 || newPassword.length() < 1) {
			return "error";
		}

		URL targetURL = new URL(newAddress);
		int targetPort = targetURL.getPort();
		if (targetPort < 0) {
			if (targetURL.getProtocol().equalsIgnoreCase("https"))
				targetPort = 443;
			else
				targetPort = 80;
		}

		HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
            httpConnector.addHttpAuth(newAddress, newUsername, newPassword);
        } catch (HttpConnectorException e2) {
            logger.error("Error while initializing HTTP connector");
            return "error";
        }

		CloseableHttpClient httpClient = httpConnector.getHttpClient(newAddress);

		HttpHost targetHost = new HttpHost(targetURL.getHost(), targetPort, targetURL.getProtocol());

		URI targetUri = new URI(Utils.addTrailingSlash(targetURL.getPath()) + REGISTER_PATH);
		HttpPost httpPost = new HttpPost(targetUri.normalize().toString());
		CloseableHttpResponse response = httpClient.execute(targetHost, httpPost);

		try {
			String location = response.getFirstHeader("Location").getValue();
			newAppKey = location.substring(location.lastIndexOf("/")+1);
			logger.debug("Received Appkey: " + newAppKey);
		} catch (Exception e1) {
			e1.printStackTrace();
			newAppKey = "error";
		}

		int statusCode = response.getStatusLine().getStatusCode();
		response.close();

		if (statusCode == HttpStatus.SC_CREATED) {
		    try (Connection connection = ResourceManager.getConnection() ) {
				Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
				Registry registry = new Registry();
				registry.setAddress(newAddress);
				registry.setAppKey(newAppKey);
				registry.setPassword(newPassword);
				registry.setEmail(newUsername);

				RegistryDao registryDao = new RegistryDao(configuration);
				registryDao.insert(registry);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return "rorList";
		}
		else if (statusCode == HttpStatus.SC_UNAUTHORIZED)
			return "unauthorized";
		else
			return "error";
	}

	/**
	 * Send delete command to registry of registries.
	 *
	 * @param registry the registry (of registries) to which the HTTP Delete will be sent
	 * @return true, if successful. false otherwise.
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws URISyntaxException when the stored address of the ror can't be parsed as an URI
	 */
	private boolean sendDeleteCommand(Registry registry) throws IOException, URISyntaxException {
		logger.info("Deleting Ror: " + registry.getAddress());
		URL targetURL = new URL(registry.getAddress());
		int targetPort = targetURL.getPort();
		if (targetPort < 0) {
			if (targetURL.getProtocol().equalsIgnoreCase("https"))
				targetPort = 443;
			else
				targetPort = 80;
		}

		HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
            httpConnector.addHttpAuth(registry.getAddress(), registry.getEmail(), registry.getPassword());
        } catch (HttpConnectorException e) {
            logger.error("Error while initializing HTTP connector");
            return false;
        }
		CloseableHttpClient httpClient = httpConnector.getHttpClient(registry.getAddress());

		HttpHost targetHost = new HttpHost(targetURL.getHost(), targetPort, targetURL.getProtocol());
		URI targetUri = new URI(Utils.addTrailingSlash(registry.getAddress()) + REGISTER_PATH + "/" + registry.getAppKey());
		HttpDelete httpDelete = new HttpDelete(targetUri.normalize().toString());

		httpDelete.setHeader("Content-Type", "application/xml;charset=UTF-8");
		CloseableHttpResponse response = httpClient.execute(targetHost, httpDelete);

		int statusCode = response.getStatusLine().getStatusCode();
		response.close();
		if (statusCode == HttpStatus.SC_NO_CONTENT) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Delete registry of registries from the database
	 *
	 * @param registry the registry of registries
	 * @return an outcome to redirect to the correct xhtml page
	 */
	public String deleteRor(Registry registry) {
		try {
			if (!sendDeleteCommand(registry)) {
				return "rorList";
			}
		} catch (IOException | URISyntaxException e1) {
			e1.printStackTrace();
		}

		try (Connection connection = ResourceManager.getConnection() ) {
			Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
			RegistryDao registryDao = new RegistryDao(configuration);
			registryDao.delete(registry);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "rorList";
	}
}
