/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.omnifaces.context.OmniPartialViewContext;

import de.samply.share.client.core.tables.pojos.Actor;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.model.User;
import de.samply.share.utils.BCrypt;
import de.samply.share.utils.DbUtils;

// TODO: Auto-generated Javadoc
/**
 * A JSF Managed Bean that is existent for a whole session. It handles user access.
 */
@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3530708843581593724L;

	/** The Constant SESSION_USERNAME. */
	private static final String SESSION_USERNAME = "username";

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(LoginController.class);

	/** The signin token. */
	private String signinToken;

	/** The user. */
	private User user = new User();

	/**  The user's current password. */
    private String currentPass;

    /**  The user's new password. */
    private String newPass;

    /**  The repeated input of the user's new password. */
    private String newPassRepeat;

	/** The database connection. */
	private transient Connection connection = null;


	/**
	 * Get the logged in user name.
	 *
	 * @return the user name of the logged in user
	 */
	public String getLoggedUsername() {
		// check the session
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return httpServletRequest.getSession().getAttribute(SESSION_USERNAME).toString().toLowerCase();
	}

	/**
	 * Gets the signin token.
	 *
	 * @return the signin token
	 */
	public String getSigninToken() {
		return signinToken;
	}

	/**
	 * Sets the signin token.
	 *
	 * @param signinToken the new signin token
	 */
	public void setSigninToken(String signinToken) {
		System.out.println("Login Controller: set session token to " + signinToken);
		this.signinToken = signinToken;
	}

	/**
	 * Gets the currently logged in user.
	 *
	 * @return the currently logged in user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the logged in user.
	 *
	 * @param user the logged in user
	 */
	public void setUser(User user) {
		this.user = user;
	}

    /**
     * Gets the user's current password.
     *
     * @return the user's current password
     */
    public String getCurrentPass() {
        return currentPass;
    }

    /**
     * Sets the user's current password.
     *
     * @param currentPass the new user's current password
     */
    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }

    /**
     * Gets the user's new password.
     *
     * @return the user's new password
     */
    public String getNewPass() {
        return newPass;
    }

    /**
     * Sets the user's new password.
     *
     * @param newPass the user's new password
     */
    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    /**
     * Gets the repeated input of the user's new password.
     *
     * @return the repeated input of the user's new password
     */
    public String getNewPassRepeat() {
        return newPassRepeat;
    }

    /**
     * Sets the new repeated input of the user's new password.
     *
     * @param newPassRepeat the repeated input of the user's new password
     */
    public void setNewPassRepeat(String newPassRepeat) {
        this.newPassRepeat = newPassRepeat;
    }

	/**
	 * Login with a signin token, which is generated by {@link WebService.createSigninToken(String)}.
	 * Depending on the success of this, the user is redirected to either the login screen or to the configuration screen (admin) or the export screen (exporter)
	 */
	public void loginWithToken() {
		if (signinToken == null || signinToken.length() < 1) {
			return;
		}
		FacesContext facesContext = FacesContext.getCurrentInstance();
		OmniPartialViewContext context = OmniPartialViewContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		String outcome = null;

		Actor actor = DbUtils.getActorByToken(signinToken);

		if (signinToken != null && actor != null) {
			injectLogin(request.getSession(), context, actor.getUsername());
			if (actor.getUsername().equalsIgnoreCase("exporter")) {
			    this.user = new User();
			    this.user.setUsername(actor.getUsername());
			    this.user.setPassword(actor.getPassword());

				outcome = "exportOverview";
			} else {
				outcome = "admin_login";
			}
		} else {
			logger.debug("Token rejected " + signinToken);
		}
		NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
		nh.handleNavigation(facesContext, "login", outcome);
	}

	/**
	 * Inject login.
	 * When an external application (e.g. Samply.EDC) generates a signinToken via its application key,
	 * and the user of that application is then using a link to login to samply share without entering personal credentials (username/password)
	 * the login will be injected, depending on the signinToken affiliation to a user.
	 *
	 * @param session the current http session
	 * @param context the current request context
	 * @param username the username of the injected user
	 */
	public void injectLogin(HttpSession session, OmniPartialViewContext context, String username) {
		session.setAttribute(SESSION_USERNAME, username);
		context.addArgument("loggedIn", true);
	}

	/**
	 * Try to login the user with the credentials entered on the login page.
	 *
	 * @return the outcome of the credentials check. Either back to the login page or to the configured success page (usually configuration.xhtml)
	 */
	public String login() {

	    OmniPartialViewContext context = OmniPartialViewContext.getCurrentInstance();
		FacesMessage msg = null;
		boolean loggedIn = false;

		TimeZone.setDefault(TimeZone.getTimeZone("CET"));

		try {
			connection = ResourceManager.getConnection();
			Actor actorToCheck = DbUtils.fetchActorByNameIgnoreCase(getUser().getUsername().trim());

			if (actorToCheck != null && BCrypt.checkpw(getUser().getPassword(), actorToCheck.getPassword())) {
				loggedIn = true;

				// set the session
				HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				httpServletRequest.getSession().setAttribute(SESSION_USERNAME, getUser().getUsername());
			} else {
				loggedIn = false;
				msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.loginFailedTitle"),	Messages.getString("LoginController.loginFailedMessage"));
	            FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			context.addArgument("loggedIn", loggedIn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ignore) {
				}
			}
		}

		if (loggedIn) {
		    if (getUser().getUsername().equalsIgnoreCase("admin")) {
		        return "configuration";
		    } else {
		        return "incomingInquiries";
		    }
		} else {
			return "";
		}

	}

	/**
	 * Logout.
	 *
	 * @return the outcome logout to redirect to the login page
	 */
	public String logout() {
		// invalidate the session
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().invalidate();

		redirectToPage("/index.xhtml");
		return "logout";
	}

	/**
	 * Check if any user is logged-in.
	 *
	 * @return true if a user is logged-in, false otherwise
	 */
	public boolean isUserLoggedIn() {
		// check the session
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return (httpServletRequest.getSession().getAttribute(SESSION_USERNAME) != null);
	}

	/**
	 * Redirect user to a page relative to WebContent/ e.g. "/index.xhtml"
	 * @param pageLocation the redirct target
	 */
	private void redirectToPage(String pageLocation) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.setViewRoot(facesContext.getApplication().getViewHandler().createView(facesContext, pageLocation));
		facesContext.getPartialViewContext().setRenderAll(true);
		facesContext.renderResponse();
	}

    /**
     * Refresh session.
     */
    public void refreshSession() {
        FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    /**
     * Change password.
     */
    public void changePassword() {
        FacesMessage facesMessage = null;

        Actor userToCheck = DbUtils.fetchActorByNameIgnoreCase(getUser().getUsername());

        if (userToCheck != null && BCrypt.checkpw(currentPass, userToCheck.getPassword())) {
            if (newPass.equals(newPassRepeat)) {
                if (DbUtils.changePasswordForUser(userToCheck, newPass)) {
                    logger.info("Password changed");
                    facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("LoginController.passwordChangeTitle"),
                            Messages.getString("LoginController.passwordChanged"));
                } else {
                    logger.info("Error while changing password");
                    facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.passwordChangeTitle"),
                            Messages.getString("LoginController.passwordError"));
                }
            } else {
                logger.info("Password mismatch");
                facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.passwordChangeTitle"),
                        Messages.getString("LoginController.passwordMismatch"));
            }
        } else {
            logger.info("Old Password wrong");
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.passwordChangeTitle"),
                    Messages.getString("LoginController.wrongPassword"));
        }
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }
}
