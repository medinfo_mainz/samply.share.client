/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;

import de.samply.share.client.core.tables.daos.ActorDao;
import de.samply.share.client.core.tables.pojos.Actor;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.utils.BCrypt;
import de.samply.share.utils.DbUtils;

/**
 * A JSF Managed Bean that is valid for the lifetime of a view. It holds methods and information necessary to handle communication with connected search brokers
 */
@ManagedBean(name = "userController")
@ViewScoped
public class UserController implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5348654341602813763L;

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(UserController.class);

    /** The broker list. */
    private List<Actor> userList;

    private String newUsername;
    private String newPass;
    private String newPassRepeat;

    public List<Actor> getUserList() {
        return userList;
    }

    public void setUserList(List<Actor> userList) {
        this.userList = userList;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getNewPassRepeat() {
        return newPassRepeat;
    }

    public void setNewPassRepeat(String newPassRepeat) {
        this.newPassRepeat = newPassRepeat;
    }

    /**
     * Initializes the UserController. Loads the List of brokers the client is registered to from the database and stores the corresponding status in a map.
     */
    @PostConstruct
    public void init() {
        refreshUsers();
    }

    /**
     * Refresh broker list from Database.
     */
    private void refreshUsers() {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            userList = new ArrayList<Actor>();
            ActorDao actorDao = new ActorDao(configuration);
            userList = actorDao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete broker from database.
     *
     * @param broker
     *            the broker to delete
     * @return the outcome for the jsf2 navigation
     */
    public String deleteUser(Actor user) {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            ActorDao actorDao = new ActorDao(configuration);
            actorDao.delete(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "userList";
    }

    public void createUser() {
        FacesMessage facesMessage = null;
        if (newPass == null || newPass.length() < 1) {
            logger.info("No Password provided");
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("ul.createNewUser"),
                    Messages.getString("LoginController.noPasswordProvided"));
        } else if (!newPass.equals(newPassRepeat)) {
            logger.info("Password mismatch");
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("ul.createNewUser"),
                    Messages.getString("LoginController.passwordMismatch"));
        } else {
            Actor user = new Actor();
            user.setUsername(newUsername);
            user.setPassword(BCrypt.hashpw(newPass, BCrypt.gensalt()));
            DbUtils.insertActor(user);
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("ul.createNewUser"),
                    Messages.getString("ul.userCreated"));
            refreshUsers();
        }

        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public void changeUserPassword() {
        FacesMessage facesMessage = null;
        Actor userToCheck = DbUtils.fetchActorByNameIgnoreCase(newUsername);

        if (userToCheck != null) {
            if (newPass.equals(newPassRepeat)) {
                if (DbUtils.changePasswordForUser(userToCheck, newPass)) {
                    logger.info("Password changed");
                    facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("LoginController.passwordChangeTitle"),
                            Messages.getString("LoginController.passwordChanged"));
                } else {
                    logger.info("Error while changing password");
                    facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.passwordChangeTitle"),
                            Messages.getString("LoginController.passwordError"));
                }
            } else {
                logger.info("Password mismatch");
                facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.passwordChangeTitle"),
                        Messages.getString("LoginController.passwordMismatch"));
            }
        } else {
            logger.info("Invalid username " + newUsername);
            facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("LoginController.passwordChangeTitle"),
                    Messages.getString("Unknown username"));
        }
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }
}
