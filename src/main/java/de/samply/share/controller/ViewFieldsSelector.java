/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.domain.EnumElementType;
import de.samply.common.mdrclient.domain.Result;
import de.samply.jsf.JsfUtils;
import de.samply.jsf.MdrUtils;
import de.samply.share.client.core.tables.pojos.ChartDefinition;
import de.samply.share.client.core.tables.pojos.ExportDefinition;
import de.samply.share.common.model.uiquerybuilder.MenuItem;
import de.samply.share.common.model.uiquerybuilder.MenuItemTreeManager;
import de.samply.share.listener.OsseEdcContext;
import de.samply.share.model.osse.ViewFields;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This Bean provides the list of MDR items used in the registry and the possibility to select them.
 */
@ManagedBean(name = "viewFieldsSelector")
@ViewScoped
public class ViewFieldsSelector extends ItemSearchController {

    private static final long serialVersionUID = 3100702515956408875L;

    private static final Logger logger = LogManager.getLogger(ViewFieldsSelector.class);

    /** The selected ViewFields as DTO-Object. */
    private ViewFields viewFields;

    /** The selected ViewFields as list of booleans. */
    private ArrayList<Boolean> selection;

    /** Set of MDR IDs that are used in the registry. */
    private final Set<String> registerElementIds = OsseEdcContext.getOsseEdcConfiguration().getUsedMdrElements();

    /**
     * @return the selected ViewFields as list of booleans
     */
    public List<Boolean> getSelection() {
        return selection;
    }

    /**
     * @return the selected ViewFields as DTO-Object
     */
    public ViewFields getViewFields() {
        return viewFields;
    }

    /**
     * @return whether all available MDR items are selected
     */
    public boolean isAllSelected() {
        return (selection == null || !selection.contains(false));
    }

    /**
     * Does nothing, values are submitted to selection list. Only needed for JSF value bindung.
     *
     * @param allSelected
     *            ignored
     */
    public void setAllSelected(boolean allSelected) {
    }

    /**
     * Resets MDR items and the selection.
     */
    @Override
    public void resetMenuItems() {
        menuItems.clear();

        for (Result result : getMdrRootElements().getResults()) {
            addElement(result);
        }
        if (viewFields.getMdrKey().isEmpty()) {
            // no view field is selected
            selection = new ArrayList<>(Collections.nCopies(menuItems.size(), false));
        } else if (viewFields.getMdrKey().get(0).equals("*")) {
            // all view fields are selected
            selection = new ArrayList<>(Collections.nCopies(menuItems.size(), true));
        } else {
            // update selection according to unmarshalled object
            select(viewFields.getMdrKey());
        }
    }

    @Override
    public void onMdrItemsRefresh() {
        super.onMdrItemsRefresh();
        // the menu items need to be reset - super classes are used in queryBuilder, 
        // where this is called separately
        resetMenuItems();
    }
    
    

    /**
     * Selects the items with the specified MDR keys.
     *
     * @param mdrKeys
     *            the mdr keys that should be selected.
     */
    private void select(List<String> mdrKeys) {
        selection = new ArrayList<>(Collections.nCopies(menuItems.size(), false));
        // collect keys of menuItems and their associated indexes
        HashMap<String, Integer> mdrKeyToMenuItemIndex = new HashMap<>();
        for (int i = 0; i < menuItems.size(); ++i) {
            mdrKeyToMenuItemIndex.put(menuItems.get(i).getMdrId(), i);
        }

        // select associated menuItems for all mdrkeys
        for (String mdrKey : mdrKeys) {
            Integer menutItemIndex = mdrKeyToMenuItemIndex.get(mdrKey);
            if (menutItemIndex == null) {
                logger.warn("MDR-Key " + mdrKey + " is no longer used in " + "registry, but should be selected.");
            } else {
                selection.set(menutItemIndex, true);
            }
        }
    }

    /**
     * Adds all MDR items that are members of the given Result to the list of menu items.
     *
     * @param result
     *            result having members
     */
    private void addMembers(Result result) {
        List<Result> members;
        try {
            members = mdrClient.getMembers(result.getId(), JsfUtils.getLocaleLanguage(), getAccessToken(), getMdrAuthKeyId());
        } catch (MdrConnectionException | ExecutionException ex) {
            throw new RuntimeException("Error getting members of element" + result.getId() + "with designation "
                    + result.getDesignations().get(0).getDesignation(), ex);
        }
        for (Result member : members) {
            addElement(member);
        }
    }

    /**
     * Adds a single MDR data element or recursively all elements of a MDR data element group to the list of menu items.
     *
     * @param result
     *            MDR client result containing data element or data element group
     */
    private void addElement(Result result) {
        EnumElementType elementType = EnumElementType.valueOf(result.getType());
        if (elementType == EnumElementType.DATAELEMENTGROUP) {
            addMembers(result);
        } else {
            if (elementType == EnumElementType.DATAELEMENT) {
                if (!registerElementIds.contains(result.getId())) {
                    // do nothing: filter out dataelements that are not used in the register
                } else {
                    MenuItem menuItem = MenuItemTreeManager.buildMenuItem(result.getId(), elementType, MdrUtils.getDesignation(result.getDesignations()),
                            MdrUtils.getDefinition(result.getDesignations()), new ArrayList<MenuItem>(), null);
                    menuItems.add(menuItem);
                }
            }
        }
    }

    /**
     * Serializes the selected ViewFields and sets them in the given ExportDefinition.
     *
     * @param exportDefinition
     */
    public void serializeViewFields(ExportDefinition exportDefinition) {
        // set viewfields in exportDefinition -
        // nur, wenn nicht alle ausgewählt sind:
        viewFields = new ViewFields();
        List<String> mdrKeys = viewFields.getMdrKey();
        for (int i = 0; i < selection.size(); ++i) {
            if (selection.get(i)) {
                mdrKeys.add(menuItems.get(i).getMdrId());
            }
        }

        // serialize object to string
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ViewFields.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            try (StringWriter stringWriter = new StringWriter()) {
                marshaller.marshal(viewFields, stringWriter);
                exportDefinition.setViewFields(stringWriter.toString());
            }
        } catch (JAXBException | IOException ex) {
            throw new RuntimeException("Could not serialize ViewFields", ex);
        }
    }

    /**
     * Sets the selected view field in the given ChartDefinition. It also performs a validation of the selection.
     *
     * @param chartDefinition
     * @return true, if there is exactly one view field selected; false otherwise
     */
    public boolean setViewFieldsFor(ChartDefinition chartDefinition) {
        String viewField1 = null;
        for (int i = 0; i < selection.size(); ++i) {
            if (selection.get(i) != null && selection.get(i)) {
                if (viewField1 == null) {
                    viewField1 = menuItems.get(i).getMdrId();
                } else { // two values are selected
                    viewFieldValidationFailed();
                    return false;
                }
            }
        }
        if (viewField1 == null) { // no value was selected
            viewFieldValidationFailed();
            return false;
        } else {
            chartDefinition.setViewField1(viewField1);
            return true;
        }
    }

    /**
     * Validation has failed because there has not been selected exactly one ViewField. Displays the message to the user. (Only relevant to selection for ChartDefinition.)
     */
    private void viewFieldValidationFailed() {
        FacesMessage message = new FacesMessage();
        message.setSeverity(FacesMessage.SEVERITY_ERROR);
        message.setSummary(Messages.getString("charts.viewFieldRequired"));
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("chartDefinitionForm", message);
        context.validationFailed();
    }

    /**
     * Initializes the selected ViewFields with the ViewFields of the ChartDefinition.
     *
     * @param exportDefinition
     *            the ExportDefinition whose ViewFields are to be selected
     */
    public void init(ExportDefinition exportDefinition) {
        String serializedViewFields = exportDefinition.getViewFields();
        if (serializedViewFields != null && !serializedViewFields.isEmpty()) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(ViewFields.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                viewFields = (ViewFields) unmarshaller.unmarshal(new StringReader(serializedViewFields));
            } catch (JAXBException ex) {
                throw new RuntimeException("Could not initialize ViewFields", ex);
            }
        } else {
            viewFields = new ViewFields();
        }
    }

    /**
     * Initializes the selected ViewFields with the ViewFields of the ChartDefinition.
     *
     * @param chartDefinition
     *            the ChartDefinition whose ViewFields are to be selected
     */
    public void init(ChartDefinition chartDefinition) {
        // init view fields
        viewFields = new ViewFields();
        String viewField = chartDefinition.getViewField1();
        if (viewField != null) {
            viewFields.getMdrKey().add(chartDefinition.getViewField1());
        }
    }

}
