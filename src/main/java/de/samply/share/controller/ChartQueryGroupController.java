/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.Flash;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.omnifaces.util.Faces;

import com.google.common.base.Objects;

import de.samply.share.client.core.Tables;
import de.samply.share.client.core.tables.daos.ChartDefinitionDao;
import de.samply.share.client.core.tables.daos.ChartQueryGroupDao;
import de.samply.share.client.core.tables.pojos.ChartDefinition;
import de.samply.share.client.core.tables.pojos.ChartQueryGroup;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.model.uiquerybuilder.QueryConverter;
import de.samply.share.model.Chart;
import de.samply.share.model.osse.Query;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.ViewFields;
import de.samply.share.utils.OsseLDMConnector;

/**
 * This class manages a group of charts.
 */
@ManagedBean
@ViewScoped
public class ChartQueryGroupController implements Serializable, QueryControllerInterface {

    private static final long serialVersionUID = 57134365307413884L;
    private Integer groupId;
    private ChartQueryGroup chartQueryGroup;
    private ArrayList<Chart> charts;
    private List<ChartDefinition> chartDefinitions;

    private static final String CHART_QUERY_GROUP_FLASH_KEY = "chartQueryGroup";
    private static final String CHART_DEFINITIONS_FLASH_KEY = "chartDefinitions";
    private static final Logger logger = Logger.getLogger(ChartQueryGroupController.class);

    /**
     * @return the ID of the currently managed/viewed chart group
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the ID of the chart group that is to be managed by this bean
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the charts that have been created from the loaded chart definitions
     * and the export from the registry
     */
    public List<Chart> getCharts() {
        return charts;
    }

    /**
     * @return the loaded chart definitions
     */
    List<ChartDefinition> getChartDefinitions() {
        return chartDefinitions;
    }

    /**
     * @return the currently managed chart query group
     */
    public ChartQueryGroup getChartQueryGroup() {
        return chartQueryGroup;
    }

    /**
     * @return the serialized query of the currently managed ChartQueryGroup
     */
    @Override
    public String getSerializedQuery() {
        return chartQueryGroup.getQuery();
    }

    /**
     * Sets the serialized query of the currently managed ChartQueryGroup.
     */
    @Override
    public void setSerializedQuery(String serializedQuery) {
        chartQueryGroup.setQuery(serializedQuery);
    }

    /**
     * @return outcome of chart group management page
     */
    @Override
    public String onSubmit() {
        return gotoChartGroup();
    }

    /**
     * Keeps ChartQueryGroup, its associated ChartDefinitions, as well as
     * error messages in Flash, so that the values are preserved in the
     * Post-Redirect-Get-Pattern.
     */
    public void keepInFlash() {
        Flash flash = Faces.getFlash();
        flash.setKeepMessages(true);
        flash.put(CHART_QUERY_GROUP_FLASH_KEY, chartQueryGroup);
        flash.put(CHART_DEFINITIONS_FLASH_KEY, chartDefinitions);
        flash.setRedirect(true);
    }

    /**
     * @return performs actions necessary to go back to chart group management
     * page
     */
    public String gotoChartGroup() {
        keepInFlash();
        return "chartQueryGroup";
    }

    /**
     * @return performs actions necessary to go back to chart definition
     * management page
     */
    public String gotoChartDefinition() {
        keepInFlash();
        return "chartDefinition";
    }

    /**
     * Loads the chart group from the DB or takes it from the Flash,
     * using the id given by {@link #setGroupId(java.lang.Integer) }.
     */
    public void loadChartGroup() {
        // at first try to get export definition from flash
        chartQueryGroup = Faces.getFlashAttribute(CHART_QUERY_GROUP_FLASH_KEY);
        // if that ist not succesfull, load from db
        if (chartQueryGroup == null || !Objects.equal(groupId, chartQueryGroup.getId())) {
            // load from db or create new object
            if (groupId != null) {
                try (Connection connection = ResourceManager.getConnection()) {
                    Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
                    ChartQueryGroupDao chartQueryGroupDao = new ChartQueryGroupDao(configuration);
                    chartQueryGroup = chartQueryGroupDao.fetchOneById(groupId);

                    // load charts in group
                    DSLContext create = ResourceManager.getDSLContext(connection);
                    chartDefinitions = create
                            .select().from(Tables.CHART_DEFINITION)
                            .where(Tables.CHART_DEFINITION.GROUP_ID.equal(groupId))
                            .orderBy(Tables.CHART_DEFINITION.ID)
                            .fetch().into(ChartDefinition.class);

                } catch (SQLException ex) {
                    throw new RuntimeException("Loading chart query group and charts "
                            + "from DB with group id " + groupId + " failed.", ex);
                }
            }
            else {
                // a new empty chart group is created
                chartQueryGroup = new ChartQueryGroup();
                chartDefinitions = new ArrayList<>();
            }
        }
        else {
            logger.debug("Got chart query group from Flash: " + chartQueryGroup.toString());
            // when a chart group is found in the cache, there may also be
            // a group of chartDefinitions in the flash
            chartDefinitions = Faces.getFlashAttribute(CHART_DEFINITIONS_FLASH_KEY);
            if (chartDefinitions == null) {
                chartDefinitions = new ArrayList<>();
            }
            else {
                logger.debug("Got list of chart definitions from Flash with length " + chartDefinitions.size());
            }
        }
    }

    /**
     * Performs an export by using the chart definitions' view fields and
     * the query of the chart query group and extracts the data from
     * the result that is needed to draw the charts.
     */
    public void createCharts() {
        // create charts
        charts = new ArrayList<>();

        if (chartDefinitions == null || chartDefinitions.size() < 1) {
            return;
        }

        // create view for export
        View view = new View();
        if (chartQueryGroup.getQuery() != null) {
            try {
                view.setQuery(QueryConverter.xmlToQuery(chartQueryGroup.getQuery()));
            } catch (JAXBException ex) {
                throw new RuntimeException(ex);
            }
        }
        else {
            view.setQuery(new Query());
        }
        ViewFields viewFields = new ViewFields();
        view.setViewFields(viewFields);
        for (ChartDefinition chartDefinition : chartDefinitions) {
            viewFields.getMdrKey().add(chartDefinition.getViewField1());
        }

        try {
            logger.debug("Getting export for chart with view: "
                    + QueryConverter.asString(JAXBContext.newInstance(View.class), view));
        } catch (JAXBException ex) {
            throw new RuntimeException(ex);
        }

        // perform export
        OsseLDMConnector osseLdmConnector = new OsseLDMConnector();
        QueryResult queryResult;
        try {
            queryResult = osseLdmConnector.getFullQueryResult(view);
        } catch (IOException | JAXBException | SQLException ex) {
            throw new RuntimeException(ex);
        }

        for (ChartDefinition chartDefinition : chartDefinitions) {
            charts.add(new Chart(chartDefinition, queryResult));
        }
    }

    /**
     * Saves the ChartQueryGroup and all associated ChartDefinitions in the DB.
     * @return outcome for overview of chart groups
     * @throws SQLException in case of a DB error
     */
    public String save() throws SQLException {

        Configuration configuration = new DefaultConfiguration().set(SQLDialect.POSTGRES);
        try (Connection connection = ResourceManager.getConnection()) {
            configuration.set(connection);

            if (chartQueryGroup.getId() != null) {
                // update existing chart query group
                ChartQueryGroupDao chartQueryGroupDao = new ChartQueryGroupDao(configuration);
                chartQueryGroupDao.update(chartQueryGroup);
            }
            else { // insert new chart query group
                DSLContext dslContext = ResourceManager.getDSLContext(connection);
                Record record = dslContext
                        .insertInto(Tables.CHART_QUERY_GROUP)
                        .set(Tables.CHART_QUERY_GROUP.NAME, chartQueryGroup.getName())
                        .set(Tables.CHART_QUERY_GROUP.QUERY, chartQueryGroup.getQuery())
                        .returning(Tables.CHART_QUERY_GROUP.ID).fetchOne();
                chartQueryGroup.setId(record.getValue(Tables.CHART_QUERY_GROUP.ID));
            }
            // after saving the group, the associated chart definitions are saved
            for (ChartDefinition chartDefinition : chartDefinitions) {
                if (chartDefinition.getId() != null) {
                    ChartDefinitionDao chartDefinitionDao = new ChartDefinitionDao(configuration);
                    chartDefinitionDao.update(chartDefinition);
                }
                else {
                    DSLContext dslContext = ResourceManager.getDSLContext(connection);
                    Record record = dslContext
                            .insertInto(Tables.CHART_DEFINITION)
                            .set(Tables.CHART_DEFINITION.GROUP_ID, chartQueryGroup.getId())
                            .set(Tables.CHART_DEFINITION.VIEW_FIELD1, chartDefinition.getViewField1())
                            .set(Tables.CHART_DEFINITION.TYPE, chartDefinition.getType())
                            .returning(Tables.CHART_DEFINITION.ID).fetchOne();
                    chartDefinition.setId(record.getValue(Tables.CHART_DEFINITION.ID));
                }
            }
        }

        return "chartsOverview";
    }

    /**
     * Removes a ChartDefinition from {@link #getChartDefinitions() }.
     * Also deletes it in the DB if it has been persisted.
     * @param definitionIdx index in {@link #getChartDefinitions() }
     * of ChartDefinition object that is to be deleted
     * @return outcome of chart group page
     * @throws SQLException in case of a DB error
     */
    public String deleteChart(int definitionIdx) throws SQLException {
        ChartDefinition chartDefinition = chartDefinitions.remove(definitionIdx);
        charts.remove(definitionIdx);

        if (chartDefinition.getId() != null) {
            // the chart definition has to be deleted in the database
            Configuration configuration = new DefaultConfiguration().set(SQLDialect.POSTGRES);
            try (Connection connection = ResourceManager.getConnection()) {
                configuration.set(connection);
                ChartDefinitionDao chartDefinitionDao = new ChartDefinitionDao(configuration);
                chartDefinitionDao.deleteById(chartDefinition.getId());
            }
        }
        // reload page
        return gotoChartGroup();
    }

}
