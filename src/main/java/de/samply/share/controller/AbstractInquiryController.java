/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;
import javax.xml.bind.JAXBException;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.omnifaces.model.tree.TreeModel;

import de.samply.common.http.HttpConnector43;
import de.samply.common.http.HttpConnectorException;
import de.samply.share.client.core.Tables;
import de.samply.share.client.core.enums.InquiryArchiveType;
import de.samply.share.client.core.enums.InquiryStatus;
import de.samply.share.client.core.tables.daos.BrokerDao;
import de.samply.share.client.core.tables.daos.InquiryDao;
import de.samply.share.client.core.tables.pojos.Broker;
import de.samply.share.client.core.tables.pojos.Inquiry;
import de.samply.share.client.core.tables.pojos.Inquiryarchive;
import de.samply.share.client.jdbc.ResourceManager;
import de.samply.share.common.model.uiquerybuilder.EnumOperator;
import de.samply.share.common.model.uiquerybuilder.QueryItem;
import de.samply.share.model.EnumCriteriaHit;
import de.samply.share.model.osse.Attribute;
import de.samply.share.utils.DbUtils;
import de.samply.share.utils.InquiryExecutionHandler;
import de.samply.share.utils.Utils;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractInquiryController.
 */
public abstract class AbstractInquiryController implements Serializable {

    /** The inquiries path. */
    private final String INQUIRIES_PATH = "rest/searchbroker/inquiries";

    /** The path where contact information can be found. */
    private final String CONTACT_PATH = "contact";

    /** The path where additional information can be found. */
    private final String INFO_PATH = "info";

    /** The auth header key. */
    final String AUTH_HEADER_KEY = "WWW-AUTHENTICATE";

    /** The auth header value samply. */
    final String AUTH_HEADER_VALUE_SAMPLY = "Samply";

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5890029855627854464L;

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(AbstractInquiryController.class);

    /** The criteria tree. */
    private TreeModel<QueryItem> criteriaTree;

    /** The inquiries. */
    private List<Inquiry> inquiries;

    /** The archived inquiries. */
    private List<Inquiry> archivedInquiries;

    /** The reply text. */
    private String replyText;

    /** The reply value. */
    private String replyValue;

    /** The text to store as inquiry closure note. */
    private String noteToSelf;

    /**  The archivation type (positive/negative/no reply). */
    private InquiryArchiveType archiveType;

    /** The selected inquiry. */
    private Inquiry selectedInquiry;

    /** The archivation information of the selected inquiry. */
    private Inquiryarchive selectedInquiryArchive;

    /** The selected inquiry id. */
    private int selectedInquiryId;

    /** The used http connector. */
    private transient HttpConnector43 httpConnector;

    /** The operator list. */
    private static List<String> operatorList;
    static {
        operatorList = new ArrayList<String>();
        operatorList.add(EnumOperator.EQUAL.toString());
        operatorList.add(EnumOperator.GREATER.toString());
        operatorList.add(EnumOperator.GREATER_OR_EQUAL.toString());
        operatorList.add(EnumOperator.IS_NOT_NULL.toString());
        operatorList.add(EnumOperator.IS_NULL.toString());
        operatorList.add(EnumOperator.LESS_OR_EQUAL_THEN.toString());
        operatorList.add(EnumOperator.LESS_THEN.toString());
        operatorList.add(EnumOperator.LIKE.toString());
        operatorList.add(EnumOperator.NOT_EQUAL_TO.toString());
        operatorList.add(EnumOperator.BETWEEN.toString());
    }

    /**
     * Gets the operator list.
     *
     * @return the operator list
     */
    public List<String> getOperatorList() {
        return operatorList;
    }

    /**
     * Gets the criteria tree.
     *
     * @return the criteria tree
     */
    public TreeModel<QueryItem> getCriteriaTree() {
        return criteriaTree;
    }

    /**
     * Sets the criteria tree.
     *
     * @param criteriaTree the new criteria tree
     */
    public void setCriteriaTree(TreeModel<QueryItem> criteriaTree) {
        this.criteriaTree = criteriaTree;
    }

    /**
     * Gets the inquiries.
     *
     * @return the inquiries
     */
    public List<Inquiry> getInquiries() {
        return inquiries;
    }

    /**
     * Sets the inquiries.
     *
     * @param inquiries
     *            the new inquiries
     */
    public void setInquiries(List<Inquiry> inquiries) {
        this.inquiries = inquiries;
    }

    /**
     * Gets the archived inquiries.
     *
     * @return the archived inquiries
     */
    public List<Inquiry> getArchivedInquiries() {
        return archivedInquiries;
    }

    /**
     * Sets the archived inquiries.
     *
     * @param archivedInquiries
     *            the new archived inquiries
     */
    public void setArchivedInquiries(List<Inquiry> archivedInquiries) {
        this.archivedInquiries = archivedInquiries;
    }

    /**
     * Gets the selected inquiry.
     *
     * @return the selected inquiry
     */
    public Inquiry getSelectedInquiry() {
        return selectedInquiry;
    }

    /**
     * Sets the selected inquiry.
     *
     * @param selectedInquiry
     *            the new selected inquiry
     */
    public void setSelectedInquiry(Inquiry selectedInquiry) {
        this.selectedInquiry = selectedInquiry;
    }

    /**
     * Gets the inquiry archive information.
     *
     * @return the selected inquiry archive information
     */
    public Inquiryarchive getSelectedInquiryArchive() {
        return selectedInquiryArchive;
    }

    /**
     * Sets the inquiry archive information.
     *
     * @param selectedInquiryArchive
     *            the new selected inquiry archive information
     */
    public void setSelectedInquiryArchive(Inquiryarchive selectedInquiryArchive) {
        this.selectedInquiryArchive = selectedInquiryArchive;
    }

    /**
     * Gets the selected inquiry id.
     *
     * @return the selected inquiry id
     */
    public int getSelectedInquiryId() {
        return selectedInquiryId;
    }

    /**
     * Sets the selected inquiry id.
     *
     * @param selectedInquiryId
     *            the new selected inquiry id
     */
    public void setSelectedInquiryId(int selectedInquiryId) {
        this.selectedInquiryId = selectedInquiryId;
    }

    /**
     * Gets the reply text.
     *
     * @return the reply text
     */
    public String getReplyText() {
        return replyText;
    }

    /**
     * Sets the reply text.
     *
     * @param replyText
     *            the new reply text
     */
    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }

    /**
     * Gets the reply value.
     *
     * @return the reply value
     */
    public String getReplyValue() {
        return replyValue;
    }

    /**
     * Sets the reply value.
     *
     * @param replyValue
     *            the new reply value
     */
    public void setReplyValue(String replyValue) {
        this.replyValue = replyValue;
    }

    /**
     * Gets the note to self that is used for archiving an inquiry.
     *
     * @return the note to self
     */
    public String getNoteToSelf() {
        return noteToSelf;
    }

    /**
     * Sets the note to self that is used for archiving an inquiry.
     *
     * @param noteToSelf the new note to self
     */
    public void setNoteToSelf(String noteToSelf) {
        this.noteToSelf = noteToSelf;
    }

    /**
     * Gets the archive type.
     *
     * @return the archive type
     */
    public InquiryArchiveType getArchiveType() {
        return archiveType;
    }

    /**
     * Sets the archive type.
     *
     * @param archiveType the new archive type
     */
    public void setArchiveType(InquiryArchiveType archiveType) {
        this.archiveType = archiveType;
    }

    /**
     * Initializes the Inquiry Controller.
     */
    public void init() {
        inquiries = new ArrayList<Inquiry>();
        archivedInquiries = new ArrayList<Inquiry>();
        loadInquiries();
        try {
            HashMap<String, String> configParams = DbUtils.getHttpConfigParams();
            httpConnector = new HttpConnector43(configParams);
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
        }
    }

    /**
     * Should an error information be shown for an inquiry?
     *
     * @return true, if an error occurred
     */
    public boolean showError() {
        return selectedInquiry.getStatus().equals(InquiryStatus.IS_LDM_ERROR);
    }

    /**
     * Should a processing information be shown for an inquiry?
     *
     * @return true, if processing
     */
    public boolean showProcessing() {
        return (selectedInquiry.getResultId() == null);
    }

    /**
     * Is the inquiry expired?
     *
     * @return true, if expired
     */
    public boolean showExpired() {
        if (selectedInquiry.getResultId() != null && !showResults() && !showError()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Should the result information be shown?
     *
     * @return true, if successful
     */
    public abstract boolean showResults();

    /**
     * Collect inquiries from all searchbrokers the client is currently registered to.
     */
    public abstract void collectInquiries();

    /**
     * Load all inquiries from the database / split by status.
     */
    public void loadInquiries() {
        inquiries = DbUtils.fetchInquiriesOrderByDate(Tables.INQUIRY.STATUS.in(InquiryStatus.IS_NEW,
                InquiryStatus.IS_PROCESSED, InquiryStatus.IS_PROCESSING, InquiryStatus.IS_LDM_ERROR));

        archivedInquiries = DbUtils.fetchInquiriesOrderByDate(Tables.INQUIRY.STATUS.in(InquiryStatus.IS_ARCHIVED));
    }

    /**
     * Gets the broker name for a given broker id.
     *
     * @param id
     *            the broker id
     * @return the broker name
     */
    public String getBrokerNameById(int id) {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);

            BrokerDao brokerDao = new BrokerDao(configuration);
            Broker broker = brokerDao.fetchOneById(id);
            return getBrokerName(broker);
        } catch (SQLException e) {
            e.printStackTrace();
            return "unknown";
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return "unknown";
        } catch (IOException e) {
            e.printStackTrace();
            return "unknown";
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return "unknown";
        }
    }

    /**
     * Gets the broker name.
     *
     * @param broker
     *            the broker
     * @return the broker name
     * @throws ClientProtocolException
     *             the client protocol exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws URISyntaxException
     *             the URI syntax exception
     */
    public String getBrokerName(Broker broker) throws ClientProtocolException, IOException, URISyntaxException {

        if (broker.getName() != null && broker.getName().length() > 0) {
            return broker.getName();
        }

        URL targetURL = new URL(broker.getAddress());
        int targetPort = targetURL.getPort();
        if (targetPort < 0) {
            if (targetURL.getProtocol().equalsIgnoreCase("https"))
                targetPort = 443;
            else
                targetPort = 80;
        }

        HttpConnector43 httpConnector;
        try {
            httpConnector = new HttpConnector43(DbUtils.getHttpConfigParams());
        } catch (HttpConnectorException e1) {
            logger.error("Error while initializing HTTP connector");
            return broker.getAddress();
        }

        CloseableHttpClient httpClient = httpConnector.getHttpClient(broker.getAddress());

        HttpHost targetHost = new HttpHost(targetURL.getHost(), targetPort, targetURL.getProtocol());
        URI targetUri = new URI(Utils.addTrailingSlash(targetURL.getPath()) + "rest/searchbroker/name");
        HttpGet httpGet = new HttpGet(targetUri.normalize().toString());
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
        httpGet.setConfig(requestConfig);

        CloseableHttpResponse response;
        try {
            response = httpClient.execute(targetHost, httpGet);

            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            String name = EntityUtils.toString(entity, Consts.UTF_8);
            response.close();

            if (statusCode == HttpStatus.SC_OK) {
                broker.setName(name);
                DbUtils.updateBroker(broker);
                return broker.getName();
            } else
                return broker.getAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return broker.getAddress();
        }

    }

    /**
     * Gets the result count for a given inquiry by its id.
     *
     * @param inquiryId
     *            the inquiry id
     * @return the result count
     */
    public abstract String getResultCountById(int inquiryId);

    /**
     * Load the selected inquiry and mark it as seen.
     *
     * @throws JAXBException             the JAXB exception
     * @throws ClientProtocolException             the client protocol exception
     * @throws URISyntaxException             the URI syntax exception
     */
    public abstract void loadInquiry() throws JAXBException, ClientProtocolException, URISyntaxException;

    /**
     * Load inquiry archive.
     */
    protected void loadInquiryArchive() {
        selectedInquiryArchive = DbUtils.getArchiveByInquiry(selectedInquiry);
    }

    /**
     * Load contact string.
     *
     * @return the string
     * @throws URISyntaxException the URI syntax exception
     */
    protected String loadContactString() throws URISyntaxException {
        HttpHost brokerHost = null;
        try {
            Broker broker = getBrokerForSelectedInquiry();
            brokerHost = Utils.getAsHttpHost(broker.getAddress());
            URL brokerUrl = new URL(broker.getAddress());

            CloseableHttpClient httpClient = httpConnector.getHttpClient(brokerHost);

            URI uri = new URI(Utils.addTrailingSlash(brokerUrl.getPath()) + INQUIRIES_PATH + "/" + selectedInquiry.getInquirySourceId() + "/" + CONTACT_PATH);
            HttpGet httpGet = new HttpGet(uri.normalize().toString());
            httpGet.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_SAMPLY + " " + broker.getAuthcode());

            int statusCode = 0;
            String responseString = "";
            try (CloseableHttpResponse response = httpClient.execute(brokerHost, httpGet)) {
                statusCode = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, Consts.UTF_8);
                EntityUtils.consume(entity);
            }

            if (statusCode == HttpStatus.SC_OK) {
                return responseString;
            } else {
                throw new RuntimeException("Couldn't load contact");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Load info string.
     *
     * @return the string
     * @throws URISyntaxException the URI syntax exception
     */
    protected String loadInfoString() throws URISyntaxException {
        HttpHost brokerHost = null;
        try {
            Broker broker = getBrokerForSelectedInquiry();
            brokerHost = Utils.getAsHttpHost(broker.getAddress());
            URL brokerUrl = new URL(broker.getAddress());

            CloseableHttpClient httpClient = httpConnector.getHttpClient(brokerHost);

            URI uri = new URI(Utils.addTrailingSlash(brokerUrl.getPath()) + INQUIRIES_PATH + "/" + selectedInquiry.getInquirySourceId() + "/" + INFO_PATH);
            HttpGet httpGet = new HttpGet(uri.normalize().toString());
            httpGet.addHeader(AUTH_HEADER_KEY, AUTH_HEADER_VALUE_SAMPLY + " " + broker.getAuthcode());

            int statusCode = 0;
            String responseString = "";
            try (CloseableHttpResponse response = httpClient.execute(brokerHost, httpGet)) {
                statusCode = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, Consts.UTF_8);
                EntityUtils.consume(entity);
            }

            if (statusCode == HttpStatus.SC_OK) {
                return responseString;
            } else {
                throw new RuntimeException("Couldn't load info");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Change the status of the selected inquiry.
     *
     * @param inquiryDao
     *            the inquiry dao
     * @param status
     *            the new status
     */
    private void changeSelectedInquiryStatus(InquiryDao inquiryDao, InquiryStatus status) {
        selectedInquiry.setStatus(status);
        inquiryDao.update(selectedInquiry);
    }

    /**
     * Change selected inquiry status.
     *
     * @param status the status
     * @return the string
     */
    public String changeSelectedInquiryStatus(InquiryStatus status) {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryDao inquiryDao = new InquiryDao(configuration);

            changeSelectedInquiryStatus(inquiryDao, status);

            inquiryDao.update(selectedInquiry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "incomingInquiries";
    }

    /**
     * Gets the pagination indices.
     *
     * @return the pagination indices
     */
    public abstract List<Integer> getPaginationIndices();

    /**
     * Gets the Table Row class in order to highlight it for matching results.
     *
     * @param attribute            the attr
     * @return the tr class
     */
    public String getTrClass(Attribute attribute) {
        EnumCriteriaHit criteriaHit = isCriteriaHit(attribute);
        if (criteriaHit == EnumCriteriaHit.MATCH)
            return "success";
        else if (criteriaHit == EnumCriteriaHit.MISMATCH)
            return "danger";
        else
            return "";
    }

    /**
     * Checks if a criteria is hit (currently not really used).
     *
     * @param attribute
     *            the attribute
     * @return the enum criteria hit
     */
    private EnumCriteriaHit isCriteriaHit(Attribute attribute) {
        if (criteriaTree == null || criteriaTree.getChildCount() < 1) {
            return EnumCriteriaHit.UNQUESTIONED;
        }
        return EnumCriteriaHit.UNQUESTIONED;
    }

    /**
     * Mark inquiry as unseen.
     *
     * @return the outcome
     */
    public String markSelectedInquiryAsUnseen() {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryDao inquiryDao = new InquiryDao(configuration);
            selectedInquiry.setSeen(false);
            inquiryDao.update(selectedInquiry);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "incomingInquiries";
    }

    /**
     * Archive selected inquiry.
     *
     * @return the outcome
     */
    public String archiveSelectedInquiry() {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryDao inquiryDao = new InquiryDao(configuration);

            changeSelectedInquiryStatus(inquiryDao, InquiryStatus.IS_ARCHIVED);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "incomingInquiries";
    }

    /**
     * Ignore selected inquiry.
     *
     * @return the outcome
     */
    public String ignoreSelectedInquiry() {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryDao inquiryDao = new InquiryDao(configuration);
            selectedInquiry.setIgnore(true);
            inquiryDao.update(selectedInquiry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "incomingInquiries";
    }

    /**
     * Unignore selected inquiry.
     *
     * @return the outcome
     */
    public String unignoreSelectedInquiry() {
        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            InquiryDao inquiryDao = new InquiryDao(configuration);
            selectedInquiry.setIgnore(false);
            inquiryDao.update(selectedInquiry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Set inquiry as archived with reason.
     *
     * @return the outcome
     */
    public String closeInquiry() {
        Inquiryarchive inquiryArchive = new Inquiryarchive();
        inquiryArchive.setNote(noteToSelf);
        inquiryArchive.setType(archiveType);

        int archiveId = DbUtils.insertInquiryArchive(inquiryArchive);
        selectedInquiry.setArchiveId(archiveId);
        selectedInquiry.setTrack(Boolean.FALSE);
        selectedInquiry.setStatus(InquiryStatus.IS_ARCHIVED);
        DbUtils.updateInquiry(selectedInquiry);
        return "archivedInquiries";
    }

    /**
     * Set inquiry as active.
     *
     * @return the outcome
     */
    public String openInquiry() {
        DbUtils.deleteInquiryArchive(selectedInquiryArchive);
        selectedInquiryArchive = null;
        selectedInquiry.setArchiveId(null);
        selectedInquiry.setStatus(InquiryStatus.IS_NEW);
        DbUtils.updateInquiry(selectedInquiry);
        executeSelectedInquiry();
        return "incomingInquiries";
    }

    /**
     * Gets the broker for selected inquiry.
     *
     * @return the broker for selected inquiry
     */
    private Broker getBrokerForSelectedInquiry() {
        Broker broker = null;

        try (Connection connection = ResourceManager.getConnection() ) {
            Configuration configuration = new DefaultConfiguration().set(connection).set(SQLDialect.POSTGRES);
            BrokerDao brokerDao = new BrokerDao(configuration);
            broker = brokerDao.fetchOneById(selectedInquiry.getBrokerId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return broker;
    }

    /**
     * Execute selected inquiry.
     *
     * @return the string
     */
    public String executeSelectedInquiry() {
        logger.debug("Executing selected inquiry (id=" + selectedInquiryId + ")");
        if (selectedInquiry != null && selectedInquiry.getResultId() != null) {
            DbUtils.deleteInquiryresult(selectedInquiry.getResultId());
        }
        InquiryExecutionHandler.addInquiryTask(selectedInquiry, selectedInquiry.getId().intValue());
        return "incomingInquiries";
    }

    /**
     * Change result page.
     */
    public abstract void changeResultPage();

    /**
     * Sets the tracking for the selected inquiry.
     *
     * @param event the new tracking
     */
    public void setTracking(AjaxBehaviorEvent event) {
        DbUtils.updateInquiry(selectedInquiry);
    }
}
