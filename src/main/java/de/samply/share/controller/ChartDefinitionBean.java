/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import java.io.Serializable;
import java.util.concurrent.ExecutionException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.share.client.core.enums.ChartType;
import de.samply.share.client.core.tables.pojos.ChartDefinition;
import de.samply.share.common.utils.MdrDatatype;
import de.samply.web.mdrFaces.MdrContext;

/**
 * This Bean manages a single ChartDefinition.
 */
@ManagedBean
@ViewScoped
public class ChartDefinitionBean implements Serializable {

    private static final long serialVersionUID = -3399070258993810810L;

    private Integer definitionIdx;

    /**
     * This object is only used if {@code definitionIdx < 0}.
     */
    private final ChartDefinition newChartDefinition;

    private transient MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();

    public ChartDefinitionBean() {
        definitionIdx = -1;
        newChartDefinition = new ChartDefinition();
        newChartDefinition.setType(ChartType.PIE);
    }

    @ManagedProperty(value = "#{chartQueryGroupController}")
    private ChartQueryGroupController chartQueryGroupController;

    @ManagedProperty(value = "#{viewFieldsSelector}")
    private ViewFieldsSelector viewFieldsSelector;

    /**
     * @return the ChartDefinition managed by this Bean.
     */
    public ChartDefinition getChartDefinition() {
        if (definitionIdx < 0) {
            return newChartDefinition;
        }
        else {
            // TODO check IndexOutOfBoundsException and answer with error code
            return chartQueryGroupController.getChartDefinitions().get(definitionIdx);
        }
    }

    /**
     * @return the index of the chart definition that is to be edited.
     * An index less than zero indicates a new ChartDefinition is to be created.
     */
    public Integer getDefinitionIdx() {
        return definitionIdx;
    }

    /**
     * Sets the index of the ChartDefinition that is to be managed by this Bean.
     * If the index is negative, a newly ChartDefinition is used.
     * @param definitionIdx index of ChartDefinition in {@link ChartQueryGroupController#getChartDefinitions() }
     */
    public void setDefinitionIdx(Integer definitionIdx) {
        this.definitionIdx = definitionIdx;
    }

    /**
     * Needed for injection.
     * @param chartQueryGroupController the ChartQueryGroupController to be injected
     */
    public void setChartQueryGroupController(ChartQueryGroupController chartQueryGroupController) {
        this.chartQueryGroupController = chartQueryGroupController;
    }

    /**
     * Needed for injection.
     * @param viewFieldsSelector the viewFieldsSelector to be injected
     */
    public void setViewFieldsSelector(ViewFieldsSelector viewFieldsSelector) {
        this.viewFieldsSelector = viewFieldsSelector;
    }

    /**
     * Tries to accepts the user's changes to the edited chart definition.
     * @return same page, if validation fails; otherwise go to chart query group
     */
    public String takeChartDefinition() {
        if (!viewFieldsSelector.setViewFieldsFor(getChartDefinition())) {
            // validation failed, redirect to this page again and keep messages
            chartQueryGroupController.keepInFlash();
            return "chartDefinition";
        }
        if (definitionIdx < 0) {
            chartQueryGroupController.getChartDefinitions().add(newChartDefinition);
            newChartDefinition.setGroupId(chartQueryGroupController.getGroupId());
        }
        return chartQueryGroupController.gotoChartGroup();
    }

    /**
     * Determines if an MDR item should be available for selection.
     * A bar chart can display only int and float values, a pie chart int and enumerated values.
     * @param mdrId MDR ID of menu item
     * @return whether the item with the given mdrId should be rendered
     */
    public boolean renderItem(String mdrId) {
        Validations validations;
        try {
            validations = mdrClient.getDataElementValidations(mdrId, "en");
        } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException ex) {
            throw new RuntimeException(ex);
        }

        MdrDatatype dataType = MdrDatatype.get(validations.getDatatype());
        if (getChartDefinition().getType() == ChartType.HISTOGRAM) {
            switch (dataType) {
                case FLOAT:
                case INTEGER:
                    return true;
                default:
                    return false;
            }
        } else if (dataType == MdrDatatype.CATALOG) {
            // Catalog datatype currently unsupported
            return false;
        }
        else return true;
    }

    /**
     * A call of this method makes sure that there is not an invalid
     * combination of chart type and view field in the chart definition.
     */
    public void deselectUnselectableItem() {
        String selectedItemMdrId = getChartDefinition().getViewField1();
        if (selectedItemMdrId != null && !renderItem(selectedItemMdrId)) {
            selectedItemMdrId = null;
        }
    }

    /**
     * Prepares the parameter {@code definitionIdx}
     * for going to the chart definition page.
     * @param definitionIdx parameter for chart definition page request
     * @return
     */
    public String editChartDefinition(Integer definitionIdx) {
        setDefinitionIdx(definitionIdx);
        chartQueryGroupController.keepInFlash();
        return "chartDefinition";
    }
}
