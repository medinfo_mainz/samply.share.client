/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.controller;

import de.samply.common.mdrclient.domain.EnumElementType;
import de.samply.common.mdrclient.domain.Result;
import de.samply.jsf.MdrUtils;
import de.samply.share.common.control.uiquerybuilder.AbstractItemSearchController;
import de.samply.share.common.model.uiquerybuilder.MenuItem;
import de.samply.share.common.model.uiquerybuilder.MenuItemTreeManager;
import de.samply.share.listener.OsseEdcContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * This class implements the item search for the registry's MDR elements.
 */
@ManagedBean(name="ItemSearchController")
@ViewScoped
public class ItemSearchController extends AbstractItemSearchController {

    private static final long serialVersionUID = -1947376006661034190L;
    private static final String GROUP_OF_REGISTER_ELEMENTS = "group_of_register_elements";

    /**
     * Gets the private key of the OSSE.EDC registry in Auth from the
     * OSSE.EDC Configuration.
     *
     * @return the private key string
     */
    @Override
    public String getPrivateKey() {
        return OsseEdcContext.getOsseEdcConfiguration().getMdrAuthPrivateKey();
    }

    /**
     * Gets key ID of private key from OSSE.EDC Configuration.
     * @return
     */
    @Override
    public String getMdrAuthKeyId() {
        return OsseEdcContext.getOsseEdcConfiguration().getMdrAuthKeyId();
    }

    /**
     * Gets Auth host used by MDR from OSSE.EDC Configuration.
     * @return
     */
    @Override
    public String getMdrAuthUrl() {
        return OsseEdcContext.getOsseEdcConfiguration().getMdrAuthUrl();
    }

    /**
     * Performs a search in the MDR but returns only elements used in the registry.
     * @param searchText
     * @return
     */
    @Override
    protected List<Result> searchItems(String searchText) {
        return getOnlyItemsUsedInRegistry(super.searchItems(searchText));
    }

     /**
     * Clears MDR caache and additionally loads OSSE EDC configuration.
     */
    @Override
    public void onMdrItemsRefresh() {
        OsseEdcContext.reset();
        super.onMdrItemsRefresh();
    }

    /**
     * Resets the menu items. Adds only items used in the registry.
     * Also adds an enclosing group.
     */
    @Override
    public void resetMenuItems() {
        menuItems.clear();
        ArrayList<MenuItem> registerElements = new ArrayList<>();

        // create (virtual) enclosing group for all elements in register
        MenuItem groupOfRegisterElements = MenuItemTreeManager.buildMenuItem(
                GROUP_OF_REGISTER_ELEMENTS,
                EnumElementType.DATAELEMENTGROUP,
                Messages.getString("export.registerElementsGroup"),
                "", new ArrayList<MenuItem>(), null);

        // add root elements as children
        List<Result> rootItems = getMdrRootElements().getResults();
        for (Result result : getOnlyItemsUsedInRegistry(rootItems)) {

            MenuItem menuItem = MenuItemTreeManager.buildMenuItem(result.getId(),
                    EnumElementType.valueOf(result.getType()),
                    MdrUtils.getDesignation(result.getDesignations()),
                    MdrUtils.getDefinition(result.getDesignations()),
                    new ArrayList<MenuItem>(), groupOfRegisterElements);
            registerElements.add(menuItem);
        }

        // set this group as only child
        MenuItemTreeManager.setItemAndParentsOpen(groupOfRegisterElements);
        groupOfRegisterElements.setChildren(registerElements);
        menuItems.add(groupOfRegisterElements);
    }

    /**
     * Returns only members used in registry.
     * @param mdrId MDR ID of data element group
     * @return
     */
    @Override
    protected List<Result> getGroupMembers(String mdrId) {
        return getOnlyItemsUsedInRegistry(super.getGroupMembers(mdrId));
    }


    /**
     * On click of a data element group, toggle it, unless its the group
     * of all register elements.
     * @param mdrId MDR ID of data element group
     */
    @Override
    public void onDataElementGroupClick(final String mdrId) {
        // ignore virtual enclosing group
        if (!GROUP_OF_REGISTER_ELEMENTS.equals(mdrId)) {
            super.onDataElementGroupClick(mdrId);
        }
    }

    /**
     * Filters the list and returns only results that are used in registry.
     * @param results list of Results to filter
     * @return filtered list
     */
    private List<Result> getOnlyItemsUsedInRegistry(List<Result> results) {
        List<Result> ret = new ArrayList<>();
        for (Result result : results) {
            if (isUsedItem(result)) {
                ret.add(result);
            }
        }
        return ret;
    }

    /**
     * Checks whether a given item is used in the registry.
     * @param result result containing an MDR item
     * @return
     */
    private boolean isUsedItem(Result result) {
        EnumElementType elementType = EnumElementType.valueOf(result.getType());
        if (elementType == EnumElementType.DATAELEMENT) {
            return isUsedDataElement(result);
        }
        else if (elementType == EnumElementType.DATAELEMENTGROUP) {
            return containsUsedItems(result);
        }
        else return false;
    }

    /**
     * Checks if a given data element group contains items that are used in the
     * registry.
     * @param result result containing an MDR item
     */
    private boolean containsUsedItems(Result result) {
        return !getGroupMembers(result.getId()).isEmpty();
    }

    /**
     * MDR IDs of all elements used in the registry.
     */
    private final Set<String> registerElementIds = OsseEdcContext
            .getOsseEdcConfiguration().getUsedMdrElements();

    /**
     * Checks if a given data element is used in the registry.
     * @param result result containing an MDR item
     */
    private boolean isUsedDataElement(Result result) {
        return (registerElementIds.contains(result.getId()));
    }
}
