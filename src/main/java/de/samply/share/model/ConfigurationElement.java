/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.model;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import de.samply.share.listener.OsseEdcContext;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigurationElement.
 */
@ManagedBean
@SessionScoped
public class ConfigurationElement implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8485191775704694471L;

	/** The poll frequency for querying the local data management. */
	private int pollFrequency;

	/** The timeout for querying the local data management. */
	private int timeout;

	/** The time when inquiries from decentral search are collected. */
	private Date collectInquiriesAt;

	/** The url to local data management. */
	private String ldmUrl;

    /** The url to the local EDC component. */
    private String edcUrl;

	/** The http proxy url. */
	private String httpProxyUrl;

	/** The http proxy port. */
	private String httpProxyPort;

	/** The https proxy url. */
	private String httpsProxyUrl;

	/** The https proxy port. */
	private String httpsProxyPort;

	/** The http proxy username. */
	private String httpProxyUsername;

	/** The http proxy password. */
	private String httpProxyPassword;

    /** The https proxy username. */
    private String httpsProxyUsername;

    /** The https proxy password. */
    private String httpsProxyPassword;

	/** The proxy realm. */
	private String proxyRealm;

	/** The trust store password. */
	private String trustStorePassword;

	/** Is automatic upload to registry of registries activated?. */
	private String automaticRorUpload;

	/** Perform automatic upload to registry of registries daily at this time. */
	private Date rorUploadAt;

	/** The mail adress of the receiver of any messages sent by OSSE.Share */
	private String mailReceiverAdress;

	/** The adress where OSSE.Share is reachable */
    private String mailTeilerUrl;

	/**
	 * Instantiates a new configuration element.
	 */
	public ConfigurationElement() {
	}

    /**
	 * Gets the time at which inquiries from decentral search are collected.
	 * @return the time at which inquiries are collected
	 */
	public Date getCollectInquiriesAt() {
		return collectInquiriesAt;
	}

	/**
	 * Sets the time at which inquiries from decentral search are collected.
	 *
	 * @param collectInquiriesAt the new time
	 */
	public void setCollectInquiriesAt(Date collectInquiriesAt) {
		this.collectInquiriesAt = collectInquiriesAt;
	}

	/**
	 * Gets the URL of the metadata repository (mdr).
	 *
	 * @return the mdr url
	 */
	public String getMdrUrl() {
        // In OSSE context the MDR from the registry is used
        return OsseEdcContext.getOsseEdcConfiguration().getMdrUrl();
	}

	/**
	 * Sets the URL of the metadata repository (mdr).
	 *
	 * @param mdrUrl the new mdr url
	 */
	public void setMdrUrl(String mdrUrl) {
        throw new IllegalArgumentException("MDR URL cannot be set in OSSE "
                + "but is read from OSSE EDC database.");
	}

	/**
	 * Gets the URL of the local data management.
	 *
	 * @return the ldm url
	 */
	public String getLdmUrl() {
		return ldmUrl;
	}

	/**
	 * Sets the URL of the local data management.
	 *
	 * @param ldmUrl the new ldm url
	 */
	public void setLdmUrl(String ldmUrl) {
		this.ldmUrl = ldmUrl;
	}

	/**
	 * Gets the edc url.
	 *
	 * @return the edc url
	 */
	public String getEdcUrl() {
        return edcUrl;
    }

    /**
     * Sets the edc url.
     *
     * @param edcUrl the new edc url
     */
    public void setEdcUrl(String edcUrl) {
        this.edcUrl = edcUrl;
    }

	/**
	 * Gets the timeout for querying the local data management.
	 *
	 * @return the timeout
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Sets the timeout for querying the local data management.
	 *
	 * @param timeout the new timeout
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * Gets the poll frequency for querying the local data management.
	 *
	 * @return the poll frequency
	 */
	public int getPollFrequency() {
		return pollFrequency;
	}

	/**
	 * Sets the poll frequency querying the local data management.
	 *
	 * @param pollFrequency the new poll frequency
	 */
	public void setPollFrequency(int pollFrequency) {
		this.pollFrequency = pollFrequency;
	}

	/**
	 * Gets the http proxy url.
	 *
	 * @return the http proxy url
	 */
	public String getHttpProxyUrl() {
		return httpProxyUrl;
	}

	/**
	 * Sets the http proxy url.
	 *
	 * @param httpProxyUrl the new http proxy url
	 */
	public void setHttpProxyUrl(String httpProxyUrl) {
		this.httpProxyUrl = httpProxyUrl;
	}

	/**
	 * Gets the http proxy port.
	 *
	 * @return the http proxy port
	 */
	public String getHttpProxyPort() {
		return httpProxyPort;
	}

	/**
	 * Sets the http proxy port.
	 *
	 * @param httpProxyPort the new http proxy port
	 */
	public void setHttpProxyPort(String httpProxyPort) {
		this.httpProxyPort = httpProxyPort;
	}

	/**
	 * Gets the https proxy url.
	 *
	 * @return the https proxy url
	 */
	public String getHttpsProxyUrl() {
		return httpsProxyUrl;
	}

	/**
	 * Sets the https proxy url.
	 *
	 * @param httpsProxyUrl the new https proxy url
	 */
	public void setHttpsProxyUrl(String httpsProxyUrl) {
		this.httpsProxyUrl = httpsProxyUrl;
	}

	/**
	 * Gets the https proxy port.
	 *
	 * @return the https proxy port
	 */
	public String getHttpsProxyPort() {
		return httpsProxyPort;
	}

	/**
	 * Sets the https proxy port.
	 *
	 * @param httpsProxyPort the new https proxy port
	 */
	public void setHttpsProxyPort(String httpsProxyPort) {
		this.httpsProxyPort = httpsProxyPort;
	}

	/**
	 * Gets the http proxy username.
	 *
	 * @return the http proxy username
	 */
	public String getHttpProxyUsername() {
		return httpProxyUsername;
	}

	/**
	 * Sets the http proxy username.
	 *
	 * @param httpProxyUsername the new http proxy username
	 */
	public void setHttpProxyUsername(String httpProxyUsername) {
		this.httpProxyUsername = httpProxyUsername;
	}

	/**
	 * Gets the http proxy password.
	 *
	 * @return the http proxy password
	 */
	public String getHttpProxyPassword() {
		return httpProxyPassword;
	}

	/**
	 * Sets the  httpproxy password.
	 *
	 * @param httpProxyPassword the new http proxy password
	 */
	public void setHttpProxyPassword(String httpProxyPassword) {
		this.httpProxyPassword = httpProxyPassword;
	}

    /**
     * Gets the https proxy username.
     *
     * @return the https proxy username
     */
    public String getHttpsProxyUsername() {
        return httpsProxyUsername;
    }

    /**
     * Sets the https proxy username.
     *
     * @param httpsProxyUsername the new https proxy username
     */
    public void setHttpsProxyUsername(String httpsProxyUsername) {
        this.httpsProxyUsername = httpsProxyUsername;
    }

    /**
     * Gets the https proxy password.
     *
     * @return the https proxy password
     */
    public String getHttpsProxyPassword() {
        return httpsProxyPassword;
    }

    /**
     * Sets the https proxy password.
     *
     * @param httpsProxyPassword the new https proxy password
     */
    public void setHttpsProxyPassword(String httpsProxyPassword) {
        this.httpsProxyPassword = httpsProxyPassword;
    }

	/**
	 * Gets the proxy realm.
	 *
	 * @return the proxy realm
	 */
	public String getProxyRealm() {
		return proxyRealm;
	}

	/**
	 * Sets the proxy realm.
	 *
	 * @param proxyRealm the new proxy realm
	 */
	public void setProxyRealm(String proxyRealm) {
		this.proxyRealm = proxyRealm;
	}

	/**
	 * Gets the trust store password.
	 *
	 * @return the trust store password
	 */
	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	/**
	 * Sets the trust store password.
	 *
	 * @param trustStorePassword the new trust store password
	 */
	public void setTrustStorePassword(String trustStorePassword) {
		this.trustStorePassword = trustStorePassword;
	}

	/**
	 * Checks whether automatic upload to the registries of registries is enabled.
	 * String representation is used for easier use with the db interface.
	 *
	 * @return true if activated, false if deactivated
	 */
	public String getAutomaticRorUpload() {
		return automaticRorUpload;
	}

	/**
	 * Enables/Disables the automatic upload to the registries of registries.
	 * String representation is used for easier use with the db interface.
	 *
	 * @param automaticRorUpload the new automatic ror upload
	 */
	public void setAutomaticRorUpload(String automaticRorUpload) {
		this.automaticRorUpload = automaticRorUpload;
	}

	/**
	 * Gets time at which daily automatic upload to registry of registries is performed.
	 *
	 * @return the time at which daily automatic upload is performed
	 */
	public Date getRorUploadAt() {
		return rorUploadAt;
	}

	/**
	 * Sets time at which daily automatic upload to registry of registries is performed.
	 *
	 * @param rorUploadAt the new ror upload time
	 */
	public void setRorUploadAt(Date rorUploadAt) {
		this.rorUploadAt = rorUploadAt;
	}

    /**
     * Gets the mail receiver adress.
     *
     * @return the mail receiver adress
     */
    public String getMailReceiverAdress() {
        return mailReceiverAdress;
    }

    /**
     * Sets the mail receiver adress.
     *
     * @param mailReceiverAdress the new mail receiver adress
     */
    public void setMailReceiverAdress(String mailReceiverAdress) {
        this.mailReceiverAdress = mailReceiverAdress;
    }

    /**
     * Gets the OSSE.Share url.
     *
     * @return the OSSE.Share url
     */
    public String getMailTeilerUrl() {
        return mailTeilerUrl;
    }

    /**
     * Sets the OSSE.Share url.
     *
     * @param mailTeilerUrl the new OSSE.Share url
     */
    public void setMailTeilerUrl(String mailTeilerUrl) {
        this.mailTeilerUrl = mailTeilerUrl;
    }

}
