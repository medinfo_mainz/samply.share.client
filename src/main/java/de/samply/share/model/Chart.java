/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;

import de.samply.share.client.core.enums.ChartType;
import de.samply.share.client.core.tables.pojos.ChartDefinition;
import de.samply.share.controller.Messages;
import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.Case;
import de.samply.share.model.osse.Container;
import de.samply.share.model.osse.Entity;
import de.samply.share.model.osse.Patient;
import de.samply.share.model.osse.QueryResult;

/**
 *
 */
public class Chart implements Serializable {

    private static final long serialVersionUID = -1259686777416200186L;

    private static final Logger logger = Logger.getLogger(Chart.class);

    private ChartDefinition chartDefinition;
    private String xAsJsonArray;
    private String yAsJsonArray;

    private static final int NOF_HISTOGRAM_CLASSES = 10;
    private static final String AMBIGUOUS_VALUE_KEY = "charts.ambiguousValue";
    private static final String NO_VALUE_KEY = "charts.noValue";

    private HashMap<Patient, ArrayList<String>> valuesByPatients;

    public final void addValue(Patient patient, String value) {
        ArrayList<String> values = valuesByPatients.get(patient);
        if (values != null) {
            values.add(value);
        }
        else {
            values = new ArrayList<>();
            values.add(value);
            valuesByPatients.put(patient, values);
        }
    }

    /**
     * For testing only.
     */
    public Chart() {
        chartDefinition = new ChartDefinition();
        chartDefinition.setType(ChartType.HISTOGRAM);
        valuesByPatients = new HashMap<>();
    }

    public Chart(ChartDefinition chartDefinition, QueryResult queryResult) {
        this.chartDefinition = chartDefinition;
        valuesByPatients = new HashMap<>();

        for (Entity entPatient : queryResult.getEntity()) {
            Patient patient = (Patient) entPatient;
            for (Case _case : patient.getCase()) {
                for (Container container : _case.getContainer()) {
                    addValues(patient, container);
                }
            }
            // if the patient does not have the mdr attribute, remember by
            // putting an empty list
            if (!valuesByPatients.containsKey(patient)) {
                valuesByPatients.put(patient, new ArrayList<String>());
            }
        }

        logger.debug("chart.type is " + chartDefinition.getType());
        switch (chartDefinition.getType()) {
            case HISTOGRAM: setXYForHistogram(); break;
            case PIE: setXYForPieChart(); break;
        }
        logger.debug("chart.xAsJsonArray is " + xAsJsonArray);
        logger.debug("chart.yAsJsonArray is " + yAsJsonArray);

    }

    private void setXYForHistogram() {

        ArrayList<Double> values = new ArrayList<>();
        for (Map.Entry<Patient, ArrayList<String>> patientEntry : valuesByPatients.entrySet()) {
            // take average of values
            double sum = 0;
            boolean nonEmptyValueFound = false;
            for (String value : patientEntry.getValue()) {
                if (value != null && !value.isEmpty()) {
                    // for a bar chart, the values have to be numeric
                    sum += Double.parseDouble(value);
                    nonEmptyValueFound = true;
                }
            }
            if (nonEmptyValueFound) {
                values.add(sum / patientEntry.getValue().size());
            }
        }

        if (!values.isEmpty()) {

            if (values.size() > 1) {
                double minValue = Collections.min(values);
                double maxValue = Collections.max(values);

                double classWidth = (maxValue - minValue) / NOF_HISTOGRAM_CLASSES;

                double[] x = new double[NOF_HISTOGRAM_CLASSES + 1];
                for (int i = 0; i < NOF_HISTOGRAM_CLASSES + 1; ++i) {
                    x[i] = minValue + classWidth * i;
                }

                String[] xValuesAsJsonString = new String[NOF_HISTOGRAM_CLASSES];
                for (int i = 0; i < NOF_HISTOGRAM_CLASSES; ++i) {
                    xValuesAsJsonString[i] = "'" + formatDouble(x[i])
                            + " - " + formatDouble(x[i+1]) + "'";
                }
                xAsJsonArray = "[" + Joiner.on(", ").join(xValuesAsJsonString) + "]";

                Integer[] classCount = new Integer[NOF_HISTOGRAM_CLASSES];
                Arrays.fill(classCount, 0);
                for (double value : values) {
                    int classIndex = (int) ((value - minValue) / classWidth);
                    if (classIndex < NOF_HISTOGRAM_CLASSES) {
                        classCount[classIndex] += 1;
                    }
                    else {
                        classCount[NOF_HISTOGRAM_CLASSES - 1] += 1;
                    }

                }
                yAsJsonArray = "[" + Joiner.on(", ").join(classCount) + "]";
            }
            else {
                xAsJsonArray = "['"+ values.get(0) + "']";
                yAsJsonArray = "[1]";
            }
        }
        else {
            // no data to display
            xAsJsonArray = null;
            xAsJsonArray = null;
        }
    }

    private void setXYForPieChart() {
        // values of the attributes,
        // mapped to the number of patients, where they occur
        TreeMap<String, Integer> countedValues = new TreeMap<>();

        // for each patient
        for (Map.Entry<Patient, ArrayList<String>> patientEntry : valuesByPatients.entrySet()) {

            // determine value
            String value;
            if (patientEntry.getValue().isEmpty()) {
                value = Messages.getString(NO_VALUE_KEY);
            }
            else if ((new HashSet<>(patientEntry.getValue())).size() > 1) {
                // there is more than one value for the attribute
                value = Messages.getString(AMBIGUOUS_VALUE_KEY);
            }
            else { // there are values and they are all the same
                value = patientEntry.getValue().get(0);
            }

            // increment counter for value
            Integer count = countedValues.get(value);
            if (count == null) {
                countedValues.put(value, 1);
            } else {
                countedValues.put(value, count + 1);
            }
        }

        if (countedValues.isEmpty()) {
            xAsJsonArray = null;
            yAsJsonArray = null;
        }
        else {
            // tree map guarantees that values are returned in ascending order of the keys
            xAsJsonArray = "['" + Joiner.on("', '").join(countedValues.keySet()) + "']";
            yAsJsonArray = "[" + Joiner.on(", ").join(countedValues.values()) + "]";
        }

    }

    private static String formatDouble(double aDouble) {
        return String.format("%.1f", aDouble);
    }

    /**
     * Recursively adds values for the chart definition's MDR key (=view field).
     * @param patient the patient from whom the values are
     * @param container a container containing attributes with MDR keys and values
     */
    private void addValues(Patient patient, Container container) {
        for (Attribute attribute : container.getAttribute()) {
            String mdrKey = attribute.getMdrKey();
            if (chartDefinition.getViewField1().equals(mdrKey)) {
                addValue(patient, attribute.getValue().getValue());
            }
        }
        for (Container childContainer : container.getContainer()) {
            addValues(patient, childContainer);
        }
    }

    public String getXAsJsonArray() {
        return xAsJsonArray;
    }

    public String getYAsJsonArray() {
        return yAsJsonArray;
    }

    public boolean isDataAvailable() {
        return xAsJsonArray != null;
    }

    public ChartDefinition getChartDefinition() {
        return chartDefinition;
    }

}
