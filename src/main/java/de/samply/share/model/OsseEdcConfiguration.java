/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Encapsulates information from the OSSE.EDC Configuration that is relevant in
 * the MDR client.
 */
public class OsseEdcConfiguration {

    @SerializedName("auth.keyid")
    private String mdrAuthKeyId;

    @SerializedName("auth.rest")
    private String mdrAuthUrl;

    @SerializedName("auth.my.privkey")
    private String mdrAuthPrivateKey;

    /**
     * Keys contain the MDR elements used in forms but not those used only in records.
     */
    @SerializedName("mdrEntityIsInForm")
    private Map<String, Object> mdrElementsInForms; // only keys are important

    /**
     * Keys are the mdr keys of the records, values are the lists of mdr elements
     * used in the records.
     */
    @SerializedName("recordHasMdrEntities")
    private Map<String, Object> mdrElementsInRecords;

    @SerializedName("mdr.rest")
    private String mdrUrl;

    private Set<String> usedMdrElements;

    /**
     * @return the set of MDR IDs that are used in the OSSE.EDC registry
     */
    @SuppressWarnings("unchecked")
    public Set<String> getUsedMdrElements() {
        if (usedMdrElements == null) {
            // compute mdr elements used in registry from mdr elements in
            // forms and mdr elements in records
            if (mdrElementsInForms == null) {
                mdrElementsInForms = new HashMap<String, Object>();
            }
            usedMdrElements = new HashSet<>(mdrElementsInForms.keySet());

            if (mdrElementsInRecords == null) {
                mdrElementsInRecords = new HashMap<String, Object>();
            }
            for (Object mdrElementsInRecord : mdrElementsInRecords.values()) {
                if (mdrElementsInRecord instanceof String) {
                    usedMdrElements.add((String) mdrElementsInRecord);
                } else {
                    usedMdrElements.addAll((List<String>) mdrElementsInRecord);
                }
            }
        }
        return usedMdrElements;
    }

    /**
     * @return the key ID of the registry as user on the Auth server that is used
     * by the MDR
     */
    public String getMdrAuthKeyId() {
        return mdrAuthKeyId;
    }

    /**
     * @return the URL of the Auth server that is used by the MDR
     */
    public String getMdrAuthUrl() {
        return mdrAuthUrl;
    }

    /**
     * @return the private key of the OSSE.EDC registry to login at the MDR
     */
    public String getMdrAuthPrivateKey() {
        return mdrAuthPrivateKey;
    }

    /**
     * @return the URL of the MDR used by the OSSE.EDC registry
     */
    public String getMdrUrl() {
        return mdrUrl;
    }

}
