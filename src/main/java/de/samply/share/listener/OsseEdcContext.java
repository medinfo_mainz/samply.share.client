/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.listener;

import com.google.gson.Gson;
import de.samply.common.config.Postgresql;
import de.samply.common.config.Store;
import de.samply.config.util.FileFinderUtil;
import de.samply.config.util.JAXBUtil;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.model.OsseEdcConfiguration;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * This class loads and provides the configuration of OSSE.EDC.
 */
public class OsseEdcContext implements ServletContextListener {

    private static OsseEdcConfiguration osseEdcConfiguration;

    /**
     * @return the OSSE.EDC configuration
     */
    public static OsseEdcConfiguration getOsseEdcConfiguration() {
        return osseEdcConfiguration;
    }

    /**
     * Read configuration from Database of edc.osse.
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        reset();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    /**
     * Creates a connection to the OSSE.EDC databas
     * by using the OSSE.EDC {@code backend.xml} configuration file.
     * @return a connection to the OSSE.EDC database
     * @throws SQLException if a connection could not be established
     */
    private static Connection getOsseEdcDbConnection() throws SQLException {
        File registryDbConfig;
        try {
            registryDbConfig = FileFinderUtil.findFile("backend.xml", ProjectInfo.INSTANCE.getProjectName());
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Configuration file of registry could not be found.", ex);
        }
        Store store;
        try {
            store = JAXBUtil.unmarshall(registryDbConfig, JAXBContext.newInstance(Store.class), Store.class);
        } catch (JAXBException | SAXException | ParserConfigurationException | FileNotFoundException ex) {
            throw new RuntimeException("Configuration file of registry could not be parsed.", ex);
        }

        Postgresql dbconfig = store.getPostgresql();
        String osseEdcDatabaseUrl = "jdbc:postgresql://" + dbconfig.getHost() + "/" + dbconfig.getDatabase();
        String osseEdcUser = dbconfig.getUsername();
        String password = dbconfig.getPassword();

        return DriverManager.getConnection(osseEdcDatabaseUrl, osseEdcUser, password);
    }

    /**
     * Resets the OsseEdcContext.
     */
    public static void reset() {
        String sql = "SELECT data FROM configs WHERE name = 'osse'";
        try (Connection con = getOsseEdcDbConnection(); PreparedStatement ps = con.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {

            rs.next();
            osseEdcConfiguration = new Gson().fromJson(rs.getString("data"), OsseEdcConfiguration.class);
        } catch (SQLException ex) {
            throw new RuntimeException("Error reading OSSE EDC configuration from database.", ex);
        }
    }
}
