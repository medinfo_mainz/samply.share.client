/*
 * Copyright (C) 2015 Working Group on Joint Research,
 * Division of Medical Informatics,
 * Institute of Medical Biometrics, Epidemiology and Informatics,
 * University Medical Center of the Johannes Gutenberg University Mainz
 *
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.share.listener;

import java.io.IOException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import de.samply.config.util.FileFinderUtil;
import de.samply.share.common.utils.ProjectInfo;
import de.samply.share.utils.Config;
import de.samply.share.utils.CustodianDailyRollingFileAppender;
import de.samply.share.utils.ScheduledInquiryCollection;
import de.samply.share.utils.ScheduledMailSending;
import de.samply.share.utils.ScheduledRorUpload;
import de.samply.share.utils.Utils;

/**
 * Implements a ServletContextListener. Holds some applicationwide settings for other methods to access
 *
 */
public class StartupListener implements javax.servlet.ServletContextListener {

	/** The Constant logger. */
	private final static Logger logger = Logger.getLogger(StartupListener.class);

	private static ExecutorService executor;

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
        logger.info("Shutting down Scheduled RoR Upload Service");
        ScheduledRorUpload.shutdownService();
        logger.info("Shutting down Executor Service");
		executor.shutdown();
		logger.info("Shutting down Inquiry Collection Service");
		ScheduledInquiryCollection.shutdownService();
        logger.info("Shutting down Mail Service");
        ScheduledMailSending.shutdownService();

		// This manually deregisters JDBC driver, which prevents Tomcat 7 from complaining about memory leaks to this class
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);
				logger.info("Deregistering jdbc driver: " + driver);
			} catch (SQLException e) {
				logger.fatal("Error deregistering driver:" + driver + "\n" + e.getMessage());
			}

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ProjectInfo.INSTANCE.initProjectMetadata(sce);
		logger.info("Starting OSSE.Share...");

		Config c = Config.instance;
        ProjectInfo.INSTANCE.setConfig(c);

        log4jSetup();

		createExecutor();
		ScheduledRorUpload.scheduleRorUploadFromConfig();
		ScheduledInquiryCollection.scheduleInquiryCollectionFromConfig();
		ScheduledMailSending.scheduleMailSendingHourly();
	}

    public static synchronized void submitTask(Runnable runnable) {
        if (executor == null) {
            createExecutor();
        }
        executor.submit(runnable);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static synchronized Future<String> submitTask(Callable callable) {
        if (executor == null) {
            createExecutor();
        }
        return executor.submit(callable);
    }

    static void createExecutor() {
        executor = Executors.newFixedThreadPool(10);
    }

    /**
     * Log4j setup.
     */
    private void log4jSetup() {
        Logger root = Logger.getRootLogger();
        root.setLevel(Config.instance.getLogLevel());
        String logFileName = Utils.addTrailingFileSeparator(FileFinderUtil.getLogDir(ProjectInfo.INSTANCE.getProjectName())) + "samply.share.log";
        logger.info("Logging to " + logFileName);
        PatternLayout layout = new PatternLayout("%d{MMM dd yyyy HH:mm:ss,SSS} %-5p %c %x - %m%n");
        try {
            CustodianDailyRollingFileAppender appender;
            appender = new CustodianDailyRollingFileAppender(layout, logFileName, "'.'yyyy-MM-dd");
            appender.setMaxNumberOfDays("10");
            appender.setCompressBackups("true");
            appender.setName("SamplyShareFileAppender");

            if (!Config.instance.debugIsOn()) {
                root.warn("Redirecting IMBEI Commons log to " + logFileName + ".");
                root.removeAllAppenders();
            }

            root.addAppender(appender);
            root.info("Logger setup to log on level " + Config.instance.getLogLevel() + " to " + logFileName);
        } catch (IOException e) {
            root.fatal("Unable to log to " + logFileName + ": " + e.getMessage());
            return;
        }
    }
}