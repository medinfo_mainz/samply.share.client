# Samply.Share.Client

Samply.Share.Client handles reporting registry information to a registry of registries as well as exporting patient data to xml or csv files.

# Features

- report registry information to registry of registries
- prepare data exports based on queries against Samply.Store

# Build

In order to build this project, you need to configure maven properly. See
Samply.Maven for more information.

Use maven to build the war:

``` 
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>share-client</artifactId>
    <version>1.1.1</version>
</dependency>
```

# Installation

The configuration file (samply.share.conf) needs to be placed in the folder /etc/osse/. 
In samply.share.conf the database is configurated and the log level is set.
The sql/ddl.sql script defines the the tables needed in the database. The user set in samply.share.conf should be made owner of the tables and sequences created with that script.
The dml.sql script adds the basic configuration, you must edit this values to fit your installation. Especially the two Tokens that are added must be changed and the passwords for the 2 roles have to be set (BCrypt hashed). You need to enter the apiKeys created for the edc component. See Samply.edc for more information.