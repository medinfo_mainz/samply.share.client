INSERT INTO Configuration(Name, Value) VALUES ('COLLECT_INQUIRIES_AT', '04:00');
INSERT INTO Configuration(Name, Value) VALUES ('POLL_FREQUENCY', '10');
INSERT INTO Configuration(Name, Value) VALUES ('TIMEOUT', '1440');
INSERT INTO Configuration(Name, Value) VALUES ('MDR_URL', 'https://mdr.osse-register.de/v3/api/mdr');
INSERT INTO Configuration(Name, Value) VALUES ('LDM_URL', '');
INSERT INTO Configuration(Name, Value) VALUES ('EDC_URL', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTP_PROXY_URL', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTP_PROXY_PORT', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTPS_PROXY_URL', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTPS_PROXY_PORT', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTP_PROXY_USERNAME', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTP_PROXY_PASSWORD', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTPS_PROXY_USERNAME', '');
INSERT INTO Configuration(Name, Value) VALUES ('HTTPS_PROXY_PASSWORD', '');
INSERT INTO Configuration(Name, Value) VALUES ('PROXY_REALM', '');
INSERT INTO Configuration(Name, Value) VALUES ('TRUSTSTORE_PASSWORD', NULL);
INSERT INTO Configuration(Name, Value) VALUES ('AUTOMATIC_ROR_UPLOAD', 'false');
INSERT INTO Configuration(Name, Value) VALUES ('AUTOMATIC_ROR_UPLOAD_AT', '02:00');
INSERT INTO Configuration(Name, Value) VALUES ('DB_VERSION', '1.2.0');
INSERT INTO Configuration(Name, Value) VALUES ('MAIL_TEILER_URL', '');

-- die apikeys die im register gebraucht werden.
INSERT INTO Tokens(id, apikey) VALUES(1, 'ZvDKWpgCXSeed23MfbHs');
INSERT INTO Tokens(id, apikey) VALUES(2, 'DR2R0OCNc0iVflKx5Y7h');

INSERT INTO Actor (id,username,password,name, tokens_id) VALUES (1,'admin','$2a$10$nMAQW9ZUf8evd2KUU4Op.eWHK18hkiBDKYxpWnp6oxxQY8yApK4CS',null, 1);
-- adminpass

INSERT INTO Actor (id,username,password,name, tokens_id) VALUES (2,'exporter','$2a$10$YR2zloyMUBjQFrQV1tpaVu1gAJntgsrkPyDGgBwO7SDujl9JHdJGO',null, 2);
-- exportpass