BEGIN;
SET CONSTRAINTS ALL DEFERRED;
DROP TABLE IF EXISTS "log" CASCADE;
DROP TABLE IF EXISTS "configuration" CASCADE;
DROP TABLE IF EXISTS "actor" CASCADE;
DROP TABLE IF EXISTS "broker" CASCADE;
DROP TABLE IF EXISTS "request" CASCADE;
DROP TABLE IF EXISTS "inquirer" CASCADE;
DROP TABLE IF EXISTS "decentralSearchRequest" CASCADE;
DROP TABLE IF EXISTS "inquiry" CASCADE;
DROP TABLE IF EXISTS "registry" CASCADE;
DROP TABLE IF EXISTS "export_definition" CASCADE;
DROP TABLE IF EXISTS "tokens" CASCADE;
DROP TABLE IF EXISTS "inquiryResult" CASCADE;
DROP TABLE IF EXISTS "chartQueryGroup" CASCADE;

CREATE TYPE inquiry_archive_type AS ENUM (
    'IA_POSITIVE_REPLY',
    'IA_NEGATIVE_REPLY',
    'IA_NO_REPLY'
);

CREATE TYPE inquiry_status AS ENUM (
    'IS_NEW',
    'IS_ARCHIVED',
    'IS_PROCESSING',
    'IS_PROCESSED',
    'IS_LDM_ERROR'
);

CREATE TABLE "log" (
    Id SERIAL PRIMARY KEY,
    Actor TEXT,
    Dated timestamp,
    Logger TEXT,
    Level TEXT,
    Type TEXT,
    Message TEXT
);

CREATE TABLE "configuration" (
    Name TEXT PRIMARY KEY,
    Value TEXT
);

CREATE TABLE "actor" (
    Id SERIAL PRIMARY KEY,
    Username TEXT UNIQUE,
    Password TEXT,
    Name TEXT,
    tokens_id INTEGER UNIQUE
);

CREATE TABLE "broker" (
    Id SERIAL PRIMARY KEY,
    address TEXT,
    name TEXT,
    authcode TEXT,
    last_checked timestamp without time zone,
    email TEXT
);

CREATE TABLE "inquiry" (
    Id SERIAL PRIMARY KEY,
    Broker_id INTEGER NOT NULL,
    Result_id INTEGER,
    Inquiry_Source_id INTEGER NOT NULL,
    archive_id INTEGER,
    Revision INTEGER,
    criteria TEXT NOT NULL,
    received_at timestamp without time zone,
    answered_at timestamp without time zone,
    expose_location TEXT,
    status inquiry_status,
    seen boolean DEFAULT false,
    track boolean DEFAULT false,
    ignore boolean DEFAULT false,
    label TEXT,
    description TEXT
);

CREATE TABLE "inquiryResult" (
    Id SERIAL PRIMARY KEY,
    location TEXT NOT NULL,
    size INTEGER,
    executed_at timestamp without time zone,
    expires_at timestamp without time zone,
    notification_sent boolean default false
);

CREATE TABLE "inquiryArchive" (
    id SERIAL PRIMARY KEY,
    type inquiry_archive_type NOT NULL,
    archived_at timestamp without time zone DEFAULT now(),
    note TEXT
);

CREATE TABLE "registry" (
    Id SERIAL PRIMARY KEY,
    address TEXT,
    app_key TEXT,
    last_upload_at timestamp without time zone,
    email TEXT,
    password TEXT
);

CREATE TABLE "tokens" (
    Id SERIAL PRIMARY KEY,
    apikey TEXT UNIQUE,
    signin_token TEXT,
    token_expires_at timestamp without time zone
);

CREATE TABLE "note" (
    Id SERIAL PRIMARY KEY,
    Inquiry_id INTEGER NOT NULL,
    content TEXT NOT NULL
);

CREATE TABLE "export_definition" (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    query TEXT DEFAULT NULL, -- NULL -> empty query
    view_fields TEXT DEFAULT NULL, -- NULL -> all view fields
    actor_id INTEGER NOT NULL,
    changed_from date,
    changed_to date,
    created_from date,
    created_to date,
    episode_from date,
    episode_to date
);

CREATE TABLE "chart_query_group" (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    query TEXT DEFAULT NULL
);

CREATE TYPE chart_type AS ENUM (
    'HISTOGRAM',
    'PIE'
);

CREATE TABLE "chart_definition" (
    id SERIAL PRIMARY KEY,
    group_id INTEGER,
    view_field1 TEXT NOT NULL, -- other view fields may be added for kaplan-meier.
    "type" chart_type NOT NULL
);

ALTER TABLE "actor" ADD FOREIGN KEY (tokens_id) REFERENCES "tokens" (id) ON DELETE CASCADE;
ALTER TABLE "inquiry" ADD FOREIGN KEY (broker_id) REFERENCES "broker" (id) ON DELETE CASCADE;
ALTER TABLE "inquiry" ADD FOREIGN KEY (result_id) REFERENCES "inquiryResult" (id) ON DELETE SET NULL;
ALTER TABLE "inquiry" ADD FOREIGN KEY (archive_id) REFERENCES "inquiryArchive" (id) ON DELETE SET NULL;
ALTER TABLE "note" ADD FOREIGN KEY (inquiry_id) REFERENCES "inquiry" (id) ON DELETE CASCADE;
ALTER TABLE "export_definition" ADD FOREIGN KEY (actor_id) REFERENCES "actor" (id) ON DELETE CASCADE;
ALTER TABLE "chart_definition" ADD FOREIGN KEY (group_id) REFERENCES "chart_query_group" (id) ON DELETE CASCADE;
COMMIT;