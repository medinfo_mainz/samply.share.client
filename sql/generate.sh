#!/bin/bash

# Put jooq.xml and this script in a folder along with the following files and execute generate.sh:
# jooq-3.7.0.jar  jooq-codegen-3.7.0.jar  jooq-codegen-maven-3.7.0.jar  jooq-meta-3.7.0.jar  jooq-scala-3.7.0.jar  postgresql-9.3-1100.jdbc4.jar

java -classpath '*' org.jooq.util.GenerationTool jooq.xml
